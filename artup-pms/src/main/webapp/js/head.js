console.log('乐上 - 爱学习，爱乐上');

//var basePath = 'http://localhost/joyupx/';
var basePath = 'http://127.0.0.1/joyupx/';

window.document.write('<meta name="viewport" content="width=device-width, initial-scale=1.0">');
window.document.write('<meta name="description" content="乐上是一个面向开发者、产品经理、UI设计师、运维工程师等人员的知识分享、知识发布、话题讨论平台。自创建以来，乐上秉承简洁、易用、极致的原则并致力并专注于为开发者、产品经理、UI设计师、运维工程师打造一个纯净的技术交流社区，推动并帮助开发者、产品经理、设计师、运维工程师通过互联网获得知识、分享知识，话题讨论，从而让更多开发者、产品经理、设计师、运维工程师从中受益。乐上的使命是帮助开发者、产品经理、UI设计师、运维城市场更便捷的获取知识，分享经验，交流话题，快乐成长。" />');
window.document.write('<meta name="author" content="乐上" />');
window.document.write('<meta name="keyword" content="乐上,开发者,程序员,程序猿,程序媛,产品经理,设计师,UI,美工,运维,工程师,极客,编程,代码,开源,IT网站,Developer,Programmer,Coder,Geek,技术社区" />');
