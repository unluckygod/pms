var host = 'http://127.0.0.1/';

var memberId = -1;		// 会员ID
var nickname = '';		// 会员的昵称
var username = '';		// 会员的用户名

var memberStr = window.sessionStorage.getItem('memberStr');
if(undefined !== memberStr && null !== memberStr && 0 !== memberStr.trim().length) {
	var member = JSON.parse(memberStr);
	
	if(undefined !== member && null !== member) {
		if(undefined !== member.id && null !== member.id && '' !== member.id) {
			memberId = member.id;
		}
		
		if(undefined !== member.nickname && null !== member.nickname && '' !== member.nickname) {
			nickname = member.nickname;
		}

		if(undefined !== member.username && null !== member.username && '' !== member.username) {
			username = member.username;
		}
	}
}
	
window.document.write('<nav class="navbar navbar-default navbar-fixed-top" role="navigation">');
window.document.write('		<div class="container">');
window.document.write('			<div class="navbar-header">');
window.document.write('				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-menu"><i class="fa fa-bars fa-fw"></i></button>');
window.document.write('				<a class="navbar-brand weight-900" href="index.html"><img alt="" id="logo" src="' + host + 'images/favicon.png" style="width: 48px; height: 48px; margin-top: 5px;"></a>');
window.document.write('			</div>');
window.document.write('			<div class="collapse navbar-collapse" id="main-menu">');
window.document.write('				<ul id="navigation" class="nav navbar-nav navbar-right">');
//window.document.write('					<li class="active dropdown"><a href="' + host + 'index.html">乐上 </a></li>');
window.document.write('					<li><a href="' + host + '">乐上 </a></li>');
if(undefined !== memberId && null !== memberId && 0 < memberId) {
	window.document.write('					<li><a href="' + (host + username) + '">我的主页 </a></li>');
}
window.document.write('					<li><a href="JavaScript:void(0);" onclick="writeArticle();">创作</a></li>');
//window.document.write('					<li class="dropdown">');
//window.document.write('						<a href="#" class="dropdown-toggle" data-toggle="dropdown">管理 <i class="fa fa-caret-down"></i></a>');
//window.document.write('						<ul class="dropdown-menu">');
//window.document.write('							<li><a href="' + host + 'module/blog/html/blogAdd_emditor.html">新博文</a></li>');
//window.document.write('							<li><a href="' + host + 'blogAdd.html">新博文 3</a></li>');
//window.document.write('							<li><a href="' + host + 'module/blog/html/blogAdd_editor.html">新博文 2</a></li>');
//window.document.write('							<li><a href="icons.html">短消息</a></li>');
//window.document.write('							<li><a href="support.html">草稿箱</a></li>');
//window.document.write('							<li><a href="animation-engine.html">设置</a></li>');
//window.document.write('							<li><a href="isotope.html">签名</a></li>');
//window.document.write('							<li><a href="parallax.html">备份</a></li>');		// 后续开发
//window.document.write('						</ul>');
//window.document.write('					</li>');
if(undefined === memberId || null === memberId || -1 === memberId) {
	window.document.write('					<li><a id="signin_navigation_link" href="' + host + 'signin.html">登录</a></li>');
	window.document.write('					<li><a id="register_navigation_link" href="' + host + 'signup.html">注册</a></li>');
} else {
	window.document.write('					<li class="dropdown" >');
	window.document.write('						<a href="' + host + 'member/' + username + '" class="dropdown-toggle" data-toggle="dropdown">' + nickname + '</a>');
	window.document.write('						<ul class="dropdown-menu">');
	window.document.write('							<li><a href="' + host + 'member/' + username + '">我的空间</a></li>');
	window.document.write('							<li><a id="signout_navigation_link" href="javascript:;">退出</a></li>');
	window.document.write('						</ul>');
	window.document.write('					</li>');
} 
window.document.write('					<li><div class="social-icons" style="width: 30px;"></div></li>');
window.document.write('				</ul>');
window.document.write('			</div>');	// navbar-collapse end
window.document.write('		</div>');	// container end
window.document.write('</nav>');
window.document.write('');

/**
 * 写随笔
 */
var writeArticle = function(){
	if( null === getSignedMember() ) {
		layer.msg('您尚未登录，请先登录！', { time: 2000, icon: 5 });
		
		setTimeout(function(){
			window.location.href = host + 'signin';
		}, 2000);
	} else {
		window.location.href = host + 'writeArticle';
	}
}

/*var menuSwitch = function(){
	var navigationItems = window.document.getElementById('navigation').getElementsByTagName("li");
	for(var index = 0; index < navigationItems.length; index++) {
		var item = navigationItems[index];
		item.onclick = function(){
			item.className = 'active';
		};
	}
}*/