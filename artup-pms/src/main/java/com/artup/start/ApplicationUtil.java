package com.artup.start;

import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.artup.util.os.OperationSystemInfo;
import com.artup.util.shell.ChmodShell;

@Component
public class ApplicationUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationUtil.class);
	
	public boolean initFilePermission(String rootPath) {
		if(StringUtils.isBlank(rootPath)) {
			return false;
		}
		
//		LOGGER.info("rootPath = {}", rootPath);
		
		File rootFile = new File(rootPath);
		
		if(!rootFile.exists()) {
			return false;
		}
		
		if(OperationSystemInfo.isLinux()) {
			ChmodShell.updateFolderAuth(rootFile.getAbsolutePath());
		}
		if(OperationSystemInfo.isWindows()) {
			System.out.println(rootFile.getAbsolutePath());
		}
		
		if(rootFile.isDirectory()){
			File subFiles [] = rootFile.listFiles();

			if(null != subFiles && 0 < subFiles.length) {
				for(int index = 0; index < subFiles.length; index++){
					File subFile = subFiles[index];
					
					if(OperationSystemInfo.isLinux()) {
						ChmodShell.updateFolderAuth(subFile.getAbsolutePath());
					}
					if(OperationSystemInfo.isWindows()) {
						System.out.println(subFile.getAbsolutePath());
					}
					
					if(subFile.isDirectory()) {
						initFilePermission(subFile.getAbsolutePath());
					}
				}
			}
		}
		
		return true;
	}
	
	public static void main(String[] args) {
		String contextPath = "D:/workspace/artup-mobile/";
		ApplicationUtil applicationUtil = new ApplicationUtil();
		applicationUtil.initFilePermission(contextPath);
	}
}
