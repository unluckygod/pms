package com.artup.cache;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.artup.common.CacheManager;
import com.artup.common.Constants;
import com.artup.common.cache.CacheObject;
import com.artup.dao.RegionDao;
import com.artup.pojo.Region;
import com.artup.util.CommonUtils;
import com.artup.util.SpringContextUtils;
import com.artup.util.cache.RedisUtils;

import redis.clients.jedis.Jedis;

/**
 * 缓存
 * @author hapday
 * @date 2017年7月19日 @Time 下午2:51:11
 */
public class RegionCache extends CacheObject  {
	private static Jedis jedis; 
	
	private static RegionDao regionDao;

	private static RegionCache _instance;
	
	private final static Logger LOGGER = LoggerFactory.getLogger(RegionCache.class);
	
	public static RegionCache getInstanceByLoad () {
		if (_instance == null) {
			_instance = new RegionCache();
		}
		
		queryAllRegionList();
		
		return _instance;
	}
	
	@Override
	public Object loadData() {
		return null;
	}
	
	@Override
	public String getCacheName() {
		return Constants.CACHE_TYPE_REGION_LIST;
	}
	
	/**
	 * 查询【全部地区】列表
	 */
	private static void queryAllRegionList() {
		StringBuilder pmpt = new StringBuilder("正在加载【全部地区列表】实例");
		if (null == regionDao) {
			regionDao = SpringContextUtils.getBean(RegionDao.class);
		}
		
		List<Region> regionList = null;
		try {
			regionList = regionDao.selectAllRegion();
		} catch (Exception e) {
			LOGGER.error("查询【全部地区列表】 - 失败！", e);
		}
		
		if(CommonUtils.isNotEmpty(regionList)) {
			jedis = RedisUtils.getJedis();
//			jedis.ltrim(Constants.CACHE_TYPE_REGION_LIST, 1, 0);	// 清空上次的记录
//			jedis.lpush(Constants.CACHE_TYPE_REGION_LIST, JSONObject.toJSONString(regionList) );
			jedis.set(Constants.CACHE_TYPE_REGION_LIST, JSONObject.toJSONString(regionList) );
		}
		
		pmpt.append(" [" + (regionList == null ? 0 : regionList.size()) + " instances]");
		CacheManager.printMessage(pmpt.toString(), true);
	}
}
