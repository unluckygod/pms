package com.artup.util.excel;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;

public abstract class AbstractExport{
//	protected int rowIndex = 0;

//	protected WritableWorkbook writableWorkbook;

	protected WritableSheet ws;
	
	protected String filePrefix;
	
	protected String fileSuffix;

	protected String fullName;
	
	protected String encodeFullName;
	
	protected Date exportTime;
	
	protected WritableCellFormat titleStyle;	//表格表头样式
	protected WritableCellFormat contentStyle;	//普通单元格样式
	protected WritableCellFormat format3;	//小计样式
	protected WritableCellFormat format4;	//小计样式
	protected WritableCellFormat formatTitle;   //标题样式
	
	protected WritableCellFormat formatTitle1;//主标题样式
	protected WritableCellFormat formatTitle2;   //副标题样式
	
	public AbstractExport() throws Exception{
		exportTime = new Date();
		
		WritableFont fontTitle2 = new WritableFont(WritableFont.TIMES, 11, WritableFont.NO_BOLD);
		formatTitle2 = new WritableCellFormat(fontTitle2);
		formatTitle2.setAlignment(jxl.format.Alignment.LEFT);
		formatTitle2.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
		//formatTitle2.setBackground(Colour.ICE_BLUE);
		formatTitle2.setBorder(Border.ALL, BorderLineStyle.THIN);
		
		WritableFont fontTitle1 = new WritableFont(WritableFont.TIMES, 16, WritableFont.BOLD);
		formatTitle1 = new WritableCellFormat(fontTitle1);
		formatTitle1.setAlignment(jxl.format.Alignment.CENTRE);
		formatTitle1.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
		//formatTitle1.setBackground(Colour.LIGHT_BLUE);
		formatTitle1.setBorder(Border.ALL, BorderLineStyle.THIN);
		
		WritableFont fontTitle = new WritableFont(WritableFont.TIMES, 14, WritableFont.BOLD);
		formatTitle = new WritableCellFormat(fontTitle);
		formatTitle.setAlignment(jxl.format.Alignment.LEFT);
		formatTitle.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
		//formatTitle.setBackground(Colour.ICE_BLUE);
		formatTitle.setBorder(Border.ALL, BorderLineStyle.THIN);
		
		
		
		WritableFont titleWritableFont = new WritableFont(WritableFont.TIMES, 13, WritableFont.BOLD);
		titleStyle = new WritableCellFormat(titleWritableFont);
		titleStyle.setAlignment(jxl.format.Alignment.CENTRE);
		titleStyle.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
		titleStyle.setBackground(Colour.ICE_BLUE);
		titleStyle.setBorder(Border.ALL, BorderLineStyle.THIN);
		
		WritableFont font2 = new WritableFont(WritableFont.TIMES, 11);
		contentStyle = new WritableCellFormat(font2);
		contentStyle.setAlignment(jxl.format.Alignment.CENTRE);
		contentStyle.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
		contentStyle.setBorder(Border.ALL, BorderLineStyle.THIN);
		
		WritableFont font4 = new WritableFont(WritableFont.TIMES, 12);
		format4 = new WritableCellFormat(font4);
		format4.setAlignment(jxl.format.Alignment.CENTRE);
		format4.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
		format4.setBorder(Border.ALL, BorderLineStyle.THIN);
		format4.setBackground(Colour.LIGHT_BLUE);
		
		WritableFont font3 = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
		format3 = new WritableCellFormat(font3);
		format3.setAlignment(jxl.format.Alignment.CENTRE);
		format3.setBackground(Colour.ICE_BLUE);
		format3.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
	}
	
	public OutputStream getOutputStream() throws IOException {
		HttpServletResponse response = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse();
		response.setContentType("Application/msexcel");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Content-disposition", "attachment; filename=" + fullName);

		return response.getOutputStream();
	}
	
	public void initExport() throws IOException {
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		HttpServletResponse response = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse();
		if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {
			this.encodeFullName = new String(this.fullName.getBytes("UTF-8"),
					"ISO8859-1"); // firefox浏览器
		} else if (request.getHeader("User-Agent").toUpperCase()
				.indexOf("MSIE") > 0) {
			this.encodeFullName = URLEncoder.encode(this.fullName, "UTF-8"); // IE浏览器
		} else {
			this.encodeFullName = toUtf8String(this.fullName);
		}
		response.setContentType("Application/msexcel");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Content-Disposition", "attachment;filename=" + this.encodeFullName);
	}
	
	/**
	 * 把字符串转换成utf-8格式(Excel导出时,文件名调用)
	 * 
	 * @param s
	 * @return
	 */
	public static String toUtf8String(String s) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c >= 0 && c <= 255) {
				sb.append(c);
			} else {
				byte[] b;
				try {
					b = Character.toString(c).getBytes("utf-8");
				} catch (Exception ex) {
					b = new byte[0];
				}
				for (int j = 0; j < b.length; j++) {
					int k = b[j];
					if (k < 0)
						k += 256;
					sb.append("%" + Integer.toHexString(k).toUpperCase());
				}
			}
		}
		return sb.toString();
	}

	public String getFilePrefix() {
		return filePrefix;
	}

	public void setFilePrefix(String filePrefix) {
		this.filePrefix = filePrefix;
	}

	public String getFileSuffix() {
		return fileSuffix;
	}

	public void setFileSuffix(String fileSuffix) {
		this.fileSuffix = fileSuffix;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Date getExportTime() {
		return exportTime;
	}

	public void setExportTime(Date exportTime) {
		this.exportTime = exportTime;
	}
	public void doExport(String templeteFile,Map beans) {  
	    
	}
}
