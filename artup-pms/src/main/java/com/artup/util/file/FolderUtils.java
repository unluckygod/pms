package com.artup.util.file;

import java.io.File;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import com.artup.util.date.DateUtil;
import com.artup.util.os.OperationSystemInfo;
import com.artup.util.shell.ChmodShell;

/**
 * 文件夹
 * @author hapday
 * @date 2017年7月21日 @Time 上午11:07:32
 */
public class FolderUtils {

    /**
     * 创建完整路径
     *
     * @param path
     *            a {@link java.lang.String} object.
     */
    public static final void mkdirs(final String... path) {
        for (String foo : path) {
            final String realPath = FilenameUtils.normalizeNoEndSeparator(foo, true);
            final File folder = new File(realPath);
            if (!folder.exists() || folder.isFile()) {
                folder.mkdirs();
            }
        }
    }

    /**
	 * 获取存储路径
	 * @param passportId 通行证ID
	 * @return 存储路径
	 */
	public static String getStoragePath(String folderPrefix, int passportId) {
		if(StringUtils.isBlank(folderPrefix)) {
			return null;
		}
		
		String fileUploadPath = folderPrefix + File.separator + DateUtil.getNowYear() + File.separator + DateUtil.getNowMonth() + File.separator + DateUtil.getNowMonthDay();

		File yearFile = new File(folderPrefix + File.separator + DateUtil.getNowYear());
		File monthFile = new File(folderPrefix + File.separator + DateUtil.getNowYear() + File.separator + DateUtil.getNowMonth());
		File dayFile = new File(folderPrefix + File.separator + DateUtil.getNowYear() + File.separator + DateUtil.getNowMonth() + File.separator + DateUtil.getNowMonthDay());
		File userFile = new File(folderPrefix + File.separator + DateUtil.getNowYear() + File.separator + DateUtil.getNowMonth() + File.separator + DateUtil.getNowMonthDay() + File.separator + passportId);
		
		if(!yearFile.exists()){
			yearFile.mkdir();
			
			if(OperationSystemInfo.isLinux()) {
				ChmodShell.updateFolderAuth(yearFile.getAbsolutePath());
			}
		}
		if(!monthFile.exists()){
			monthFile.mkdir();

			if(OperationSystemInfo.isLinux()) {
				ChmodShell.updateFolderAuth(monthFile.getAbsolutePath());
			}
		}
		if(!dayFile.exists()){
			dayFile.mkdir();

			if(OperationSystemInfo.isLinux()) {
				ChmodShell.updateFolderAuth(dayFile.getAbsolutePath());
			}
		}
		if(!userFile.exists()){
			userFile.mkdir();

			if(OperationSystemInfo.isLinux()) {
				ChmodShell.updateFolderAuth(userFile.getAbsolutePath());
			}
		}
		
		fileUploadPath = userFile.getAbsolutePath();
		System.out.println(fileUploadPath);
		
		return fileUploadPath + File.separator;
	}
}