package com.artup.util.file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 文件上传
 * @author hapday
 * @date 2017年7月14日 @Time 上午10:44:27
 */
public class FileUploadUtil {
	public static boolean saveFileFromInputStream(InputStream inputStream, String fileUploadPath,
			String newFileName) throws IOException {
		File file = new File(fileUploadPath, newFileName);
		
		if(!file.exists()){
			file.createNewFile();
		}
		
		FileOutputStream fileOutputStream = new FileOutputStream(file);
		byte[] buffer = new byte[1024 * 1024];
		int bytesum = 0;
		int byteread = 0;
		while ((byteread = inputStream.read(buffer)) != -1) {
			bytesum += byteread;
			fileOutputStream.write(buffer, 0, byteread);
			fileOutputStream.flush();
		}
		fileOutputStream.close();
		inputStream.close();
		
		System.out.println("已上传 " + bytesum);
		
		return true;
	}
}
