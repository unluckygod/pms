package com.artup.util.file;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * 读取 Properties 文件
 * @author 雨尘
 * @date 2015年4月21日	@time 下午1:29:38
 */
public final class PropertiesUtils {
	private final static Logger logger = Logger.getLogger(PropertiesUtils.class);
    private static String value = "";
    private static Properties properties = new Properties();
    private static InputStream inputStream = null;

	static {
		// 文件是相对于 classpath 而言的，也就是说 classpath 目录为根目录
		inputStream = PropertiesUtils.class.getResourceAsStream("/common.properties");	
//		properties = new Properties();
		try {
			properties.load(inputStream);
		} catch (IOException e) {
			logger.error(e);
		}
	} 

    public static String getValue(String key) {
    	value = properties.getProperty(key).trim();
    	
    	return value;
    }

    public static String getValue(String filePath, String key) {
		
		try {
			inputStream = PropertiesUtils.class.getResourceAsStream(filePath);
			properties.load(inputStream);
		} catch (IOException e) {
			logger.error(e);
		}
		
    	value = properties.getProperty(key).trim();
    	
    	return value;
    }
}