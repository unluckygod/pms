package com.artup.util.image;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.artup.pojo.ImageInfo;
import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;

/**
 * 图片信息
 * @author hapday
 * @date 2017年8月18日 @Time 上午10:44:50
 */
public class ImageInfoUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ImageInfoUtil.class);
	
	/**
	 * 解析图片元数据，获取图片的基本信息
	 * @param imageFullName 待解析的图片全名（包含完整路径的图片名）
	 * @return
	 */
	public static ImageInfo parseImageInfo(String imageFullName) {
		if(StringUtils.isBlank(imageFullName)) 
			return null;
		
		File imageFile = new File(imageFullName);
		
		Metadata metadata = null;
		try {
			metadata = ImageMetadataReader.readMetadata(imageFile);
		} catch (ImageProcessingException | IOException e) {
			LOGGER.error("读取图片元数据 - 失败！", e);
		}
		
		ImageInfo imageInfo = printImageTags(imageFile, metadata);
		return imageInfo;
	}

	/**
	 * 解析图片元数据 
	 * @param sourceFile
	 *            源文件
	 * @param metadata
	 *            metadata元数据信息
	 * @return
	 */
	private static ImageInfo printImageTags(File sourceFile, Metadata metadata) {
		ImageInfo imageInfo = new ImageInfo();
		imageInfo.setName(sourceFile.getName());
		imageInfo.setSize(sourceFile.getTotalSpace());

		for (Directory directory : metadata.getDirectories()) {
			for (Tag tag : directory.getTags()) {
				String tagName = tag.getTagName();
				String description = tag.getDescription();
				
//				System.out.println("\n---------------------------");
//				System.out.println("标签名：" + tagName);
//				System.out.println("参数值：" + description);
//				System.out.println("---------------------------\n");
				
				if (tagName.equals("File Size")) {
					imageInfo.setSize2(description);
				} else if (tagName.equals("Image Height")) {
					// 图片高度
					imageInfo.setHeight(description);
				} else if (tagName.equals("Image Width")) {
					// 图片宽度
					imageInfo.setWidth(description);
				} else if (tagName.equals("Date/Time Original")) {
					// 拍摄时间
					imageInfo.setCreateTime(description);
				} else if (tagName.equals("GPS Altitude")) {
					// 海拔
					imageInfo.setAltitude(description);
				} else if ("Lens Make".equals(tagName)) {
					imageInfo.setLensMake(description);
				} else if ("Lens Model".equals(tagName)) {
					imageInfo.setLensModel(description);
				} else if (tagName.equals("GPS Latitude")) {
					// 纬度
					imageInfo.setLatitude(pointToLatlong(description));
				} else if (tagName.equals("GPS Longitude")) {
					// 经度
					imageInfo.setLongitude(pointToLatlong(description));
				}
			}
			for (String error : directory.getErrors()) {
				LOGGER.error("解析图片元数据中遇到的错误：", error);
			}
		}
		
		return imageInfo;
	}

	/**
	 * 经纬度转换 度分秒转换
	 * 
	 * @param point
	 *            坐标点
	 * @return
	 */
	private static String pointToLatlong(String point) {
		Double du = Double.parseDouble(point.substring(0, point.indexOf("°")).trim());
		Double fen = Double.parseDouble(point.substring(point.indexOf("°") + 1, point.indexOf("'")).trim());
		Double miao = Double.parseDouble(point.substring(point.indexOf("'") + 1, point.indexOf("\"")).trim());
		Double duStr = du + fen / 60 + miao / 60 / 60;
		return duStr.toString();
	}

	public static void main(String[] args) {
		String sourceImage = null;
		sourceImage = "C:\\Users\\Administrator\\Desktop\\20170818_IMG_0250.jpg";
//		sourceImage = "D:\\Documents\\Pictures\\IMG_6954.jpg";
		// sourceImage = "D:\\Documents\\Pictures\\two.jpg";
		ImageInfo imgInfoBean = ImageInfoUtil.parseImageInfo(sourceImage);
		System.out.println(imgInfoBean.toString());
	}
}