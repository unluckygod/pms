package com.artup.util.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.artup.util.file.PropertiesUtils;

import redis.clients.jedis.Jedis;

/**
 * redis 工具
 * 不建议使用的此工具
 * @author hapday
 * @date 2017年3月10日 @Time 下午2:28:39
 */
public final class RedisUtils2 {
	private static final Logger LOGGER = LoggerFactory.getLogger(RedisUtils2.class);
	
	private static String config = "/application.properties";
	
//	private static String HOST = PropertiesUtils.getValue("/properties/redis.properties", "redis.host");	// IP
//	private static int PORT = Integer.parseInt(PropertiesUtils.getValue("/properties/redis.properties", "redis.port"));		// 端口号
//	private static String PASSWORD = PropertiesUtils.getValue("/properties/redis.properties", "redis.password");	// 密码
//	private static int TIMEOUT = Integer.parseInt(PropertiesUtils.getValue("/properties/redis.properties", "redis.timeout"));	// 超时时间，单位：毫秒

	private static String HOST = PropertiesUtils.getValue(config, "spring.redis.host");	// IP
	private static int PORT = Integer.parseInt(PropertiesUtils.getValue(config, "spring.redis.port"));		// 端口号
	private static String PASSWORD = PropertiesUtils.getValue(config, "spring.redis.password");	// 密码
	private static int TIMEOUT = Integer.parseInt(PropertiesUtils.getValue(config, "spring.redis.timeout"));	// 超时时间，单位：毫秒
	
	private static Jedis jedis = null;
	
	public RedisUtils2() {
	}
	
	static {
		// 多次的 new 会导致内存资源吃紧
		jedis = new Jedis(HOST, PORT, TIMEOUT);
		jedis.auth(PASSWORD);
	}

	/**
	 * 获取 redis 资源，建立与 redis 的连接
	 * @return
	 */
	public synchronized static Jedis getJedis() {
		try {
			return jedis;
		} catch(Exception e) {
			LOGGER.error("连接 redis 失败！", e);
			
			return null;
		}
	}

	/**
	 * 释放资源，关闭 redis 连接
	 * @param jedis
	 */
	public static void close(Jedis jedis) {
		try {
			if (null != jedis) {
				jedis.close();
			}
		} catch(Exception e) {
			LOGGER.error("关闭 redis 连接失败！", e);
		}
	}
}