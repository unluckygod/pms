package com.artup.util;

import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.MethodMetadata;

public class AnnotationUtil {
	
	/**
	 * 获取类上annotation的所有属性
	 * 
	 * @param metadata
	 * @param annoClass
	 * @return annotationAttributes
	 */
	public static AnnotationAttributes attributesFor(AnnotationMetadata metadata, Class<?> annoClass) {
		return attributesFor(metadata, annoClass.getName());
	}

	/**
	 * 获取annotation的指定属性
	 * 
	 * @param metadata
	 * @param annoClassName
	 * @return annotationAttributes
	 */
	public static AnnotationAttributes attributesFor(AnnotationMetadata metadata, String annoClassName) {
		return AnnotationAttributes.fromMap(metadata.getAnnotationAttributes(annoClassName, false));
	}

	/**
	 * 获取annotation的指定属性
	 * 
	 * @param metadata
	 * @param targetAnno
	 * @return annotationAttributes
	 */
	public static AnnotationAttributes attributesFor(MethodMetadata metadata, Class<?> targetAnno) {
		return AnnotationAttributes.fromMap(metadata.getAnnotationAttributes(targetAnno.getName()));
	}
}
