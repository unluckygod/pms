package com.artup.util.shell;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 执行 Shell 脚本
 * @author hapday
 * @date 2017年3月14日 @Time 下午2:28:22
 */
public class ChmodShell {
	private static final Logger LOGGER = LoggerFactory.getLogger(ChmodShell.class);
	/**
	 * 更新文件夹的权限
	 * @param folderName
	 */
	public static boolean updateFolderAuth(String folderName) {
		if(StringUtils.isBlank(folderName)) {
			return false;
		}
		
//		String command = "chmod a+x mk.sh";		// 设置 shell 脚本的为“可读写”与“可执行”
		String command = "chmod 777 " + folderName;		// 设置 shell 脚本的为“可读写”与“可执行”
		Process process = null;
		try {
			process = Runtime.getRuntime().exec(command);
		} catch (IOException e) {
			LOGGER.error("执行更新文件权限失败！", e);
			
			return false;
		}
		try {
			// 阻塞当前线程，直到 command 命令执行完毕
			process.waitFor();		
		} catch (InterruptedException e) {
			LOGGER.error("等待命令执行 - 失败！", e);
			
			return false;
		} 
		
		// 要执行的 shell 脚本
		/*command = "bash /home/hapday/mk.sh" + folderName; 	// 也可以是 ksh、sh 等
		
		try {
			process = Runtime.getRuntime().exec(command);
		} catch (IOException e) {
			e.printStackTrace();
		}

		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		try {
			while ( null != (bufferedReader.readLine()) );
			
			bufferedReader.close();		// 关闭缓冲流
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			// 阻塞当前线程，直到 command 命令执行完毕
			process.waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}*/
		
		return true;
	}

	/**
	 * 更新文件的权限
	 * @param fileName 文件名（文件或目录）
	 * @param authority 权限值
	 * @return
	 */
	public static boolean updateFileAuthority(String fileName, int authority) {
		if(StringUtils.isBlank(fileName)) {
			return false;
		}
		
		if(0 >= authority) {
			authority = 775;
		}
		
//		String command = "chmod a+x mk.sh";		// 设置 shell 脚本的为“可读写”与“可执行”
		String command = "chmod " + authority + " " + fileName;		// 设置 shell 脚本的为“可读写”与“可执行”
		Process process = null;
		try {
			process = Runtime.getRuntime().exec(command);
		} catch (IOException e) {
			LOGGER.error("执行更新文件权限失败！", e);
			
			return false;
		}
		
		try {
			// 阻塞当前线程，直到 command 命令执行完毕
			process.waitFor();		
		} catch (InterruptedException e) {
			LOGGER.error("等待命令执行 - 失败！", e);
			
			return false;
		} 
		
		return true;
	}
	
	public static boolean isLinux(){
		String osType = System.getProperty("os.name").toLowerCase(); 
		return 0 <= osType.indexOf("linux");  
	}
	
	public static void main(String[] args) {
		String parameter = "test";
		
		if(null != args && 0 < args.length) {
			parameter = args[0];
		}
		
		updateFolderAuth(parameter);
	}
}
