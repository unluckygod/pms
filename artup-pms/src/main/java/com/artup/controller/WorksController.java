package com.artup.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.artup.common.ResponseResult;
import com.artup.pojo.Works;
import com.artup.service.WorksService;

/**
 * 作品
 * @author hapday
 * @date 2017年7月21日 @Time 下午6:41:20
 */
@RestController
@RequestMapping( value = { "admin/works" } )
public class WorksController {
	protected static final Logger LOGGER = LoggerFactory.getLogger(WorksController.class);

	@Autowired
	private WorksService worksService;
	
	/**
	 * 查询【作品】
	 * @param id
	 * @return
	 */
	@RequestMapping( value = { "queryWorks/{id}" } )
	public ResponseResult queryWorks(@PathVariable String id) {
		LOGGER.info("id = {}", id);
		
		ResponseResult responseResult = this.worksService.queryWorksById(id);
		
		return responseResult;
	}
	
	/**
	 * 根据【IDs】批量删除【作品】
	 * @param ids 【IDs】
	 * @return
	 */
	@RequestMapping( value = { "batchDeleteWorks/{passportId}" }, method = { RequestMethod.POST } )
	public ResponseResult batchDeleteWorks(@PathVariable Integer passportId, String ids) {
		LOGGER.info("passportId = {}, ids = {}", passportId, ids);
		
		ResponseResult responseResult = this.worksService.batchDeleteWorksByIds(passportId, ids);
		
		return responseResult;
	}
	
	/**
	 * 保存【作品】
	 * @param worksJSONObject
	 * @return
	 */
	@RequestMapping( value = { "saveWorks" } )
	public ResponseResult saveWorks(@RequestBody String worksJSONObject) {
		LOGGER.info("worksJSONObject = {}", worksJSONObject);
		
		ResponseResult responseResult = this.worksService.saveWorks(worksJSONObject);
		
		return responseResult;
	}
	
	/**
	 * 查询【作品列表】
	 * @param works
	 * @return 【作品列表】
	 */
	@RequestMapping( value = { "queryWorkses" }, method = { RequestMethod.POST } )
	public ResponseResult queryWorkses(Works works) {
		LOGGER.info("works = {}", JSONObject.toJSONString(works));
		
		ResponseResult responseResult = this.worksService.queryWorksList(works);
		
		return responseResult;
	}
	
	/**
	 * 查询【购物车中作品数】
	 * @param passportId 通行证ID
	 * @return 查询【购物车中作品数】
	 */
	@RequestMapping( value = { "queryTrolleyWorksCount/{passportId}" }, method = { RequestMethod.GET } )
	public ResponseResult queryTrolleyWorksCount(@PathVariable Integer passportId) {
		LOGGER.info("passportId = {}", passportId);
		
		ResponseResult responseResult = this.worksService.queryTrolleyWorksCountByPassportId(passportId);
		
		return responseResult;
	}
}
