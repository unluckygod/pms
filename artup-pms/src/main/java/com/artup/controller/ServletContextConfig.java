package com.artup.controller;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.artup.interceptor.SigninInterceptor;
import com.artup.start.Application;

import lombok.extern.slf4j.Slf4j;

/**
 *
 */
@Slf4j
@Configuration
@ComponentScan(basePackageClasses = Application.class, useDefaultFilters = true)
public class ServletContextConfig extends WebMvcConfigurationSupport {

    static final private String FAVICON_URL = "/favicon.ico";
    static final private String PROPERTY_APP_ENV = "application.environment";
    static final private String PROPERTY_DEFAULT_ENV = "dev";



    /**
     * 发现如果继承了WebMvcConfigurationSupport，则在yml中配置的相关内容会失效。
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
    	log.info("----------------");
    	log.info("注册【资源】");
    	log.info("----------------");
        registry.addResourceHandler("/").addResourceLocations("/**");
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
    }


    /**
     * 配置servlet处理
     */
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
    	log.info("===============");
    	log.info("注册【拦截器】");
    	log.info("===============");
        registry.addInterceptor(new SigninInterceptor())
                .addPathPatterns("/admin/**")
                .excludePathPatterns(FAVICON_URL);
    }

}