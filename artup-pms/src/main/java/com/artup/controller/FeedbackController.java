package com.artup.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.artup.common.ResponseResult;
import com.artup.pojo.Feedback;
import com.artup.service.FeedbackService;
import com.artup.util.file.PropertiesUtils;

@RestController
@RequestMapping( value = { "feedback" } )
public class FeedbackController {
	protected static final Logger LOGGER = LoggerFactory.getLogger(FeedbackController.class);
	
	@Autowired
	private FeedbackService feedbackService;
	
	/**
	 * 保存【反馈意见】
	 * @param feedback 【反馈意见】
	 * @return
	 */
	@RequestMapping( value = { "saveFeedback" } )
	public ResponseResult saveFeedback(Feedback feedback) {
		LOGGER.info("feedback = {}", JSONObject.toJSONString(feedback));
		ResponseResult responseResult = this.feedbackService.saveFeedback(feedback);
		return responseResult;
	}

	/*@RequestMapping( value = { "queryFeedbacks" } )
	@SuppressWarnings("unchecked")
	public Map<String, Object> queryFeedbacks(Feedback feedback, Integer start, Integer length) {
		LOGGER.info("feedback = {}", JSONObject.toJSONString(feedback));
		
		feedback.setPageIndex(start);
		feedback.setPageSize(length);
		
		ResponseResult responseResult = this.feedbackService.queryFeedbackList(feedback);
		
		Map<String, Object> resultMap = new HashMap<String, Object>(); // 结果集
		if (null != responseResult && 1 == responseResult.getStatus()) {
			resultMap = (Map<String, Object>) responseResult.getData();
		}
	
		return resultMap;
	}*/

	@RequestMapping( value = { "queryFeedbacks" } )
	public ResponseResult queryFeedbacks(Feedback feedback, Integer start, Integer length) {
		LOGGER.info("feedback = {}", JSONObject.toJSONString(feedback));
		
		if(null == start){
			feedback.setPageIndex(1);
		} else {
			feedback.setPageIndex(start);
		}
		if(null == length) {
			feedback.setPageSize(Integer.parseInt(PropertiesUtils.getValue("pagination.pageSize")));
		} else {
			feedback.setPageSize(length);
		}
		
		ResponseResult responseResult = this.feedbackService.queryFeedbackList(feedback);
		
		return responseResult;
	}
}
