package com.artup.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.artup.common.ResponseResult;
import com.artup.pojo.User;
import com.artup.service.UserService;

/**
 * 用户管理
 * @date 2017年2月10日 下午9:40:37
 * @author hapday
 */
@Controller
@RequestMapping( value = { "admin/user" } )
public class UserController {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserService userService;

	/**
	 * 根据【ID】查询【用户】
	 * @param id
	 * @return
	 */
	@RequestMapping( value = { "queryUser/{id}" } )
	public @ResponseBody ResponseResult queryUser(@PathVariable Long id) {
		LOGGER.info("id = " + id);

		ResponseResult responseResult = userService.queryUserById(id);
        
		return responseResult;
	}

	/**
	 * 删除【用户】
	 * @param user
	 * @return
	 */
	@RequestMapping( value = { "deleteUser" }, method = { RequestMethod.POST, RequestMethod.GET } )
	public @ResponseBody ResponseResult deleteUser(long id) {
		LOGGER.info("id = " + id);
		
		ResponseResult responseResult = this.userService.deleteUserById(id);
		
		return responseResult;
	}
	
	/**
	 * 转到【用户添加】页面
	 * @return
	 */
	@RequestMapping( value = { "userAdd" }, method = { RequestMethod.GET } )
	public String userAdd() {
		return "user/userAdd";
	}
	
	/**
	 * 保存【用户】
	 * @param user
	 * @return
	 */
//	@RequestMapping( value = { "saveUser" }, produces = "application/json; charset=utf-8" )
	@RequestMapping( value = { "saveUser" } )
	public @ResponseBody ResponseResult saveUser(User user) {
		LOGGER.info("user = " + JSONObject.toJSONString(user));
		
		ResponseResult responseResult = this.userService.saveUser(user);
		
		return responseResult;
	}

	/**
	 * 转到【用户列表】页面
	 * @return
	 */
	@RequestMapping( value = { "userList" }, method = { RequestMethod.GET } )
	public String userList() {
		return "user/userList";
	}
	
	/**
	 * 模糊查询【用户列表】
	 * @param user
	 * @return
	 */
	@RequestMapping( value = { "queryUsers" } )
	public @ResponseBody ResponseResult queryUsers(@ModelAttribute("user") User user) {
		LOGGER.info("user = {}", JSONObject.toJSONString(user));
		
		ResponseResult responseResult = userService.queryUserList(user);
		
		return responseResult;
	}
	
	/**
	 * 根据【ID】查询【用户】
	 * @param id
	 * @return
	 */
	@RequestMapping( value = { "userDetail/{id}" } )
	public String userDetail( @PathVariable( name = "id" ) Long id, Model model ) {
		LOGGER.info("id = " + id);

		ResponseResult responseResult = userService.queryUserById(id);
		model.addAttribute("user", responseResult.getData());
        
		return "user/userDetail";
	}
	
	/**
	 * 转到【用户编辑】页面
	 * @return
	 */
	@RequestMapping( value = { "editUser/{id}" }, method = { RequestMethod.GET } )
	public String editUser( @PathVariable( name = "id" ) Long id, Model model ) {
		LOGGER.info("id = " + id);

		ResponseResult responseResult = userService.queryUserById(id);
		model.addAttribute("user", responseResult.getData());
		
		return "user/userEdit";
	}
	
	/**
	 * 更新【用户】
	 * @param user
	 * @return
	 */
	@RequestMapping( value = { "updateUser" }, method = { RequestMethod.POST, RequestMethod.GET } )
	public @ResponseBody ResponseResult updateUser(User user) {
		LOGGER.info("user = " + JSONObject.toJSONString(user));
		
		ResponseResult responseResult = this.userService.updateUserById(user);
		
		return responseResult;
	}
	
}
