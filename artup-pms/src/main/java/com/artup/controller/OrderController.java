package com.artup.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.artup.common.ResponseResult;
import com.artup.pojo.Order;
import com.artup.service.OrderService;

/**
 * 订单
 * @author hapday
 * @date 2017年7月17日 @Time 下午12:00:22
 */
@RestController
@RequestMapping( value = { "admin/order" } )
public class OrderController {
	protected static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);
	
	@Autowired
	private OrderService orderService;
	
	/**
	 * 保存【购物车】
	 * @param worksJSONObject
	 * @return
	 */
	@RequestMapping( value = { "saveTrolley" } )
	public ResponseResult saveTrolley(@RequestBody String worksJSONObject) {
		LOGGER.info("worksJSONObject = {}", worksJSONObject);
		
		ResponseResult responseResult = this.orderService.saveTrolley(worksJSONObject);
		
		return responseResult;
	}

	/**
	 * 查询【购物车详情列表】
	 * @param passportId 【通行证ID】
	 * @return
	 */
	@RequestMapping( value = { "queryTrolleyDetailList/{passportId}" } )
	public ResponseResult queryTrolleyDetailList(@PathVariable Integer passportId, Integer pageIndex, Integer pageSize) {
		LOGGER.info("passportId = {}, pageIndex = {}, pageSize = {}", passportId, pageIndex, pageSize);
		
		ResponseResult responseResult = this.orderService.queryTrolleyDetailListByPassportId(passportId, pageIndex, pageSize);
		
		return responseResult;
	}
	
	/**
	 * 修改【购物车数量】
	 * @param passportId 【通行证ID】
	 * @param edtId 【作品ID】
	 * @param num 【数量】
	 * @return
	 */
	@RequestMapping( value = { "updateTrolleyNum/{passportId}" } )
	public ResponseResult updateTrolleyNum(@PathVariable Integer passportId, String edtId, Integer num) {
		LOGGER.info("passportId = {}, edtId = {}, num = {}", passportId, edtId, num);
		ResponseResult responseResult = this.orderService.updateTrolleyNum(passportId, edtId, num);
		return responseResult;
	}
	
	
	/**
	 * 根据【通行证ID】查询【订单总数】
	 * @param passportId 【通行证ID】
	 * @return
	 */
	@RequestMapping( value = { "selectOrderQuantity/{passportId}" } )
	public ResponseResult selectOrderQuantity(@PathVariable Integer passportId) {
		LOGGER.info("passportId = {}", passportId);
		ResponseResult responseResult = this.orderService.selectOrderQuantityByPassportId(passportId);
		return responseResult;
	}
	
	/**
	 * 创建订单
	 * @param order 订单
	 * @return
	 */
	@RequestMapping( value = { "createOrder" }, method = { RequestMethod.POST } )
	public ResponseResult createOrder(Order order) {
		LOGGER.info("order = {}", JSONObject.toJSONString(order));
		
		ResponseResult responseResult = this.orderService.createOrder(order);
		
		return responseResult;
	}
	
	/**
	 * 创建快捷订单（直接购买）
	 * @param worksJSONObject
	 * @return
	 */
	@RequestMapping( value = { "createRapidOrder" }, method = { RequestMethod.POST } )
	public ResponseResult createRapidOrder(@RequestBody String orderJSONObject) {
		LOGGER.info("orderJSONObject = {}", orderJSONObject);
		
		ResponseResult responseResult = this.orderService.createRapidOrder(orderJSONObject);
		
		return responseResult;
	}
	
	/**
	 * 查询【订单列表】
	 * @param passportId 通行证ID
	 * @return
	 */
	@RequestMapping( value = { "queryOrders" } )
	public ResponseResult queryOrders(Order order) {
		LOGGER.info("order = {}", JSONObject.toJSONString(order));
		
		ResponseResult responseResult = this.orderService.queryOrderList(order);
		
		return responseResult;
	}
	
	/**
	 * 查询【订单详情】
	 * @param id 【ID】
	 * @return 【订单详情】
	 */
	@RequestMapping( value = { "queryOrder/{id}" } )
	public ResponseResult queryOrder(@PathVariable String id) {
		LOGGER.info("id = {}", id);
		
		ResponseResult responseResult = this.orderService.queryOrderById(id);
		
		return responseResult;
	}
	
	/**
	 * 取消订单
	 * @param code 订单号
	 * @return
	 */
	@RequestMapping( value = { "cancelOrder/{code}" } )
	public ResponseResult cancelOrder(@PathVariable String code) {
		LOGGER.info("code = {}", code);
		
		ResponseResult responseResult = this.orderService.cancelOrderByCode(code);
		
		return responseResult;
	}

	/**
	 * 回收订单/删除订单
	 * @param code 订单号
	 * @return
	 */
	@RequestMapping( value = { "recycleOrder/{code}" } )
	public ResponseResult recycleOrder(@PathVariable String code) {
		LOGGER.info("code = {}", code);
		
		ResponseResult responseResult = this.orderService.recycleOrderByCode(code);
		
		return responseResult;
	}
	
	/**
	 * 导出 Excel
	 * @param order 订单
	 */
	@RequestMapping( value = { "exportExcel" } )
	public void exportExcel(Order order) {
		LOGGER.info("order = {}", JSONObject.toJSONString(order));
		
		this.orderService.exportExcel(order);
	}
	
	/**
	 * 更新【审核状态】
	 * @param code
	 * @param checkStatus
	 * @return
	 */
	/*@RequestMapping( value = { "updateOrderCheckStatus/{code}" }, method = { RequestMethod.GET } )
	public ResponseResult updateOrderCheckStatus(@PathVariable String code, Byte checkStatus) {
		LOGGER.info("code = {}, checkStatus = {}", code, checkStatus);
		
		ResponseResult responseResult = this.orderService.updateOrderCheckStatusByCode(code, checkStatus);
		
		return responseResult;
	}*/

	/**
	 * 更新【审核状态】
	 * @param id
	 * @param checkStatus
	 * @return
	 */
	@RequestMapping( value = { "updateOrderCheckStatus/{id}/{checkStatus}" }, method = { RequestMethod.GET } )
	public ResponseResult updateOrderCheckStatus(@PathVariable String id, @PathVariable Byte checkStatus) {
		LOGGER.info("id = {}, checkStatus = {}", id, checkStatus);
		
		ResponseResult responseResult = this.orderService.updateOrderCheckStatusById(id, checkStatus);
		
		return responseResult;
	}
}
