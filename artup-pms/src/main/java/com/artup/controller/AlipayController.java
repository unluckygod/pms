package com.artup.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.artup.common.ResponseResult;
import com.artup.service.AlipayService;

/**
 * 支付宝
 * @author hapday
 * @date 2017年7月24日 @Time 下午6:50:47
 */
@RestController
@Scope(value = "prototype")
@RequestMapping( value = { "alipay" } )
public class AlipayController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AlipayController.class);
	
	@Autowired
	private AlipayService alipayService;
	/**
	 * 创建支付宝预支付信息
	 * @param orderCode 订单号
	 * @return
	 */
	@RequestMapping( value = { "createAlipayOrder" }, method = RequestMethod.GET )
	public @ResponseBody ResponseResult createAlipayOrder(String orderCode, float totalFee, String worksName) {
		LOGGER.info("orderCode = {}, totalFee = {}, worksName = {}", orderCode, totalFee, worksName);
		
		ResponseResult responseResult = this.alipayService.createAlipayOrder(orderCode, totalFee, worksName);
		
		return responseResult;
	}
	
	/**
	 * 查询【支付宝】订单
	 * @param orderCode 订单号
	 * @param alipayOrderCode 支付宝订单号
	 * @return 【支付宝】订单
	 */
	@RequestMapping( value = { "queryAlipayOrder" }, method = RequestMethod.GET )
	public ResponseResult queryAlipayOrder(String orderCode, String alipayOrderCode) {
		LOGGER.info("orderCode = {}, alipayOrderCode = {}", orderCode, alipayOrderCode);
		
		ResponseResult responseResult = this.alipayService.queryAlipayOrder(orderCode, alipayOrderCode);
		
		return responseResult;
	}
	
	/**
	 * 支付宝支付回调
	 * @param request
	 * @return
	 */
	@RequestMapping( value = { "aliPayCallback" }, method = { RequestMethod.POST, RequestMethod.GET } )
	public @ResponseBody String aliPayCallback(HttpServletRequest request) {
		ResponseResult responseResult = this.alipayService.aliPayCallback(request);
		
		if(1 == responseResult.getStatus()) {
			return "success";
		} else {
			return "failure";
		}
	}
}
