package com.artup.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.artup.common.ResponseResult;
import com.artup.pojo.Address;
import com.artup.service.AddressService;

@RestController
@RequestMapping( value = { "address" } )
public class AddressController {
	protected static final Logger LOGGER = LoggerFactory.getLogger(AddressController.class);
	
	@Autowired
	private AddressService addressService;
	
	/**
	 * 保存【地址】
	 * @param address 【地址】
	 * @return
	 */
	@RequestMapping( value = { "saveAddress" } )
	public ResponseResult saveAddress(Address address) {
		LOGGER.info("address = {}", JSONObject.toJSONString(address));
		
		ResponseResult responseResult = this.addressService.saveAddress(address);
		
		return responseResult;
	}

	/**
	 * 根据【ID】更新【地址】
	 * @param address 【地址】
	 * @return
	 */
	@RequestMapping( value = { "updateAddress" } )
	public ResponseResult updateAddress(Address address) {
		LOGGER.info("address = {}", JSONObject.toJSONString(address));
		
		ResponseResult responseResult = this.addressService.updateAddressById(address);
		
		return responseResult;
	}

	/**
	 * 根据【ID】删除【地址】
	 * @param id
	 * @return
	 */
	@RequestMapping( value = { "deleteAddress/{id}" } )
	public ResponseResult deleteAddress(@PathVariable String id) {
		LOGGER.info("id = {}", id);
		
		ResponseResult responseResult = this.addressService.deleteAddressById(id);
		
		return responseResult;
	}
	
	/**
	 * 查询【默认地址】
	 * @param passportId 通行证ID
	 * @return 默认地址
	 */
	@RequestMapping( value = { "queryDefaultAddress/{passportId}" } )
	public ResponseResult queryDefaultAddress(@PathVariable int passportId) {
		LOGGER.info("passportId = {}", passportId);
		
		ResponseResult responseResult = this.addressService.queryDefaultAddressByPassportId(passportId);
		
		return responseResult;
	}
	
	/**
	 * 查询【地址列表】
	 * @param passportId 通行证ID
	 * @return 【地址列表】
	 */
	@RequestMapping( value = { "queryAddressList/{passportId}" } )
	public ResponseResult queryAddressList(@PathVariable int passportId) {
		LOGGER.info("passportId = {}", passportId);
		
		ResponseResult responseResult = this.addressService.queryAddressListByPassportId(passportId);
		
		return responseResult;
	}

	/**
	 * 查询【地址列表】
	 * @param passportId 通行证ID
	 * @return 【地址列表】
	 */
	@RequestMapping( value = { "queryAddresses/{passportId}" } )
	public ResponseResult queryAddresses(@PathVariable int passportId) {
		LOGGER.info("passportId = {}", passportId);
		
		ResponseResult responseResult = this.addressService.queryAddressListByPassportId2(passportId);
		
		return responseResult;
	}
	
	/**
	 * 根据【ID】更新【地址】为默认
	 * @param id 【ID】
	 * @return
	 */
	@RequestMapping( value = { "updateAddressDefault/{id}" } )
	public ResponseResult updateAddressDefault(@PathVariable String id) {
		LOGGER.info("id = {}", id);
		
		ResponseResult responseResult = this.addressService.updateAddressDefaultById(id);
				
		return responseResult;
	}
}
