package com.artup.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.alibaba.fastjson.JSONObject;
import com.artup.common.ResponseResult;
import com.artup.pojo.DeskCalendar;
import com.artup.service.DeskCalendarService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping( value = { "admin/deskCalendar" } )
public class DeskCalendarController {
	@Autowired
	private DeskCalendarService deskCalendarService;
	
	@RequestMapping( value = { "queryDeskCalendars" } )
	public ResponseResult queryDeskCalendars(DeskCalendar deskCalendar) {
		log.info("deskCalendar = {}", JSONObject.toJSONString(deskCalendar));
		
		ResponseResult responseResult = this.deskCalendarService.queryDeskCalendarList(deskCalendar);
		
		return responseResult;
	}
	
	@RequestMapping( value = { "saveDeskCalendar" } )
	public @ResponseBody ResponseResult saveDeskCalendar(Integer name, String skuCode, MultipartHttpServletRequest multipartRequest ) {
		log.info("name = {}, skuCode = {}", name, skuCode);
		
		ResponseResult responseResult = this.deskCalendarService.saveDeskCalendar(name, skuCode, multipartRequest);
		
		return responseResult;
	}
}
