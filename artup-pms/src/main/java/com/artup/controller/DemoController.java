package com.artup.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping( value = { "demo" } )
public class DemoController {
	
	@RequestMapping( value = { "upload" } )
	public String uploadDemo() {
		return "fileUpload";
	}
}
