package com.artup.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.artup.common.Constants;
import com.artup.common.ResponseResult;
import com.artup.pojo.Resource;
import com.artup.service.ResourceService;

/**
 * 资源管理
 * @author hapday
 * @date 2017年3月8日 @Time 下午2:37:34
 */
@Controller
@RequestMapping( value = { "admin/resource" } )
public class ResourceController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ResourceController.class);
	
	@Autowired
	private ResourceService resourceService;

	/**
	 * 根据【ID】查询【资源】
	 * @param id
	 * @return
	 */
	@RequestMapping( value = { "queryResource/{id}" } )
	public @ResponseBody ResponseResult queryResource(@PathVariable Long id) {
		LOGGER.info("id = {}", id);

		ResponseResult responseResult = resourceService.queryResourceById(id);
        
		return responseResult;
	}

	/**
	 * 删除【资源】
	 * @param resource
	 * @return
	 */
	@RequestMapping( value = { "deleteResource" }, method = { RequestMethod.POST, RequestMethod.GET } )
	public @ResponseBody ResponseResult deleteResource(long id) {
		LOGGER.info("id = " + id);
		
		ResponseResult responseResult = this.resourceService.deleteResourceById(id);
		
		return responseResult;
	}
	
	/**
	 * 转到【资源添加】页面
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping( value = { "addResource" }, method = { RequestMethod.GET } )
	public String addResource( Model model ) {
		ResponseResult responseResult = this.resourceService.queryAllResourceList();
		
		if(null != responseResult && Constants.ACTION_STATUS_SUCCESS == responseResult.getStatus()) {
			List<Resource> resources = (List<Resource>) responseResult.getData();

			model.addAttribute("resources", resources);
		}
		
		return "resource/resourceAdd";
	}
	
	/**
	 * 保存【资源】
	 * @param resource
	 * @return
	 */
//	@RequestMapping( value = { "saveResource" }, produces = "application/json; charset=utf-8" )
	@RequestMapping( value = { "saveResource" } )
	public @ResponseBody ResponseResult saveResource(Resource resource) {
		LOGGER.info("resource = " + JSONObject.toJSONString(resource));
		
		ResponseResult responseResult = this.resourceService.saveResource(resource);
		
		return responseResult;
	}

	/**
	 * 转到【资源列表】页面
	 * @return
	 */
	@RequestMapping( value = { "resourceList" }, method = { RequestMethod.GET } )
	public String resourceList() {
		return "resource/resourceList";
	}
	
	/**
	 * 模糊查询【资源列表】
	 * @param resource
	 * @return
	 */
	@RequestMapping( value = { "queryResources" } )
	public @ResponseBody ResponseResult queryResources(@ModelAttribute("resource") Resource resource) {
		LOGGER.info("resource = {}", JSONObject.toJSONString(resource));
		
		ResponseResult responseResult = resourceService.queryResourceList(resource);
		
		return responseResult;
	}
	
	/**
	 * 根据【ID】查询【资源】
	 * @param id
	 * @return
	 */
	@RequestMapping( value = { "resourceDetail/{id}" } )
	public String resourceDetail( @PathVariable( name = "id" ) Long id, Model model ) {
		LOGGER.info("id = " + id);

		ResponseResult responseResult = resourceService.queryResourceById(id);
		model.addAttribute("resource", responseResult.getData());
        
		return "resource/resourceDetail";
	}
	
	/**
	 * 转到【资源编辑】页面
	 * @return
	 */
	@RequestMapping( value = { "editResource/{id}" }, method = { RequestMethod.GET } )
	public String editResource( @PathVariable( name = "id" ) Long id, Model model ) {
		LOGGER.info("id = " + id);

		ResponseResult responseResult = resourceService.queryResourceById(id);
		if(Constants.ACTION_STATUS_SUCCESS == responseResult.getStatus() && null != responseResult.getData()) {
			model.addAttribute("resource", responseResult.getData());
		}
		
		responseResult = this.resourceService.queryAllResourceList();
		if(Constants.ACTION_STATUS_SUCCESS == responseResult.getStatus() && null != responseResult.getData()) {
			model.addAttribute("resources", responseResult.getData());
		}
		
		return "resource/resourceEdit";
	}
	
	/**
	 * 更新【资源】
	 * @param resource
	 * @return
	 */
	@RequestMapping( value = { "updateResource" }, method = { RequestMethod.POST, RequestMethod.GET } )
	public @ResponseBody ResponseResult updateResource(Resource resource) {
		LOGGER.info("resource = " + JSONObject.toJSONString(resource));
		
		ResponseResult responseResult = this.resourceService.updateResourceById(resource);
		
		return responseResult;
	}
}
