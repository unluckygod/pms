package com.artup.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.artup.common.ResponseResult;
import com.artup.pojo.Sku;
import com.artup.service.SkuService;

@RestController
@RequestMapping( value = { "admin/sku" } )
public class SkuController {
	protected static final Logger LOGGER = LoggerFactory.getLogger(SkuController.class);
	
	@Autowired
	private SkuService skuService;
	
	@RequestMapping( value = { "querySkus" } )
	public ResponseResult querySkus(Sku sku) {
		LOGGER.info("sku = {}", JSONObject.toJSONString(sku));
		
		ResponseResult responseResult = this.skuService.querySkuList(sku);
		
		return responseResult;
	}
}
