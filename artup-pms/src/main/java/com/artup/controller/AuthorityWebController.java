package com.artup.controller;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.artup.common.ResponseResult;
import com.artup.service.AuthorityWebService;
import com.artup.util.http.HttpUtils;
import com.github.botaruibo.xvcode.generator.Generator;
import com.github.botaruibo.xvcode.generator.GifVCGenerator;

@Controller
public class AuthorityWebController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthorityWebController.class);
	
	@Autowired
	private AuthorityWebService authorityWebService;
	
	@RequestMapping( value = { "login" }, method = { RequestMethod.POST, RequestMethod.GET } )
	public @ResponseBody ResponseResult login(String username, String password, String captcha, HttpServletRequest httpServletRequest, HttpSession httpSession) {
		LOGGER.info("username = " + username);
		
		String clientIP = HttpUtils.getClientRealIP(httpServletRequest);
		
		ResponseResult responseResult = this.authorityWebService.signin(username, password, captcha, clientIP, httpSession);
		
		return responseResult;
	}

	/**
	 * 验证码生成器
	 * @param httpSession
	 * @param reponse
	 */
	@RequestMapping( value = { "securityCodeGenerator" }, method = { RequestMethod.GET } )
	public void securityCodeGenerator(HttpSession httpSession, HttpServletResponse reponse) {
		Integer height = 40;	// image 高度。  image height. count as pixel
		Integer width = 116;	// image 宽度。 image width. count as pixel
		Integer count = 5;  	// 字符数 validation code length.
		
		Generator generator = new GifVCGenerator(width, height, count);//   gif
		try {
			generator.write2out(reponse.getOutputStream()).close();
		} catch (IOException e) {
			LOGGER.error("验证码生成 - 失败！", e);
		};
		
		String securityCode = generator.text();
		System.out.println(securityCode);
		
		httpSession.setAttribute("securityCode", securityCode);
	}

	/**
	 * 退出
	 * @return
	 */
	@RequestMapping( value = { "signout" }, method = { RequestMethod.GET } )
	public @ResponseBody ResponseResult signout(HttpSession httpSession, String username) {
		LOGGER.info("username = " + username);
		
		ResponseResult responseResult = this.authorityWebService.signout(httpSession, username);
		
		return responseResult;
	}
	
	/**
	 * 取得客户端的真实 IP
	 * @param request
	 * @return
	 */
	private String getClientRealIP(HttpServletRequest request){  
        String ipAddress = request.getHeader("x-forwarded-for");  
            if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {  
                ipAddress = request.getHeader("Proxy-Client-IP");  
            }  
            if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {  
                ipAddress = request.getHeader("WL-Proxy-Client-IP");  
            }  
            if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {  
                ipAddress = request.getRemoteAddr();  
                if(ipAddress.equals("127.0.0.1") || ipAddress.equals("0:0:0:0:0:0:0:1")){  
                    //根据网卡取本机配置的IP  
                    InetAddress inet=null;  
                    try {  
                        inet = InetAddress.getLocalHost();  
                    } catch (UnknownHostException e) {  
                        e.printStackTrace();  
                    }  
                    ipAddress= inet.getHostAddress();  
                }  
            }  
            //对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割  
            if(ipAddress!=null && ipAddress.length()>15){ //"***.***.***.***".length() = 15  
                if(ipAddress.indexOf(",")>0){  
                    ipAddress = ipAddress.substring(0,ipAddress.indexOf(","));  
                }  
            }  
            return ipAddress;   
    }
	
	/**
	 * 注册
	 * @param username
	 * @param password
	 * @param captcha
	 * @param httpServletRequest
	 * @param httpSession
	 * @return
	 */
	@RequestMapping( value = { "registry" } )
	public @ResponseBody ResponseResult registry(String username, String password, String nickname, String captcha, HttpServletRequest httpServletRequest, HttpSession httpSession) {
		LOGGER.info("username = " + username);
		
		ResponseResult responseResult = this.authorityWebService.register(username, password, nickname, captcha, this.getClientRealIP(httpServletRequest), httpSession);
		
		return responseResult;
	}
}
