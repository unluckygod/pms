package com.artup.controller;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping( value = { "redis" } )
public class StringRedisController {
	protected static Logger LOGGER = LoggerFactory.getLogger(StringRedisController.class);

	@Autowired
	StringRedisTemplate stringRedisTemplate;
	
	@Resource( name = "stringRedisTemplate" )
	ValueOperations<String, String> valueOperations;
	
	@RequestMapping( value = "set" )
	public String setKeyAndValue(String key, String value){
		LOGGER.debug("key = {}, value = {}", key, value);
		this.valueOperations.set(key, value);
		
		return "set success";
	}
	
	@RequestMapping( value = "get" )
	public String getValue(String key) {
		LOGGER.debug("ke = {}", key);
		
		return this.valueOperations.get(key);
	}
	
	@RequestMapping("/second")
    String addMessage(String message) {
    	System.out.println(message);
    	
        return "这是我的第一个 Sping Boot Redis 应用程序。";
    }
}
