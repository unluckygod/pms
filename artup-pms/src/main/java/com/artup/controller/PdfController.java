package com.artup.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.artup.common.Constants;
import com.artup.common.ResponseResult;
import com.artup.service.impl.PdfServiceImpl;

/**
 * PDF 制作
 * @author hapday
 * @date 2017年8月1日 @Time 上午11:39:51
 */
@RestController
@RequestMapping( value = { "pdf" } )
public class PdfController {
	private static final Logger LOGGER = LoggerFactory.getLogger(PdfController.class);
	
	@Autowired
	private PdfServiceImpl pdfService;
	
	@RequestMapping(value = "createPdf")
	public ResponseResult createPdf(String orderCode) {
		LOGGER.info("orderCode = {}", orderCode);
		LOGGER.info("-------------- 创建 PDF 开始 --------------");
		this.pdfService.createPdf(orderCode);
		ResponseResult responseResult = new ResponseResult();
//		LOGGER.info("-------------- 创建 PDF 完成 --------------");
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("创建 PDF 开始...");
		return responseResult;
	}
	
	/**
	 * 查询【PDF 路径】列表
	 * @param passportId 通行证ID
	 * @param orderCode 订单号
	 * @return 【PDF 路径】列表
	 */
	@RequestMapping(value = { "queryPdfPathes" }, method = { RequestMethod.GET })
	public ResponseResult queryPdfPathes(Integer passportId, String orderCode){
		LOGGER.info("passportId = {}，orderCode = {}", passportId, orderCode);
		
		ResponseResult responseResult = this.pdfService.queryPdfPathList(passportId, orderCode);
		
		return responseResult;
	}
}
