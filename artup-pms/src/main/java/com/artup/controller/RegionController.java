package com.artup.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.artup.common.ResponseResult;
import com.artup.service.RegionService;

/**
 * 地区
 * @author hapday
 * @date 2017年7月13日 @Time 下午3:48:16
 */
@RestController
@RequestMapping( value = { "region" } )
public class RegionController {
	protected static final Logger LOGGER = LoggerFactory.getLogger(RegionController.class);
	
	@Autowired
	private RegionService regionService;
	
	/**
	 * 查询全部的地区
	 * @return
	 */
	@RequestMapping( value = { "queryAllRegion" } )
	public ResponseResult queryAllRegion() {
		ResponseResult responseResult = this.regionService.queryAllRegion();
		
		return responseResult;
	}

	/**
	 * 查询全部的地区
	 * @return
	 */
	@RequestMapping( value = { "queryAllRegion2" } )
	public ResponseResult queryAllRegion2() {
		ResponseResult responseResult = this.regionService.queryAllRegion2();
		
		return responseResult;
	}
}
