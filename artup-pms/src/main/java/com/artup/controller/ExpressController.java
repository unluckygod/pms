package com.artup.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.artup.common.ResponseResult;
import com.artup.service.ExpressService;

import lombok.extern.slf4j.Slf4j;

/**
 * 物流
 * @author hapday
 * @date 2017年8月2日 @Time 上午11:02:54
 */
@RestController
@RequestMapping( value = "express" )
@Slf4j
public class ExpressController {
	@Autowired
	private ExpressService expressService;
	
	/**
	 * 查询【快递列表】
	 * @param orderCode 订单号
	 * @return 【快递列表】
	 */
	@RequestMapping( value = "queryExpress/{orderCode}", method = { RequestMethod.GET } )
	public ResponseResult queryExpress(@PathVariable String orderCode) {
		log.info("orderCode = {}", orderCode);
		
		ResponseResult responseResult = this.expressService.queryExpress(orderCode);
		
		return responseResult;
	}
}
