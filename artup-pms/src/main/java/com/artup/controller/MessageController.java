package com.artup.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping( value = { "message" } )
public class MessageController {
	protected static Logger LOGGER = LoggerFactory.getLogger(MessageController.class);  

    @RequestMapping("/first")
    String addMessage(String message) {
    	System.out.println(message);
    	
        return "这是我的第一个 Sping Boot 应用程序。";
    }

    @RequestMapping("message/{content}")
    String sendMessage(@PathVariable String content) {
    	LOGGER.info("content = {}", content);
    	
    	return content;
    }
}