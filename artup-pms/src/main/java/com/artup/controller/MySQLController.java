package com.artup.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.artup.domain.Person;
import com.artup.repository.PersonRepository;

@RestController
@RequestMapping( value = { "mysql" } )
public class MySQLController {
	protected static Logger LOGGER = LoggerFactory.getLogger(MySQLController.class);
	
	@Autowired
	PersonRepository personPespository;
	
	@RequestMapping( value = "save" )
	public Person save(Person person){
		Person result = this.personPespository.save(person);
		
		return result;
	}
	
	@RequestMapping( value = "query" )
	public List<Person> query(Person person){
		List<Person> personList = this.personPespository.selectPersonByNameAndAddress(person.getName(), person.getAddress());
		
		return personList;
	}

	@RequestMapping( value = "queryPersonByName" )
	public List<Person> queryPersonByName(String name){
		LOGGER.debug("name = {}", name);
		
		List<Person> personList = this.personPespository.findByName(name);
		
		return personList;
	}

	@RequestMapping( value = "queryPersonByAddress" )
	public List<Person> queryPersonByAddress(String address){
		LOGGER.debug("address = {}", address);
		
		List<Person> personList = this.personPespository.findByAddress(address);
		
		return personList;
	}

	@RequestMapping( value = "queryPersonByNameAndAddress" )
	public List<Person> queryPersonByNameAndAddress(String name, String address){
		LOGGER.debug("name = {}, address = {}", name, address);
		
		List<Person> personList = null;
		try {
			personList = this.personPespository.findByNameAndAddress(name, address);
		} catch (Exception e) {
			LOGGER.error("根据【姓名】和【地址】查询【人员列表】 - 失败！", e);
		}
		
		return personList;
	}
}
