package com.artup.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.artup.common.ResponseResult;
import com.artup.pojo.Role;
import com.artup.service.RoleService;

/**
 * 角色管理
 * @author hapday
 * @date 2017年3月8日 @Time 下午2:37:34
 */
@Controller
@RequestMapping( value = { "admin/role" } )
public class RoleController {
	private static final Logger LOGGER = LoggerFactory.getLogger(RoleController.class);
	
	@Autowired
	private RoleService roleService;

	/**
	 * 根据【ID】查询【角色】
	 * @param id
	 * @return
	 */
	@RequestMapping( value = { "queryRole/{id}" } )
	public @ResponseBody ResponseResult queryRole(@PathVariable Long id) {
		LOGGER.info("id = {}", id);

		ResponseResult responseResult = roleService.queryRoleById(id);
        
		return responseResult;
	}

	/**
	 * 删除【角色】
	 * @param role
	 * @return
	 */
	@RequestMapping( value = { "deleteRole" }, method = { RequestMethod.POST, RequestMethod.GET } )
	public @ResponseBody ResponseResult deleteRole(long id) {
		LOGGER.info("id = " + id);
		
		ResponseResult responseResult = this.roleService.deleteRoleById(id);
		
		return responseResult;
	}
	
	/**
	 * 转到【角色添加】页面
	 * @return
	 */
	@RequestMapping( value = { "addRole" }, method = { RequestMethod.GET } )
	public String addRole() {
		return "role/roleAdd";
	}
	
	/**
	 * 保存【角色】
	 * @param role
	 * @return
	 */
//	@RequestMapping( value = { "saveRole" }, produces = "application/json; charset=utf-8" )
	@RequestMapping( value = { "saveRole" } )
	public @ResponseBody ResponseResult saveRole(Role role) {
		LOGGER.info("role = " + JSONObject.toJSONString(role));
		
		ResponseResult responseResult = this.roleService.saveRole(role);
		
		return responseResult;
	}

	/**
	 * 转到【角色列表】页面
	 * @return
	 */
	@RequestMapping( value = { "roleList" }, method = { RequestMethod.GET } )
	public String roleList() {
		return "role/roleList";
	}
	
	/**
	 * 模糊查询【角色列表】
	 * @param role
	 * @return
	 */
	@RequestMapping( value = { "queryRoles" } )
	public @ResponseBody ResponseResult queryRoles(
			@ModelAttribute("role") Role role) {
		LOGGER.info("role = {}", JSONObject.toJSONString(role));
		
		ResponseResult responseResult = roleService.queryRoleList(role);
		
		return responseResult;
	}
	
	/**
	 * 根据【ID】查询【角色】
	 * @param id
	 * @return
	 */
	@RequestMapping( value = { "roleDetail/{id}" } )
	public String roleDetail( @PathVariable( name = "id" ) Long id, Model model ) {
		LOGGER.info("id = " + id);

		ResponseResult responseResult = roleService.queryRoleById(id);
		model.addAttribute("role", responseResult.getData());
        
		return "role/roleDetail";
	}
	
	/**
	 * 转到【角色编辑】页面
	 * @return
	 */
	@RequestMapping( value = { "editRole/{id}" }, method = { RequestMethod.GET } )
	public String editRole( @PathVariable( name = "id" ) Long id, Model model ) {
		LOGGER.info("id = " + id);

		ResponseResult responseResult = roleService.queryRoleById(id);
		model.addAttribute("role", responseResult.getData());
		
		return "role/roleEdit";
	}
	
	/**
	 * 更新【角色】
	 * @param role
	 * @return
	 */
	@RequestMapping( value = { "updateRole" }, method = { RequestMethod.POST, RequestMethod.GET } )
	public @ResponseBody ResponseResult updateRole(Role role) {
		LOGGER.info("role = " + JSONObject.toJSONString(role));
		
		ResponseResult responseResult = this.roleService.updateRoleById(role);
		
		return responseResult;
	}
}
