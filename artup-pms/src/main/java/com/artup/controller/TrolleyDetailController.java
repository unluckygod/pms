package com.artup.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.artup.common.ResponseResult;
import com.artup.pojo.TrolleyDetail;
import com.artup.service.TrolleyDetailService;

/**
 * 购物车详情
 * @author hapday
 * @date 2017年7月24日 @Time 下午5:30:12
 */
@RestController
@RequestMapping( value = { "trolleyDetail" } )
public class TrolleyDetailController {
	protected static final Logger LOGGER = LoggerFactory.getLogger(TrolleyDetailController.class);

	@Autowired
	private TrolleyDetailService trolleyDetailService;
	
	/**
	 * 根据【IDs】更新【购物车详情】的【状态】
	 * @param passportId 【通行证ID】
	 * @param ids 【IDs】
	 * @return
	 */
	@RequestMapping( value = { "batchUpdateTrolleyDetailStatus/{passportId}" } )
	public ResponseResult batchUpdateTrolleyDetailStatus(@PathVariable Integer passportId, String ids) {
		LOGGER.info("passportId = {}, ids = {}", passportId, ids);
		
		ResponseResult responseResult = this.trolleyDetailService.batchUpdateTrolleyDetailStatusByIds(passportId, ids);
		
		return responseResult;
	}
	
	/**
	 * 添加购物车详情
	 * @param trolleyDetail 购物车详情
	 * @return
	 */
	@RequestMapping( value = { "saveTrolleyDetail" } )
	public ResponseResult saveTrolley(TrolleyDetail trolleyDetail) {
		LOGGER.info("trolleyDetail = {}", JSONObject.toJSONString(trolleyDetail));
		
		ResponseResult responseResult = this.trolleyDetailService.saveTrolley(trolleyDetail);
		
		return responseResult;
	}
	
	/**
	 * 更新【购物车详情】中【作品数量】
	 * @param id 【ID】
	 * @param quantity 【作品数量】
	 * @return
	 */
	@RequestMapping( value = { "updateTrolleyDetailQuantity/{id}" }, method = { RequestMethod.GET } )
	public ResponseResult updateTrolleyDetailQuantity(@PathVariable String id, int quantity) {
		LOGGER.info("id = {}", id);
		
		ResponseResult responseResult = this.trolleyDetailService.updateTrolleyDetailQuantityById(id, quantity);
		
		return responseResult;
	}
}
