package com.artup.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.artup.common.Constants;
import com.artup.common.ResponseResult;
import com.artup.pojo.Resource;
import com.artup.pojo.RoleResource;
import com.artup.service.RoleResourceService;

/**
 * 角色资源管理
 * @author hapday
 * @date 2017年3月8日 @Time 下午2:37:34
 */
@Controller
@RequestMapping( value = { "admin/roleResource" } )
public class RoleResourceController {
	private static final Logger LOGGER = LoggerFactory.getLogger(RoleResourceController.class);
	
	@Autowired
	private RoleResourceService roleResourceService;

	/**
	 * 根据【ID】查询【角色资源】
	 * @param id
	 * @return
	 */
	@RequestMapping( value = { "queryRoleResource" } )
	public @ResponseBody ResponseResult queryRoleResource(Long id) {
		LOGGER.info("id = " + id);

		ResponseResult responseResult = roleResourceService.queryRoleResourceById(id);
        
		return responseResult;
	}

	/**
	 * 删除【角色资源】
	 * @param roleResource
	 * @return
	 */
	@RequestMapping( value = { "deleteRoleResource" }, method = { RequestMethod.POST, RequestMethod.GET } )
	public @ResponseBody ResponseResult deleteRoleResource(long id) {
		LOGGER.info("id = " + id);
		
		ResponseResult responseResult = this.roleResourceService.deleteRoleResourceById(id);
		
		return responseResult;
	}
	
	/**
	 * 转到【角色资源添加】页面
	 * @return
	 */
	/*@SuppressWarnings("unchecked")
	@RequestMapping( value = { "queryRoleResourcesByRoleId" }, method = { RequestMethod.GET } )
	public String queryRoleResourcesByRoleId( Long roleId, Model model ) {
		LOGGER.info("roleId = " + roleId);

		ResponseResult responseResult = this.roleResourceService.queryResourceListByRoleId(roleId);
		
		if(null != responseResult && Constants.ACTION_STATUS_SUCCESS == responseResult.getStatus()) {
			List<RoleResource> roleResources = (List<RoleResource>) responseResult.getData();

			model.addAttribute("roleResources", roleResources);
		}
		
		return "roleResource/roleResourceAdd";
	}*/
	
	/**
	 * 保存【角色资源】
	 * @param roleResource
	 * @return
	 */
//	@RequestMapping( value = { "saveRoleResource" }, produces = "application/json; charset=utf-8" )
	@RequestMapping( value = { "saveRoleResource" } )
	public @ResponseBody ResponseResult saveRoleResource(RoleResource roleResource) {
		LOGGER.info("roleResource = {}", JSONObject.toJSONString(roleResource));
		
		ResponseResult responseResult = this.roleResourceService.saveRoleResource(roleResource);
		
		return responseResult;
	}

	/**
	 * 转到【角色资源列表】页面
	 * @return
	 */
	@RequestMapping( value = { "roleResourceList" }, method = { RequestMethod.GET } )
	public String roleResourceList() {
		return "roleResource/roleResourceList";
	}
	
	/**
	 * 模糊查询【角色资源列表】
	 * @param roleResource
	 * @return
	 */
	@RequestMapping( value = { "queryRoleResources" } )
	public @ResponseBody ResponseResult queryRoleResources(
			@ModelAttribute("roleResource") RoleResource roleResource,
			Long roleId
			, Model model
			, @RequestParam(required = false) Integer start,
			@RequestParam(required = false) Integer length) {
		LOGGER.info("roleResource = {}", JSONObject.toJSONString(roleResource));
		
		if(null == start) 
			roleResource.setPageIndex(0);
		else
			roleResource.setPageIndex(start);
		if(null == length)
			roleResource.setPageSize(10);
		else
			roleResource.setPageSize(length);
		
		ResponseResult responseResult = roleResourceService.queryRoleResourceList(roleResource);
		
		return responseResult;
	}
	
	/**
	 * 模糊查询【角色列表】
	 * @param resource
	 * @return
	 */
	@RequestMapping( value = { "queryResources" } )
	public @ResponseBody ResponseResult queryResources(
			@ModelAttribute("resource") Resource resource
			, Model model
			, Long roleId
			, @RequestParam(required = false) Integer start,
			@RequestParam(required = false) Integer length) {
		LOGGER.info("resource = " + JSONObject.toJSONString(resource) + ", roleId = " + roleId);
		
		if(null == start) 
			resource.setPageIndex(0);
		else
			resource.setPageIndex(start);
		if(null == length)
			resource.setPageSize(10);
		else
			resource.setPageSize(length);
		
		ResponseResult responseResult = roleResourceService.queryResourceList(resource, roleId);
		
		return responseResult;
	}
	
	/**
	 * 根据【ID】查询【角色资源】
	 * @param id
	 * @return
	 */
	@RequestMapping( value = { "roleResourceDetail/{id}" } )
	public String roleResourceDetail( @PathVariable( name = "id" ) Long id, Model model ) {
		LOGGER.info("id = " + id);

		ResponseResult responseResult = roleResourceService.queryRoleResourceById(id);
		model.addAttribute("roleResource", responseResult.getData());
        
		return "roleResource/roleResourceDetail";
	}
	
	/**
	 * 转到【角色资源编辑】页面
	 * @return
	 */
	@RequestMapping( value = { "editRoleResource/{id}" }, method = { RequestMethod.GET } )
	public String editRoleResource( @PathVariable( name = "id" ) Long id, Model model ) {
		LOGGER.info("id = " + id);

		ResponseResult responseResult = roleResourceService.queryRoleResourceById(id);
		if(Constants.ACTION_STATUS_SUCCESS == responseResult.getStatus() && null != responseResult.getData()) {
			model.addAttribute("roleResource", responseResult.getData());
		}
		
		/*responseResult = this.roleResourceService.queryAllRoleResourceList();
		if(Constants.ACTION_STATUS_SUCCESS == responseResult.getStatus() && null != responseResult.getData()) {
			model.addAttribute("roleResources", responseResult.getData());
		}*/
		
		return "roleResource/roleResourceEdit";
	}
	
	/**
	 * 更新【角色资源】
	 * @param roleResource
	 * @return
	 */
	@RequestMapping( value = { "updateRoleResource" }, method = { RequestMethod.POST, RequestMethod.GET } )
	public @ResponseBody ResponseResult updateRoleResource(RoleResource roleResource) {
		LOGGER.info("roleResource = {}", JSONObject.toJSONString(roleResource));
		
		ResponseResult responseResult = this.roleResourceService.updateRoleResourceById(roleResource);
		
		return responseResult;
	}
	
	/**
	 * 转到【分配角色资源】页面
	 * @return
	 */
	@RequestMapping( value = { "allocateResource/{roleId}" }, method = { RequestMethod.GET } )
	public String allocateResource( @PathVariable( name = "roleId" ) Long roleId, Model model ) {
		LOGGER.info("roleId = " + roleId);

		model.addAttribute("roleId", roleId);
		ResponseResult responseResult = this.roleResourceService.queryResourceListByRoleId( roleId );
		model.addAttribute("resources", responseResult.getData());
		
		return "role/allocateResource";
	}

	/**
	 * 根据【角色ID】更新【角色资源】
	 * @param roleId
	 * @param roleIds
	 * @return
	 */
	@RequestMapping( value = { "updateRoleResourceByRoleId" }, method = { RequestMethod.POST } )
	public @ResponseBody ResponseResult updateRoleResourceByRoleId(long roleId, String resourceIds) {
		LOGGER.info("roleId = " + roleId + ", resourceIds = " + resourceIds);
		
		ResponseResult responseResult = this.roleResourceService.updateRoleResourceByRoleId(roleId, resourceIds);
		
		return responseResult;
	}

	/**
	 * 更新【角色资源】为无效的
	 * @param id
	 * @return
	 */
	@RequestMapping( value = { "updateRoleResourceInvalid/{roleId}/{id}" }, method = { RequestMethod.POST } )
	public @ResponseBody ResponseResult updateRoleResourceInvalid( 
			@PathVariable( name = "roleId" ) Long roleId
			, @PathVariable( name = "id" ) Long id ) {
		LOGGER.info("id = " + id + ", roleId = " + roleId);
		
		ResponseResult responseResult = this.roleResourceService.updateRoleResourceInvalidById(id, roleId);
		
		return responseResult;
	}
}
