package com.artup.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.artup.common.Constants;
import com.artup.common.ResponseResult;
import com.artup.pojo.Role;
import com.artup.pojo.UserRole;
import com.artup.service.UserRoleService;

/**
 * 用户角色管理
 * @author hapday
 * @date 2017年3月8日 @Time 下午2:37:34
 */
@Controller
@RequestMapping( value = { "admin/userRole" } )
public class UserRoleController {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserRoleController.class);
	
	@Autowired
	private UserRoleService userRoleService;

	/**
	 * 根据【ID】查询【用户角色】
	 * @param id
	 * @return
	 */
	@RequestMapping( value = { "queryUserRole" } )
	public @ResponseBody ResponseResult queryUserRole(Long id) {
		LOGGER.info("id = " + id);

		ResponseResult responseResult = userRoleService.queryUserRoleById(id);
        
		return responseResult;
	}

	/**
	 * 删除【用户角色】
	 * @param userRole
	 * @return
	 */
	@RequestMapping( value = { "deleteUserRole" }, method = { RequestMethod.POST, RequestMethod.GET } )
	public @ResponseBody ResponseResult deleteUserRole(long id) {
		LOGGER.info("id = " + id);
		
		ResponseResult responseResult = this.userRoleService.deleteUserRoleById(id);
		
		return responseResult;
	}
	
	/**
	 * 转到【用户角色添加】页面
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping( value = { "queryUserRolesByUserId" }, method = { RequestMethod.GET } )
	public String queryUserRolesByUserId( Long userId, Model model ) {
		LOGGER.info("userId = " + userId);

		ResponseResult responseResult = this.userRoleService.queryUserRoleListByUserId(userId);
		
		if(null != responseResult && Constants.ACTION_STATUS_SUCCESS == responseResult.getStatus()) {
			List<UserRole> userRoles = (List<UserRole>) responseResult.getData();

			model.addAttribute("userRoles", userRoles);
		}
		
		return "userRole/userRoleAdd";
	}
	
	/**
	 * 保存【用户角色】
	 * @param userRole
	 * @return
	 */
//	@RequestMapping( value = { "saveUserRole" }, produces = "application/json; charset=utf-8" )
	@RequestMapping( value = { "saveUserRole" } )
	public @ResponseBody ResponseResult saveUserRole(UserRole userRole) {
		LOGGER.info("userRole = {}", JSONObject.toJSONString(userRole));
		
		ResponseResult responseResult = this.userRoleService.saveUserRole(userRole);
		
		return responseResult;
	}

	/**
	 * 转到【用户角色列表】页面
	 * @return
	 */
	@RequestMapping( value = { "userRoleList" }, method = { RequestMethod.GET } )
	public String userRoleList() {
		return "userRole/userRoleList";
	}
	
	/**
	 * 模糊查询【用户角色列表】
	 * @param userRole
	 * @return
	 */
	@RequestMapping( value = { "queryUserRoles" } )
	public @ResponseBody ResponseResult queryUserRoles(
			@ModelAttribute("userRole") UserRole userRole,
			Long userId
			, Model model
			, @RequestParam(required = false) Integer start,
			@RequestParam(required = false) Integer length) {
		LOGGER.info("userRole = {}", JSONObject.toJSONString(userRole));
		
		if(null == start) 
			userRole.setPageIndex(0);
		else
			userRole.setPageIndex(start);
		if(null == length)
			userRole.setPageSize(10);
		else
			userRole.setPageSize(length);
		
		ResponseResult responseResult = userRoleService.queryUserRoleList(userRole);
		
		return responseResult;
	}
	
	/**
	 * 模糊查询【角色列表】
	 * @param role
	 * @return
	 */
	/*@RequestMapping( value = { "queryRoles" } )
	public @ResponseBody ResponseResult queryRoles(
			@ModelAttribute("role") Role role
			, Long userId
			, @RequestParam(required = false) Integer start,
			@RequestParam(required = false) Integer length) {
		LOGGER.info("role = {}, userId = {}", JSONObject.toJSONString(role), userId);
		
		if(null == start) 
			role.setPageIndex(0);
		else
			role.setPageIndex(start);
		if(null == length)
			role.setPageSize(10);
		else
			role.setPageSize(length);
		
		ResponseResult responseResult = userRoleService.queryRoleList(role, userId);
		
		return responseResult;
	}*/
	
	/**
	 * 根据【ID】查询【用户角色】
	 * @param id
	 * @return
	 */
	@RequestMapping( value = { "userRoleDetail/{id}" } )
	public String userRoleDetail( @PathVariable( name = "id" ) Long id, Model model ) {
		LOGGER.info("id = " + id);

		ResponseResult responseResult = userRoleService.queryUserRoleById(id);
		model.addAttribute("userRole", responseResult.getData());
        
		return "userRole/userRoleDetail";
	}
	
	/**
	 * 转到【用户角色编辑】页面
	 * @return
	 */
	@RequestMapping( value = { "editUserRole/{id}" }, method = { RequestMethod.GET } )
	public String editUserRole( @PathVariable( name = "id" ) Long id, Model model ) {
		LOGGER.info("id = " + id);

		ResponseResult responseResult = userRoleService.queryUserRoleById(id);
		if(Constants.ACTION_STATUS_SUCCESS == responseResult.getStatus() && null != responseResult.getData()) {
			model.addAttribute("userRole", responseResult.getData());
		}
		
		/*responseResult = this.userRoleService.queryAllUserRoleList();
		if(Constants.ACTION_STATUS_SUCCESS == responseResult.getStatus() && null != responseResult.getData()) {
			model.addAttribute("userRoles", responseResult.getData());
		}*/
		
		return "userRole/userRoleEdit";
	}
	
	/**
	 * 更新【用户角色】
	 * @param userRole
	 * @return
	 */
	@RequestMapping( value = { "updateUserRole" }, method = { RequestMethod.POST, RequestMethod.GET } )
	public @ResponseBody ResponseResult updateUserRole(UserRole userRole) {
		LOGGER.info("userRole = {}", JSONObject.toJSONString(userRole));
		
		ResponseResult responseResult = this.userRoleService.updateUserRoleById(userRole);
		
		return responseResult;
	}
	
	/**
	 * 转到【分配用户角色】页面
	 * @return
	 */
	@RequestMapping( value = { "allocateRole/{userId}" }, method = { RequestMethod.GET } )
	public String allocateRole( @PathVariable( name = "userId" ) Long userId, Model model ) {
		LOGGER.info("userId = " + userId);

		model.addAttribute("userId", userId);
		ResponseResult responseResult = this.userRoleService.queryUserRoleListByUserId( userId );
		model.addAttribute("userRoles", responseResult.getData());
		
		return "user/allocateRole";
	}

	/**
	 * 根据【用户ID】更新【用户角色】
	 * @param userId
	 * @param roleIds
	 * @return
	 */
	@RequestMapping( value = { "updateUserRoleByUserId" }, method = { RequestMethod.POST } )
	public @ResponseBody ResponseResult updateUserRoleByUserId(long userId, String roleIds) {
		LOGGER.info("userId = {}, roleIds = {}", userId, roleIds);
		
		ResponseResult responseResult = this.userRoleService.updateUserRoleByUserId(userId, roleIds);
		
		return responseResult;
	}

	/**
	 * 更新【用户角色】为无效的
	 * @param id
	 * @return
	 */
	@RequestMapping( value = { "updateUserRoleInvalid/{userId}/{id}" }, method = { RequestMethod.POST } )
	public @ResponseBody ResponseResult updateUserRoleInvalid( 
			@PathVariable( name = "userId" ) Long userId
			, @PathVariable( name = "id" ) Long id ) {
		LOGGER.info("id = " + id + ", userId = " + userId);
		
		ResponseResult responseResult = this.userRoleService.updateUserRoleInvalidById(id, userId);
		
		return responseResult;
	}
	
	
	
	
	@RequestMapping( value = { "queryRoles" } )
	public @ResponseBody ResponseResult queryRoles(
			@ModelAttribute("role") Role role, Long userId) {
		LOGGER.info("role = {}, userId = {}", JSONObject.toJSONString(role), userId);
		
		ResponseResult responseResult = userRoleService.queryRoleList(role, userId);
		
		return responseResult;
	}
}
