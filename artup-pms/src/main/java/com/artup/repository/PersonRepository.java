package com.artup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.artup.domain.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {
	/**
	 * 根据“姓名”查询【人员】
	 * @param name 姓名
	 * @return 人员
	 */
	List<Person> findByName(String name);
	
	/**
	 * 根据【地址】查询【人员】
	 * @param address 地址
	 * @return 人员
	 */
	List<Person> findByAddress(String address);
	
	List<Person> findByNameAndAddress(String name, String address) throws Exception;
	
//	@Query( "SELECT p FROM Person p" )
	@Query( "SELECT p FROM Person p WHERE p.name=:name AND p.address=:address" )
	List<Person> selectPersonByNameAndAddress(@Param("name") String name, @Param("address") String address);
}
