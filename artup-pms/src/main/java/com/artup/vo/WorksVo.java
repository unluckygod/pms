package com.artup.vo;

/**
 * 作品
 * @author hapday
 * @date 2017年7月14日 @Time 下午6:14:24
 */
public class WorksVo {
	/**
	 * 
	 */
	private String id;	// ID
	private int passportId;		// 通行证ID
	private String name;	// 作品名称
	private float price;	// 作品单价
	private int quantity;	// 数量
	private String size;	// 作品尺寸
	private String color;	// 颜色
	private String shape;	// 框型
	private String typeCode;	// 类型编号
	private String typeName;	// 类型名称
	private String createTime;	// 创建时间
	private String finishTime;	// 完成时间
	private byte status;	// 状态（1-定制中；2-已完成）
	private String statusName;	// 状态名（1-定制中；2-已完成）
	private String skuCode;		// SKU
	private String thumbnailId;		// 缩略图ID
	private String thumbnailPath;	// 缩略图
	private String sku;	// SKU
	private String clientCode;	// 客户端编号
	
	public WorksVo() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getPassportId() {
		return passportId;
	}

	public void setPassportId(int passportId) {
		this.passportId = passportId;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getTypeName() {
		return typeName;
	}
	
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(String finishTime) {
		this.finishTime = finishTime;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public String getStatusName() {
		return statusName;
	}
	
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getSkuCode() {
		return skuCode;
	}

	public void setSkuCode(String skuCode) {
		this.skuCode = skuCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getShape() {
		return shape;
	}

	public void setShape(String shape) {
		this.shape = shape;
	}

	public String getThumbnailId() {
		return thumbnailId;
	}

	public void setThumbnailId(String thumbnailId) {
		this.thumbnailId = thumbnailId;
	}

	public String getThumbnailPath() {
		return thumbnailPath;
	}

	public void setThumbnailPath(String thumbnailPath) {
		this.thumbnailPath = thumbnailPath;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getClientCode() {
		return clientCode;
	}
	
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
}
