package com.artup.vo;

/**
 * 购物车
 * @author hapday
 * @date 2017年7月17日 @Time 下午12:06:02
 */
public class TrolleyVo {
	private String worksName;	// 作品名称
	private float worksPrice;	// 作品单价
	private String worksSize;	// 作品尺寸
	
	public TrolleyVo() {
	}
	
	public String getWorksName() {
		return worksName;
	}
	public void setWorksName(String worksName) {
		this.worksName = worksName;
	}
	public float getWorksPrice() {
		return worksPrice;
	}
	public void setWorksPrice(float worksPrice) {
		this.worksPrice = worksPrice;
	}
	public String getWorksSize() {
		return worksSize;
	}
	public void setWorksSize(String worksSize) {
		this.worksSize = worksSize;
	}
}
