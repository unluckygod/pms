package com.artup.vo;

/**
 * 支付流水
 * @author hapday
 * @date 2017年7月26日 @Time 下午2:51:40
 */
public class PaymentVo {
	private String id;	// ID
	private String orderId;		// 订单ID
	private String type;	// 支付类型：wechat-微信；alipay-支付宝
	private String typeName;	// 支付类型名：微信；支付宝
	private String remark;		// 备注
	private String payTime;	// 支付时间
	private String payFinishDate;		// 支付完成时间
	private byte status;	// 状态：1-待支付；2-已支付；
	private String statusName;	// 状态名：待支付；已支付；
	
	public PaymentVo() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTypeName() {
		return typeName;
	}
	
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getPayTime() {
		return payTime;
	}

	public void setPayTime(String payTime) {
		this.payTime = payTime;
	}

	public String getPayFinishDate() {
		return payFinishDate;
	}

	public void setPayFinishDate(String payFinishDate) {
		this.payFinishDate = payFinishDate;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public String getStatusName() {
		return statusName;
	}
	
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
}
