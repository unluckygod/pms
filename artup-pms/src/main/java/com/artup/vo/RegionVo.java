package com.artup.vo;

import java.util.List;

/**
 * 地区
 * @author hapday
 * @date 2017年7月13日 @Time 下午3:35:26
 */
public class RegionVo {
	private long id;	// ID
	private long parentId;	// 父ID
	private byte type;	// 类型：1-省份；2-地市；3-区县
	private String name;	// 名称
	private List<RegionVo> subregionVo;		// 下级地区列表
	
	public RegionVo() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getParentId() {
		return parentId;
	}

	public void setParentId(long parentId) {
		this.parentId = parentId;
	}

	public byte getType() {
		return type;
	}

	public void setType(byte type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<RegionVo> getSubregionVo() {
		return subregionVo;
	}

	public void setSubregionVo(List<RegionVo> subregionVo) {
		this.subregionVo = subregionVo;
	}
}
