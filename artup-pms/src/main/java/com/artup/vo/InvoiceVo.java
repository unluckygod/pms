package com.artup.vo;

/**
 * 发票
 * @author hapday
 * @date 2017年8月1日 @Time 上午11:11:13
 */
public class InvoiceVo {
	private Long id;	// ID
	private Integer passportId;		// 通行证ID
	private String title;	// 抬头
	private String content;		// 内容
	
	public InvoiceVo() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPassportId() {
		return passportId;
	}

	public void setPassportId(Integer passportId) {
		this.passportId = passportId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
