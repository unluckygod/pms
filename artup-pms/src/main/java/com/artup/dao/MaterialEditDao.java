package com.artup.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.artup.pojo.MaterialEdit;

/**
 * 素材编辑
 * @author hapday
 * @date 2017年7月17日 @Time 下午6:57:53
 */
@Repository
public interface MaterialEditDao {
	/**
	 * 添加【素材编辑】
	 * @param materialEdit 【素材编辑】
	 * @throws Exception
	 */
	@Insert( value = { "INSERT INTO bud_pic_editor (db_id, edt_db_id, pic_db_id, page, num, status, picture_absolute_path, thumbnail_image_url, action_json, create_time, update_time)"
			+ " VALUES (#{id, javaType=String, jdbcType=VARCHAR}, #{worksId, javaType=String, jdbcType=VARCHAR}, #{materialId, javaType=String, jdbcType=VARCHAR}"
			+ ", #{pageIndex, javaType=Integer, jdbcType=INTEGER}, #{materialIndex, javaType=Integer, jdbcType=INTEGER}, #{status, javaType=Byte, jdbcType=TINYINT}"
			+ ", #{editedMaterialPath, javaType=String, jdbcType=VARCHAR}, #{editedMaterialThumbnailPath, javaType=String, jdbcType=VARCHAR}, #{action, javaType=String, jdbcType=VARCHAR}, NOW(), NOW())" } )
	void insertMaterialEdit(MaterialEdit materialEdit) throws Exception;
	
	/**
	 * 根据【作品ID】查询【素材编辑】列表
	 * @param worksId 【作品ID】
	 * @return 【素材编辑】列表
	 * @throws SQLException
	 */
	@Select( value = { "SELECT e.db_id AS id, e.num AS pageIndex, e.picture_absolute_path AS editedMaterialPath, e.action_json AS action FROM bud_pic_editor e WHERE e.status = 1 AND e.edt_db_id = #{worksId, javaType=String, jdbcType=VARCHAR}" } )
	List<MaterialEdit> selectMaterialEditByWorksId(String worksId) throws SQLException;
	
	/**
	 * 根据【素材IDs】查询【素材编辑数】
	 * @param materialIds 【素材IDs】
	 * @return 【素材编辑数】
	 * @throws SQLException
	 */
	@Select( value = { "<script>"
			+ "SELECT COUNT(1) FROM bud_pic_editor me WHERE me.pic_db_id IN"
				+ "<foreach collection='materialIds' item='materialId' open='(' close=')' separator=','>"
					+ "#{materialId, javaType=String, jdbcType=VARCHAR}"
				+ "</foreach>"
			+ " </script>" } )
	int selectMaterialEditCountByMaterialIds(@Param("materialIds") String [] materialIds) throws SQLException;
	
	/**
	 * 根据【作品IDs】批量删除【素材编辑】
	 * @param worksIds 【作品IDs】
	 * @throws SQLException
	 */
	@Select( value = { "<script>"
			+ "DELETE FROM bud_pic_editor"
			+ " WHERE EDT_DB_ID IN "
				+ "<foreach collection='worksIds' item='worksId' open='(' close=')' separator=','>"
					+ "#{worksId, javaType=String, jdbcType=VARCHAR}"
				+ "</foreach>"
			+ " </script>" } )
	void batchDeleteMaterialEditByWorksIds(@Param("worksIds") String [] worksIds) throws SQLException;
}
