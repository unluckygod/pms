package com.artup.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.artup.pojo.Payment;

/**
 * 支付流水
 * @author hapday
 * @date 2017年7月26日 @Time 下午3:04:31
 */
@Repository
public interface PaymentDao {
	/**
	 * 添加【支付流水】
	 * @param payment
	 * @throws SQLException
	 */
	@Insert( value = { "INSERT INTO bud_order_payment"
			+ " (db_id, order_db_id, type, description, create_time, status)"
			+ " VALUES (#{id, javaType=String, jdbcType=VARCHAR}, #{orderId, javaType=String, jdbcType=VARCHAR}"
			+ ", #{type, javaType=String, jdbcType=VARCHAR}, #{remark, javaType=String, jdbcType=VARCHAR}"
			+ ", now(), #{status, javaType=Byte, jdbcType=TINYINT})" } )
	void insertPayment(Payment payment) throws SQLException;

	/**
	 * 根据【ID】更新【支付流水】
	 * @param payment
	 * @throws SQLException
	 */
	@Update( value = { "UPDATE bud_order_payment p"
			+ " SET p.type = #{type, javaType=String, jdbcType=VARCHAR}, p.pay_finished_time = now()"
			+ ", p.status = #{status, javaType=Byte, jdbcType=TINYINT}"
			+ " WHERE p.db_id = #{id, javaType=String, jdbcType=VARCHAR}" } )
	void updatePaymentById(Payment payment) throws SQLException;

	/**
	 * 根据【订单ID】更新【支付流水】
	 * @param payment
	 * @throws SQLException
	 */
	@Update( value = { "UPDATE bud_order_payment p"
			+ " SET p.type = #{type, javaType=String, jdbcType=VARCHAR}, p.pay_finished_time = now()"
			+ ", p.status = #{status, javaType=Byte, jdbcType=TINYINT}"
			+ " WHERE p.order_db_id = #{orderId, javaType=String, jdbcType=VARCHAR}" } )
	void updatePaymentByOrderId(Payment payment) throws SQLException;

	/**
	 * 根据【订单编号】更新【支付流水】
	 * @param payment
	 * @throws SQLException
	 */
	@Update( value = { "UPDATE bud_order_payment p"
			+ " SET p.type = #{type, javaType=String, jdbcType=VARCHAR}, p.pay_finished_time = now()"
			+ ", p.status = #{status, javaType=Byte, jdbcType=TINYINT}, p.total_price = #{totalPrice, javaType=Float, jdbcType=FLOAT}"
			+ " WHERE p.order_db_id = (SELECT db_id AS orderId FROM bud_order o WHERE o.code = #{orderCode, javaType=String, jdbcType=VARCHAR})" } )
	void updatePaymentByOrderCode(Payment payment) throws SQLException;
	
	/**
	 * 根据【订单ID】查询【支付流水】
	 * @param orderId 【订单ID】
	 * @return 【支付流水】
	 * @throws SQLException
	 */
	@Select( value = { "SELECT op.db_id AS id, op.order_db_id AS orderId, op.type, op.status, op.total_price AS totalPrice, op.pay_time AS payTime, op.pay_finished_time AS payFinishTime, op.description AS remark FROM bud_order_payment op WHERE op.order_db_id = #{orderId, javaType=String, jdbcType=VARCHAR}" } )
	Payment selectPaymentByOrderId(String orderId) throws SQLException;

	/**
	 * 根据【订单IDs】查询【支付流水】列表
	 * @param orderIds 【订单IDs】
	 * @return 【支付流水】列表
	 * @throws SQLException
	 */
	@Select( value = { "<script>SELECT op.db_id AS id, op.order_db_id AS orderId, op.type, op.status, op.total_price AS totalPrice, op.pay_time AS payTime, op.pay_finished_time AS payFinishTime, op.description AS remark FROM bud_order_payment op WHERE op.order_db_id IN "
				+ "<foreach collection='orderIds' item='orderId' open='(' close=')' separator=','>"
					+ "#{orderId, javaType=String, jdbcType=VARCHAR}"
				+ "</foreach>"
			+ "</script>" } )
	List<Payment> selectPaymentListByOrderIds(@Param("orderIds") String [] orderIds) throws SQLException;

	/**
	 * 根据【订单号】查询【支付流水】
	 * @param orderCode 【订单号】
	 * @return 【支付流水】
	 * @throws SQLException
	 */
	@Select( value = { "SELECT op.db_id AS id, op.order_db_id AS orderId, op.type, op.total_price AS totalPrice, op.status, op.pay_time AS payTime, op.pay_finished_time AS finishTime, op.description AS remark FROM bud_order_payment op WHERE op.order_db_id = (SELECT o.db_id FROM bud_order o WHERE o.code = #{orderCode, javaType=String, jdbcType=VARCHAR})" } )
	Payment selectPaymentByOrderCode(String orderCode) throws SQLException;
}
