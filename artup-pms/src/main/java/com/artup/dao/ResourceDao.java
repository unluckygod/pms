package com.artup.dao;

import java.util.List;

import com.artup.pojo.Resource;

/**
 * 资源
 * @author hapday
 * @date 2017年2月27日  @time 上午11:52:39
 */
public interface ResourceDao {
	/**
	 * 保存【资源】
	 * @param resource
	 * @return
	 * @throws Exception
	 */
	public int insertResource(Resource resource) throws Exception;

	/**
	 * 根据【ID】更新【资源】
	 * @param resource
	 * @return
	 * @throws Exception
	 */
	public int updateResourceById(Resource resource) throws Exception;

	/**
	 * 根据【ID】删除【资源】
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int deleteResourceById(Long id) throws Exception;

	/**
	 * 根据【ID】查询【资源】 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Resource selectResourceById(Long id) throws Exception;

	/**
	 * 模糊查询【资源总记录数】
	 * @param resource
	 * @return
	 * @throws Exception
	 */
	public long selectResourceTotalCount(Resource resource) throws Exception;

	/**
	 * 模糊查询【资源列表】
	 * @param resource
	 * @return
	 * @throws Exception
	 */
	public List<Resource> selectResourceList(Resource resource) throws Exception;

	/**
	 * 查询全部的【资源】列表
	 * @return 【资源】列表
	 * @throws Exception
	 */
	public List<Resource> selectAllResourceList() throws Exception;

	/**
	 * 根据【角色ID】查询有效的【资源】列表
	 * @param roleId 【角色ID】
	 * @return 有效的【资源】列表
	 * @throws Exception
	 */
	public List<Resource> selectValidResourceListByRoleId( long roleId ) throws Exception;

}
