package com.artup.dao;

import java.sql.SQLException;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.artup.pojo.OrderWorksRelation;

/**
 * 订单与作品关系
 * @author hapday
 * @date 2017年7月18日 @Time 下午2:33:00
 */
@Repository
public interface OrderWorksRelationDao {
	/**
	 * 添加【订单与作品关系】
	 * @param OrderWorksRelation 【订单与作品关系】
	 * @throws Exception
	 */
	@Insert( value = { "INSERT INTO bud_order_product_r(db_id, order_db_id, car_db_id, works_quantity, create_time)"
			+ " VALUES(#{id, javaType=String, jdbcType=VARCHAR}, #{orderId, javaType=String, jdbcType=VARCHAR}"
			+ ", #{worksId, javaType=String, jdbcType=VARCHAR}, #{worksQuantity, javaType=Integer, jdbcType=INTEGER}, NOW())" } )
	void insertOrderWorksRelation(OrderWorksRelation orderWorksRelation) throws Exception;
	
	/**
	 * 根据【作品IDs】查询【订单与作品关系数】
	 * @param materialIds 【作品IDs】
	 * @return 【订单与作品关系数】
	 * @throws SQLException
	 */
	@Select( value = { "<script>"
			+ "SELECT COUNT(1) FROM bud_order_product_r owr WHERE owr.car_db_id IN"
				+ "<foreach collection='worksIds' item='worksId' open='(' close=')' separator=','>"
					+ "#{worksId, javaType=String, jdbcType=VARCHAR}"
				+ "</foreach>"
				+ " AND owr.order_db_id NOT IN(SELECT o.db_id FROM bud_order o WHERE o.is_recycle = 1 AND o.user_db_id = #{passportId, javaType=Integer, jdbcType=INTEGER})"
			+ " </script>" } )
	int selectOrderWorksRelationCountByWorksIds(@Param("worksIds") String [] worksIds, @Param("passportId") int passportId) throws SQLException;
}
