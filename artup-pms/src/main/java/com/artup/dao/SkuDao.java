package com.artup.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.artup.pojo.Sku;

/**
 * SKU
 * @author hapday
 * @date 2017年7月17日 @Time 下午5:00:52
 */
@Repository
public interface SkuDao {
	/**
	 * 根据【编号】查询【单价】
	 * @param code 编号
	 * @return 单价
	 * @throws Exception
	 */
	@Select( value = { "SELECT s.price FROM t_sku s WHERE s.code = #{code, javaType=String, jdbcType=VARCHAR}" } )
	float selectPriceByCode(String code) throws Exception;

	/**
	 * 根据【编号】查询【作品尺寸】
	 * @param code 编号
	 * @return 作品尺寸
	 * @throws Exception
	 */
	@Select( value = { "SELECT s.works_size FROM t_sku s WHERE s.code = #{code, javaType=String, jdbcType=VARCHAR}" } )
	String selectWorksSizeByCode(String code) throws Exception;

	/**
	 * 根据【编号】查询【图片尺寸】
	 * @param code 编号
	 * @return 图片尺寸
	 * @throws Exception
	 */
	@Select( value = { "SELECT s.picture_size FROM t_sku s WHERE s.code = #{code, javaType=String, jdbcType=VARCHAR}" } )
	String selectPictureSizeByCode(String code) throws Exception;
	
	/**
	 * 查询【SKU】列表
	 * @param sku 【SKU】
	 * @return 【SKU】列表
	 * @throws SQLException
	 */
	@Select( value = { "SELECT s.id, s.code, s.name, s.works_type_code AS worksTypeCode, s.works_size AS worksSize, s.picture_size AS pictureSize, s.price, s.colour, s.create_time AS createTime FROM t_sku s ORDER BY s.create_time DESC" } )
	List<Sku> selectSkuList(Sku sku) throws SQLException;
}
