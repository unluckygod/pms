package com.artup.dao;

import java.util.List;

import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.artup.pojo.Region;

/**
 * 地区
 * @author hapday
 * @date 2017年7月18日 @Time 下午8:09:09
 */
@Repository
public interface RegionDao {
	/**
	 * 查询全部的地区
	 * @return
	 * @throws Exception
	 */
	@Select( { "SELECT r.id, r.parent_id AS parentId, r.type, r.name FROM t_region r WHERE r.is_valid = 1" } )
	List<Region> selectAllRegion() throws Exception;
}
