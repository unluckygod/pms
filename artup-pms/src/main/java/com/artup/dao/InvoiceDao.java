package com.artup.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;

import com.artup.pojo.Invoice;

/**
 * 发票
 * @author hapday
 * @date 2017年7月21日 @Time 下午4:30:53
 */
public interface InvoiceDao {
	/**
	 * 添加【发票】
	 * @param invoice 【发票】
	 * @throws SQLException
	 */
	@Insert( value = { "INSERT INTO t_invoice(passport_id, title, content, create_time)"
			+ " VALUES(#{passportId, javaType=Integer, jdbcType=INTEGER}, #{title, javaType=String, jdbcType=VARCHAR}, #{content, javaType=String, jdbcType=VARCHAR}, NOW())" } )
	@SelectKey( statement = { "select last_insert_id()" }, before = false, keyProperty = "id", keyColumn = "id", resultType = long.class )
	void insertInvoice(Invoice invoice) throws SQLException;
	
	/**
	 * 根据【ID】查询【发票详情】
	 * @param id 【ID】
	 * @return 【发票详情】
	 * @throws SQLException
	 */
	@Select( value = { "SELECT i.id, i.passport_id AS passportId, i.title, i.content, i.create_time AS createTime FROM t_invoice i WHERE i.id = #{id, javaType=Long, jdbcType=BIGINT}" } )
	Invoice selectInvoiceById(Long id) throws SQLException;

	/**
	 * 根据【IDs】查询【发票列表】
	 * @param ids 【IDs】
	 * @return 【发票列表】
	 * @throws SQLException
	 */
	@Select( value = { "<script>"
			+ "SELECT i.id, i.passport_id AS passportId, i.title, i.content, i.create_time AS createTime FROM t_invoice i WHERE i.id IN"
				+ "<foreach collection='ids' item='id' open='(' close=')' separator=','>"
					+ "#{id, javaType=Long, jdbcType=BIGINT}"
				+ "</foreach>"
				+ " </script>" } )
	List<Invoice> selectInvoiceListByIds(@Param("ids") long [] ids) throws SQLException;
}
