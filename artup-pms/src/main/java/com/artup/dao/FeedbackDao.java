package com.artup.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.artup.pojo.Feedback;

@Repository
public interface FeedbackDao {
	/**
	 * 保存【意见反馈】
	 * @param member 意见
	 * @return
	 * @throws Exception
	 */
	
	@Insert("insert into bud_feedback(id, app_version, client_device, client_os, contact, comment, pass_id, create_time) "
			+ "VALUES(#{id}, #{appVersion}, #{clientDevice}, #{clientOs}, #{contact},#{comment},#{passId},NOW())")
	void insertUserFeedback(Feedback feedback) throws Exception;
	
	/**
	 * 查询【意见反馈】列表
	 * @param feedback 【意见反馈】
	 * @return 【意见反馈】列表
	 * @throws SQLException
	 */
	@Select( value = { "SELECT f.id AS id, f.app_version AS appVersion, f.client_device AS clientDevice, f.client_os AS clientOs, f.contact AS contact, f.comment AS comment, f.pass_id AS passId, f.create_time AS createTime FROM bud_feedback f" } )
	List<Feedback> selectFeedbackList(Feedback feedback) throws SQLException;
}
