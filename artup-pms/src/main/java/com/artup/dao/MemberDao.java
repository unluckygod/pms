package com.artup.dao;

import java.util.List;

import com.artup.pojo.Member;

/**
 * 会员
 * @author hapday
 * @date 2017年2月27日  @time 上午11:52:39
 */
public interface MemberDao {
	/**
	 * 保存【会员】
	 * @param member
	 * @return
	 * @throws Exception
	 */
	public int insertMember(Member member) throws Exception;

	/**
	 * 根据【ID】更新【会员】
	 * @param member
	 * @return
	 * @throws Exception
	 */
	public int updateMemberById(Member member) throws Exception;

	/**
	 * 根据【ID】删除【会员】
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int deleteMemberById(Long id) throws Exception;

	/**
	 * 根据【ID】查询【会员】 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Member selectMemberById(Long id) throws Exception;

	/**
	 * 根据【用户名】查询【会员】
	 * @param username 【用户名】
	 * @return 【会员】
	 * @throws Exception
	 */
	public Member selectMemberByUsername( String username ) throws Exception;

	/**
	 * 模糊查询【会员总记录数】
	 * @param member
	 * @return
	 * @throws Exception
	 */
	public long selectMemberTotalCount(Member member) throws Exception;

	/**
	 * 模糊查询【会员列表】
	 * @param member
	 * @return
	 * @throws Exception
	 */
	public List<Member> selectMemberList(Member member) throws Exception;

	/**
	 * 查询全部的【会员】列表
	 * @return 【会员】列表
	 * @throws Exception
	 */
	public List<Member> selectAllMemberList() throws Exception;

	/**
	 * 根据【角色ID】查询有效的【会员】列表
	 * @param roleId 【角色ID】
	 * @return 有效的【会员】列表
	 * @throws Exception
	 */
	public List<Member> selectValidMemberListByRoleId( long roleId ) throws Exception;

}
