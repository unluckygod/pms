package com.artup.dao;

import java.util.List;

import com.artup.pojo.User;

/**
 * （系统）用户
 * @date 2017年2月15日 下午11:55:50
 * @author hapday
 */
public interface UserDao {
	/**
	 * 保存【用户】
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public int insertUser(User user) throws Exception;

	/**
	 * 根据【ID】更新【用户】
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public int updateUserById(User user) throws Exception;

	/**
	 * 根据【ID】删除【用户】
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int deleteUserById(Long id) throws Exception;

	/**
	 * 根据【ID】查询【用户】 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public User selectUserById(Long id) throws Exception;

	/**
	 * 根据【用户名】查询【用户】
	 * @param username 用户名
	 * @return 用户
	 * @throws Exception
	 */
	public User selectUserByUsername( String username ) throws Exception;

	/**
	 * 模糊查询【用户】的总记录数
	 * @param user 【用户】
	 * @return 【用户】的总记录数
	 * @throws Exception
	 */
	public long selectUserTotalCount(User user) throws Exception;

	/**
	 * 模糊查询【用户列表】
	 * @param user 【用户】
	 * @return 【用户列表】
	 * @throws Exception
	 */
	public List<User> selectUserList(User user) throws Exception;

}
