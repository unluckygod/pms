package com.artup.dao;

import java.util.List;

import com.artup.pojo.Resource;

/**
 * 权限
 * @date 2017年3月16日 下午11:21:24
 * @author hapday
 */
public interface AuthorityDao {
	/**
	 * 根据【用户ID】查询有效的【资源列表】
	 * @param userId 【用户ID】
	 * @return 【资源列表】
	 * @throws Exception
	 */
	public List<Resource> selectValidResourceListByUserId( long userId ) throws Exception;
}
