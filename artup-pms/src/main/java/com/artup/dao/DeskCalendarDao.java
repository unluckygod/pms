package com.artup.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.artup.pojo.DeskCalendar;

/**
 * 台历
 * @author hapday
 * @date 2017年9月7日 @Time 下午8:41:53
 */
@Repository
public interface DeskCalendarDao {
	/**
	 * 查询【台历】总记录数
	 * @param deskCalendar 台历
	 * @return 【台历】列表
	 * @throws SQLException
	 */
	@Select( value = { "<script>"
			+ " SELECT COUNT(1) FROM t_desk_calendar dc WHERE 1 = 1 "
					+ "<if test='null != name and -1 != name'>"
						+ " AND dc.name = #{name, javaType=Integer, jdbcType=INTEGER}"
					+ "</if>"
					+ "<if test='null != skuCode and \"\" != skuCode'>"
						+ " AND dc.sku_code = #{skuCode, javaType=String, jdbcType=VARCHAR}"
					+ "</if>"
					+ "<if test='2000 &lt; beginYearMonth and 2000 &lt; endYearMonth'>"
						+ " AND (dc.name >= #{beginYearMonth, javaType=Integer, jdbcType=INTEGER} AND dc.name &lt;= #{endYearMonth, javaType=Integer, jdbcType=INTEGER})"
//						+ " AND dc.name >= #{beginYearMonth, javaType=Integer, jdbcType=INTEGER}"
//						+ " AND dc.name <= #{endYearMonth, javaType=Integer, jdbcType=INTEGER}"
					+ "</if>"
			+ " </script>" } )
	long selectDeskCalendarTotalCount(DeskCalendar deskCalendar) throws SQLException;

	/**
	 * 查询【台历】列表
	 * @param deskCalendar 台历
	 * @return 【台历】列表
	 * @throws SQLException
	 */
	@Select( value = { "<script>"
			+ " SELECT dc.id, dc.name, dc.sku_code AS skuCode, dc.image_path AS imagePath, dc.remark, dc.create_time AS createTime FROM t_desk_calendar dc WHERE 1 = 1 "
				+ "<if test='null != name and -1 != name'>"
					+ " AND dc.name = #{name, javaType=Integer, jdbcType=INTEGER}"
				+ "</if>"
				+ "<if test='null != skuCode and \"\" != skuCode'>"
					+ " AND dc.sku_code = #{skuCode, javaType=String, jdbcType=VARCHAR}"
				+ "</if>"
				+ "<if test='2000 &lt; beginYearMonth and 2000 &lt; endYearMonth'>"
						+ " AND (dc.name >= #{beginYearMonth, javaType=Integer, jdbcType=INTEGER} AND dc.name &lt;= #{endYearMonth, javaType=Integer, jdbcType=INTEGER})"
//						+ " AND dc.name <= #{endYearMonth, javaType=Integer, jdbcType=INTEGER}"
				+ "</if>"
			+ " ORDER BY create_time DESC LIMIT #{offset, javaType=Long, jdbcType=BIGINT}, #{pageSize, javaType=Integer, jdbcType=INTEGER} </script>" } )
	List<DeskCalendar> selectDeskCalendarList(DeskCalendar deskCalendar) throws SQLException;
	
	@Insert( value = { "<script>"
			+ " INSERT INTO t_desk_calendar(name, original_image_name, expanded_name, image_path, image_size, sku_code"
				+ "<if test='null != remark and \"\" != remark'>"
					+ ", remark"
				+ "</if>"
			+ ", create_time)"
			+ " VALUE(#{name, javaType=Integer, jdbcType=INTEGER}, #{originalImageName, javaType=String, jdbcType=VARCHAR}"
			+ ", #{expandedName, javaType=String, jdbcType=VARCHAR}, #{imagePath, javaType=String, jdbcType=VARCHAR}"
			+ ", #{imageSize, javaType=Long, jdbcType=BIGINT}, #{skuCode, javaType=String, jdbcType=VARCHAR}"
			 	+ "<if test='null != remark and \"\" != remark'>"
			 		+ ", #{remark, javaType=String, jdbcType=VARCHAR}"
			 	+ "</if>"
			+ ", NOW())"
			+ "  </script>" } )
	void insertDeskCalendar(DeskCalendar deskCalendar) throws SQLException;
}
