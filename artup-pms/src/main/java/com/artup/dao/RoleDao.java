package com.artup.dao;

import java.util.List;

import com.artup.pojo.Role;

/**
 * 角色
 * @author hapday
 * @date 2017年2月27日  @time 上午11:52:39
 */
public interface RoleDao {
	/**
	 * 保存【角色】
	 * @param role
	 * @return
	 * @throws Exception
	 */
	public int insertRole(Role role) throws Exception;

	/**
	 * 根据【ID】更新【角色】
	 * @param role
	 * @return
	 * @throws Exception
	 */
	public int updateRoleById(Role role) throws Exception;

	/**
	 * 根据【ID】删除【角色】
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int deleteRoleById(Long id) throws Exception;

	/**
	 * 根据【ID】查询【角色】 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Role selectRoleById(Long id) throws Exception;

	/**
	 * 模糊查询【角色总记录数】
	 * @param role 角色
	 * @return 【角色总记录数】
	 * @throws Exception
	 */
	public long selectRoleTotalCount(Role role) throws Exception;

	/**
	 * 模糊查询【角色列表】
	 * @param role
	 * @return
	 * @throws Exception
	 */
	public List<Role> selectRoleList(Role role) throws Exception;

}
