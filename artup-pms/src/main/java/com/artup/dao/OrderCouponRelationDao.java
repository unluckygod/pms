package com.artup.dao;

import java.sql.SQLException;

import org.apache.ibatis.annotations.Insert;

import com.artup.pojo.OrderCouponRelation;

/**
 * 订单与券的关系
 * @author hapday
 * @date 2017年8月8日 @Time 下午2:48:33
 */
public interface OrderCouponRelationDao {
	/**
	 * 保存【订单与券的关系】
	 * @param orderCouponRelation 订单与券的关系
	 * @throws SQLException
	 */
	@Insert( value = { "INSERT INTO t_order_coupon_relation (passport_id, order_id, works_id, coupon_code)"
			+ " VALUES(#{passportId, javaType=Integer, jdbcType=INTEGER}, #{orderId, javaType=String, jdbcType=VARCHAR},"
			+ " #{worksId, javaType=String, jdbcType=VARCHAR}, #{couponCode, javaType=String, jdbcType=VARCHAR})" } )
	void saveOrderCouponRelation(OrderCouponRelation orderCouponRelation) throws SQLException; 
}
