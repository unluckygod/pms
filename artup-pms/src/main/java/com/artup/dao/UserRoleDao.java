package com.artup.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.artup.pojo.UserRole;

/**
 * 用户-角色关系
 * @author hapday
 * @date 2017年2月27日  @time 上午11:52:39
 */
public interface UserRoleDao {
	/**
	 * 保存【用户-角色】
	 * @param userRole
	 * @return
	 * @throws Exception
	 */
	public int insertUserRole(UserRole userRole) throws Exception;

	/**
	 * 批量保存【用户角色】
	 * @param userRoleList
	 * @return
	 * @throws Exception
	 */
	public int batchInsertUserRole(List<UserRole> userRoleList) throws Exception;

	/**
	 * 根据【ID】批量更新【用户角色】
	 * @param userRoleList
	 * @return
	 * @throws Exception
	 */
	public int batchUpdateUserRoleById(List<UserRole> userRoleList) throws Exception;
	
	/**
	 * 根据【用户ID】批量更新【用户角色】
	 * @param userRoleList
	 * @return
	 * @throws Exception
	 */
//	public int batchUpdateUserRoleByUserId(List<UserRole> userRoleList) throws Exception;

	/**
	 * 根据【ID】更新【用户-角色关系】
	 * @param userRole
	 * @return
	 * @throws Exception
	 */
	public int updateUserRoleById(UserRole userRole) throws Exception;

	/**
	 * 根据【用户ID】批量更新【用户角色】
	 * @param userRole
	 * @return
	 * @throws Exception
	 */
//	public int batchUpdateUserRoleByUserId(UserRole userRole) throws Exception;

	/**
	 * 根据【用户ID】更新【用户角色】
	 * @param userRole
	 * @return
	 * @throws Exception
	 */
	public int updateUserRoleByUserId(UserRole userRole) throws Exception;

	/**
	 * 根据【ID】更新【用户角色】为无效的 
	 * @param requestMap
	 * @return
	 * @throws Exception
	 */
	public int updateUserRoleInvalidById( @Param("id") Long id, @Param("modifierId") Long modifierId ) throws Exception;

	/**
	 * 根据【ID】删除【用户-角色关系】
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int deleteUserRoleById(Long id) throws Exception;

	/**
	 * 根据【ID】查询【用户-角色关系】 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public UserRole selectUserRoleById(Long id) throws Exception;

	/**
	 * 模糊查询【用户-角色】总记录数
	 * @param userRole
	 * @return
	 * @throws Exception
	 */
	public long selectUserRoleTotalCount(UserRole userRole) throws Exception;
	
	/**
	 * 模糊查询【用户-角色关系列表】
	 * @param userRole
	 * @return
	 * @throws Exception
	 */
	public List<UserRole> selectUserRoleList(UserRole userRole) throws Exception;

	/**
	 * 根据【用户ID】查询【用户角色】列表
	 * @param userId 用户ID
	 * @return 【用户角色】列表
	 * @throws Exception
	 */
	public List<UserRole> selectUserRoleListByUserId( long userId ) throws Exception;

	/**
	 * 根据【用户ID】查询有效的【用户角色】列表
	 * @param userId 用户ID
	 * @return 【用户角色】列表
	 * @throws Exception
	 */
	public List<UserRole> selectValidUserRoleListByUserId( long userId ) throws Exception;

}
