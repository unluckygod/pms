package com.artup.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 素材/图片
 * @author hapday
 * @date 2017年7月26日 @Time 上午11:02:09
 */
@Repository
public interface PictureDao {
	/**
	 * 根据【通行证ID】查询【素材总记录数】
	 * @param passportId 【通行证ID】
	 * @return 【素材总记录数】
	 * @throws SQLException
	 */
	long selectMaterialTotalCountByPassportId(int passportId) throws SQLException;
	
	/**
	 * 根据【用户ID】查询【图片列表】
	 * @param userId
	 * @param offset
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	List<Map<String, Object>> selectPictureListByPassportId(@Param( value = "passportId" ) int passportId, @Param( value = "offset" ) int offset, @Param( value = "pageSize" ) int pageSize) throws Exception;
}
