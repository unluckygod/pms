package com.artup.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.artup.pojo.RoleResource;

/**
 * 角色-资源
 * @author hapday
 * @date 2017年2月27日  @time 上午11:52:39
 */
public interface RoleResourceDao {
	/**
	 * 保存【角色资源】
	 * @param roleResource
	 * @return
	 * @throws Exception
	 */
	public int insertRoleResource(RoleResource roleResource) throws Exception;

	/**
	 * 批量添加【角色资源】
	 * @param roleResourceList
	 * @return
	 * @throws Exception
	 */
	public int batchInsertRoleResource(List<RoleResource> roleResourceList) throws Exception;

	/**
	 * 根据【ID】更新【角色-资源】
	 * @param roleResource
	 * @return
	 * @throws Exception
	 */
	public int updateRoleResourceById(RoleResource roleResource) throws Exception;

	/**
	 * 根据【ID】批量更新【角色资源】
	 * @param roleResourceList
	 * @return
	 * @throws Exception
	 */
	public int batchUpdateRoleResourceById(List<RoleResource> roleResourceList) throws Exception;

	/**
	 * 根据【ID】更新【角色角色】为无效的
	 * @param id
	 * @param modifierId
	 * @return
	 * @throws Exception
	 */
	public int updateRoleResourceInvalidById( @Param("id") long id, @Param("modifierId") long modifierId ) throws Exception;

	/**
	 * 根据【角色ID】更新【角色资源】
	 * @param roleResource
	 * @return
	 * @throws Exception
	 */
	public int updateRoleResourceByRoleId(RoleResource roleResource) throws Exception;

	/**
	 * 根据【ID】删除【角色-资源】
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int deleteRoleResourceById(Long id) throws Exception;

	/**
	 * 根据【ID】查询【角色-资源】 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public RoleResource selectRoleResourceById(Long id) throws Exception;

	/**
	 * 模糊查询【角色资源】总记录数
	 * @param roleResource
	 * @return
	 * @throws Exception
	 */
	public long selectRoleResourceTotalCount(RoleResource roleResource) throws Exception;

	/**
	 * 模糊查询【角色-资源列表】
	 * @param roleResource
	 * @return
	 * @throws Exception
	 */
	public List<RoleResource> selectRoleResourceList(RoleResource roleResource) throws Exception;

	/**
	 * 根据【角色ID】查询有效的【角色资源】列表
	 * @param roleId 【角色ID】
	 * @return 有效的【角色资源】列表
	 * @throws Exception
	 */
	public List<RoleResource> selectValidRoleResourceListByRoleId( long roleId ) throws Exception;

	/**
	 * 根据【角色ID】查询【角色资源】列表
	 * @param roleId 【角色ID】
	 * @return 【角色资源】列表
	 * @throws Exception
	 */
	public List<RoleResource> selectRoleResourceListByRoleId( long roleId ) throws Exception;

}
