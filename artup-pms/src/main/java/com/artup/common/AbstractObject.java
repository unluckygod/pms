package com.artup.common;

import java.util.Date;

/**
 * 通用对象
 * @author hapday
 * @date 2016年3月30日 @time 下午7:01:37
 */
@SuppressWarnings("serial")
public abstract class AbstractObject implements java.io.Serializable, java.lang.Cloneable {
	private byte isRecycle = 2;		// 是否回收（1-是；2-否）
	private Date recycleTime;		// 回收时间
	private String remark;			// 备注
	private Integer version = 1;	// 版本号
	private Long creatorId = 0L;	// 创建人ID
	private Date createTime = null;	// 创建时间
	private Long modifierId = 0L;		// 修改人ID
	private Date modifyTime = null;	// 修改时间
	private Integer pageSize = 10;	// 每页显示的条数
	private Integer pageIndex = 1;	// 当前页码
	private Long offset = 0L;		// 起始记录数，偏移量
	private Integer [] ids;		// IDs
	
	public AbstractObject() {
	}

	public byte getIsRecycle() {
		return isRecycle;
	}

	public void setIsRecycle(byte isRecycle) {
		this.isRecycle = isRecycle;
	}

	public Date getRecycleTime() {
		return recycleTime;
	}

	public void setRecycleTime(Date recycleTime) {
		this.recycleTime = recycleTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Long getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(Long creatorId) {
		this.creatorId = creatorId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getModifierId() {
		return modifierId;
	}

	public void setModifierId(Long modifierId) {
		this.modifierId = modifierId;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}

	public Long getOffset() {
		return offset;
	}

	public void setOffset(Long offset) {
		this.offset = offset;
	}

	public Integer[] getIds() {
		return ids;
	}

	public void setIds(Integer[] ids) {
		this.ids = ids;
	}
}
