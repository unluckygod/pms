package com.artup.common;

/**
 * 反馈结果
 * @author hapday
 * @date 2017年6月2日 @Time 下午12:00:20
 */
public class ResponseResult {
	private int status;		// 状态
	private Object data;	// 数据
	private String message;		// 消息
	
	public ResponseResult() {
		// TODO Auto-generated constructor stub
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
