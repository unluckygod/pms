package com.artup.common;

import java.util.HashMap;
import java.util.Map;

/**
 * 常量
 * @author hapday
 * @date 2017年7月12日 @Time 上午9:07:06
 */
public class Constants {
	public static final int ACTION_STATUS_SUCCESS = 1;	// 执行成功
	public static final int ACTION_STATUS_FAILURE = 2;	// 失败
	public static final int ACTION_STATUS_LOGINED = 3;	// 已登录
	public static final int ACTION_STATUS_LOGOUTED = 4;	// 已退出
	public static final int ACTION_STATUS_RESULT_EMPTY = 5;	// 空结果
	public static final int ACTION_STATUS_PARAMETER_EMPTY = 6;	// 参数为空
	public static final int ACTION_STATUS_PARAMETER_NOT_POSITIVE_INTEGER = 13;	// 非正整数
	public static final int ACTION_STATUS_SESSION_EXPIRE = 7;	// 会话已失效
	public static final int ACTION_STATUS_NET_SUCCESS = 8;	// 通信成功
	public static final int ACTION_STATUS_NET_TIMEOUT = 9;	// 通信超时
	public static final int ACTION_STATUS_PAY_SUCCESS = 10;	// 支付成功
	public static final int ACTION_STATUS_PAY_FAILURE = 11;	// 支付失败
	public static final int ACTION_STATUS_PARAMETER_PRECISION = 12;	// 参数精度有误
	public static final int ACTION_STATUS_NOT_MORE_THAN_ZERO = 100;	// 不大于零
    public static final String ACTION_MESSAGE_NOT_MORE_THAN_ZERO = "不大于零";	// 不大于零

    public static final String CACHE_TYPE_REGION_LIST = "regionList";	// 地区列表
    
    public static final Map<String, String> WORKS_TYPE_MAP = new HashMap<String, String>();
    public static final Map<String, String> CLIENT_TYPE_MAP = new HashMap<String, String>();
    
    static {
    	WORKS_TYPE_MAP.put("kuanghua", "框画");
    	WORKS_TYPE_MAP.put("haibao", "海报");
    	WORKS_TYPE_MAP.put("citiehua", "磁贴画");

    	CLIENT_TYPE_MAP.put("ios", "iphone");
    	CLIENT_TYPE_MAP.put("android", "安卓");
    }
    
    // 支付方式
    public static final String PAY_TYPE_WECHAT = "wechat";		// 微信
    public static final String PAY_TYPE_ALIPAY = "alipay";		// 支付宝
}
