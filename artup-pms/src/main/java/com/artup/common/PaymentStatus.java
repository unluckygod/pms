package com.artup.common;

/**
 * 支付状态
 * @author hapday
 * @date 2017年7月26日 @Time 下午2:59:06
 */
public enum PaymentStatus {
	UNPAID, PAID;

	private static byte value;
	
	public static byte getValue(PaymentStatus paymentStatus) {
		if(paymentStatus == UNPAID)	// 待付款
			value = 1;
		
		if(paymentStatus == PAID)	// 已付款
			value = 2;
		
		return value;
	}

	public static void main(String[] args) {
		System.out.println(PaymentStatus.getValue(PaymentStatus.UNPAID));
	}
}
