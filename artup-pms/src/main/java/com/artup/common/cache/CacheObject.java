package com.artup.common.cache;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import com.artup.util.SpringContextUtils;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 缓存
 * @author hapday
 * @date 2016年8月13日 下午8:13:58
 */
public abstract class CacheObject {
	private final static Logger LOGGER = LoggerFactory.getLogger(CacheObject.class);
	
	// public abstract void instanceReload();
	private static ObjectMapper objectMapper = new ObjectMapper();
	public String _cacheName;

	public abstract String getCacheName();

	/**
	 * 装载缓存数据
	 * @return
	 */
	public abstract Object loadData();

	private static JedisPool jedisPool;

	private Object _data;
	
	protected boolean isList;
	
	public boolean isList () {
		return isList;
	}

	/**
	 * 从缓存中获取所有数据
	 * @return
	 */
	public Object getData() {
		if (jedisPool == null) {
			jedisPool = SpringContextUtils.getBean("jedisPool");
		}
		Jedis jedis = jedisPool.getResource();
		String cacheContent = null;
		if (isList) {
			cacheContent = jedis.hget(getCacheName(), getCacheName());
			try {
				if (StringUtils.isNotBlank(cacheContent)) {
					_data = objectMapper.readValue(cacheContent, ArrayList.class);
				}
			} catch (JsonParseException e) {
				LOGGER.error("将 redis 缓存内容转换为 ArrayList 类型数据 - 失败！", e);
			} catch (JsonMappingException e) {
				LOGGER.error("redis 中的 JSON 数据格式不正确", e);
			} catch (IOException e) {
				LOGGER.error("读取 redis 缓存内容 - 失败！", e);
			} finally{
				jedisPool.returnResource(jedis);
			}
		} else {
			_data = jedis.hgetAll(getCacheName());
		}
		jedisPool.returnResource(jedis);
		return _data;
	}

	/**
	 * 从缓存中截取列表
	 * @param startOffset 开始点偏移量
	 * @param endOffset	   结束偏移量
	 * @return
	 */
	public Object getRange(long startOffset, long endOffset) {
		Object rangeData = null;
		Jedis jedis = jedisPool.getResource();
		String cahceContent = jedis.getrange(getCacheName(), startOffset, endOffset);
		try {
			rangeData = objectMapper.readValue(cahceContent, _data.getClass());
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			jedisPool.returnResource(jedis);
		}
		return rangeData;
	}
	
	/**
	 * 获取缓存的对象
	 * @param clz
	 * @param secondKey
	 * @return
	 */
	public Object getObject(Class<?> clz, String secondKey) {
		if (jedisPool == null) {
			jedisPool = SpringContextUtils.getBean("jedisPool");
		}
		Jedis jedis = jedisPool.getResource();
		Object obj = null;
		try {
			String cacheContent = jedis.hget(getCacheName(), secondKey);
			if (StringUtils.isNotBlank(cacheContent)) {
				obj = objectMapper.readValue(cacheContent, clz);
			}
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			jedisPool.returnResource(jedis);
		}
		return obj;
	}
	
	/**
	 * 把数据放入缓存
	 * @param secondKey
	 * @param obj
	 * @return
	 */
	public long setObject (String secondKey, Object obj) {
		if (jedisPool == null) {
			jedisPool = SpringContextUtils.getBean("jedisPool");
		}
		Jedis jedis = jedisPool.getResource();
		long result = 0;
		try {
			if (obj instanceof List) {
				result = jedis.hset(getCacheName(), secondKey, objectMapper.writeValueAsString(obj));
			}
			if (obj instanceof Map) {
				String s = jedis.hmset(getCacheName(), (Map<String, String>) obj);
				if (s.equalsIgnoreCase("ok")) {
					result = 1;
				}
			}
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			jedisPool.returnResource(jedis);
		}
		return result;
	}
	
	/**
	 * 删除缓存
	 * @param keys
	 * @return
	 */
	public long removeObject(String[] keys) {
		if (jedisPool == null) {
			jedisPool = SpringContextUtils.getBean("jedisPool");
		}
		Jedis jedis = jedisPool.getResource();
		
		long result = jedis.hdel(getCacheName(), keys);
		jedisPool.returnResource(jedis);
		return result;
	}
	
}
