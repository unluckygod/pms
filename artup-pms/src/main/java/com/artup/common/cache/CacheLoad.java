package com.artup.common.cache;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.artup.pojo.Region;

/**
 * 缓存加载类
 * @author Lomis
 *
 */
public class CacheLoad {
	
	/**
	 * 加载所有的缓存数据类
	 * @param clzs
	 */
	/*public CacheLoad(List<Class<?>> clzs) {
		for (Class<?> clz : clzs) {
			try {
				Class<?> cacheObject = Class.forName(clz.getName());
				Method method = cacheObject.getMethod("getInstanceByLoad");
				method.invoke(cacheObject);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}*/

	public <T> void cacheLoad2(List<Class<T>> clzs) {
		for (Class<T> clz : clzs) {
			try {
				Class<?> cacheObject = Class.forName(clz.getName());
				Method method = cacheObject.getMethod("getInstanceByLoad");
				method.invoke(cacheObject);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}

	public <T> CacheLoad(List<Class<T>> clzs) {
		for (Class<T> clz : clzs) {
			try {
				Class<?> cacheObject = Class.forName(clz.getName());
				Method method = cacheObject.getMethod("getInstanceByLoad");
				method.invoke(cacheObject);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		List<Class<Region>> aaa = new ArrayList<Class<Region>>();
		aaa.add(Region.class);
		
		new CacheLoad(aaa);
	}
}
