package com.artup.common;

/**
 * 作品类型
 * @author hapday
 * @date 2017年7月21日 @Time 下午4:22:47
 */
public enum WorksType {
	kuanghua, haibao, citiehua;

	private static String value;
	
	public static String getTypeName(WorksType worksType) {
		if(worksType == kuanghua)	// 框画
			value = "框画";
		
		if(worksType == haibao)	// 海报
			value = "海报";

		if(worksType == citiehua)		// 磁贴画
			value = "磁贴画";
		
		return value;
	}

	public static void main(String[] args) {
		System.out.println(WorksType.getTypeName(WorksType.haibao));
	}
}
