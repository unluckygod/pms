package com.artup.common;

/**
 * 订单状态
 * @author hapday
 * @date 2017年7月18日 @Time 上午9:45:18
 */
public enum OrderStatus {
	CANCEL, CREATED, PAYING, PAID, PDF_CREATING, PDF_CREATED, PUSHING_MES ,PUSHED_MES,DELIVERED ;

	private static byte value;
	
	public static byte getValue(OrderStatus orderStatus) {
		if(orderStatus == CANCEL)	// 已取消
			value = -1;

		if(orderStatus == CREATED)	// 已创建
			value = 1;
		
		if(orderStatus == PAYING)	// 支付中
			value = 2;

		if(orderStatus == PAID)		// 已支付
			value = 3;

		if(orderStatus == PDF_CREATING)		// PDF 制作中
			value = 4;

		if(orderStatus == PDF_CREATED)		// PDF 制作完成
			value = 5;

		if(orderStatus == PUSHING_MES)		// 推送给MES
			value = 6;
		
		if(orderStatus == PUSHED_MES)		// 成功推送到MES
			value = 7;
		
		if(orderStatus == DELIVERED)		// 已发货
			value = 10;		
		return value;
	}
	
	public static String getOrderStatusName(byte status) {
		String statusName = "";
		
		switch (status) {
		case -1: statusName = "已取消";
			break;
		case 1: statusName = "已创建";
			break;
		case 2: statusName = "支付中";
			break;
		case 3: statusName = "已支付";
			break;
		case 4: statusName = "PDF 制作中";
			break;
		case 5: statusName = "PDF 制作完成";
			break;
		case 6: statusName = "推送订单到 MES";
			break;
		case 7: statusName = "推送订单到 MES 成功";
			break;
		case 10: statusName = "已发货";

		default:
			break;
		}
		
		return statusName;
	}

	public static void main(String[] args) {
		System.out.println(OrderStatus.getValue(OrderStatus.CREATED));
		System.out.println(getOrderStatusName((byte) 1));
	}
}
