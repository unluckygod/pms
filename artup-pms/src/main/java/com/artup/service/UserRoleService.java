package com.artup.service;

import com.artup.common.ResponseResult;
import com.artup.pojo.Role;
import com.artup.pojo.UserRole;

/**
 * 用户角色管理
 * @author hapday
 * @date 2017年3月8日 @Time 下午2:28:17
 */
public interface UserRoleService {
	/**
	 * 根据【ID】查询【用户角色】
	 * @param id
	 * @return
	 */
	public ResponseResult queryUserRoleById(long id) ;

	/**
	 * 模糊查询【用户角色列表】
	 * @param userRole
	 * @return
	 */
	public ResponseResult queryUserRoleList(UserRole userRole) ;
	
	/**
	 * 模糊查询【角色列表】
	 * @param role
	 * @return
	 */
	public ResponseResult queryRoleList(Role role, long userId) ;

	/**
	 * 根据【用户ID】查询【用户角色】列表
	 * @param userId 【用户ID】
	 * @return 【用户角色】列表
	 */
	public ResponseResult queryUserRoleListByUserId( long userId ) ;
	
	/**
	 * 保存【用户角色】
	 * @param userRole
	 * @return
	 */
	public ResponseResult saveUserRole(UserRole userRole);

	/**
	 * 根据【ID】更新【用户角色】
	 * @param userRole
	 * @return
	 */
	public ResponseResult updateUserRoleById(UserRole userRole);
	
	/**
	 * 根据【ID】更新【用户角色】为无效的 
	 * @param id
	 * @param userId
	 * @return
	 */
	public ResponseResult updateUserRoleInvalidById( Long id, Long userId );

	/**
	 * 根据【用户ID】更新【用户角色】
	 * @param userRole
	 * @return
	 */
	public ResponseResult updateUserRoleByUserId(long userId, String roleIds);

	/**
	 * 根据【ID】删除【用户角色】
	 * @param userRole
	 * @return
	 */
	public ResponseResult deleteUserRoleById(long id);

	/**
	 * 查询【角色】列表
	 * @param role 【角色】
	 * @param userId 【用户ID】
	 * @return
	 */
	public ResponseResult queryRoleList(Role role, Long userId) ;
}
