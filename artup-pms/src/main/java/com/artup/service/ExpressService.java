package com.artup.service;

import com.artup.common.ResponseResult;

/**
 * 物流
 * @author hapday
 * @date 2017年8月2日 @Time 上午11:04:40
 */
public interface ExpressService {
	/**
	 * 根据【订单号】查询【物流列表】
	 * @param orderCode 【订单号】
	 * @return 【物流列表】
	 */
	ResponseResult queryExpress( String orderCode ) ;
}
