package com.artup.service;

import javax.servlet.http.HttpSession;

import com.artup.common.ResponseResult;

/**
 * 权限验证
 * @date 2017年3月16日 下午10:47:55
 * @author hapday
 */
public interface AuthorityWebService {
	/**
	 * 登陆
	 * @param username 用户名
	 * @param password 密码
	 * @param securityCode 安全码
	 * @param clientIP 客户端IP
	 * @param httpSession
	 * @return
	 */
	public ResponseResult signin( String username, String password, String securityCode, String clientIP, HttpSession httpSession ) ;

	/**
	 * 注册
	 * @param username 用户名
	 * @param password 密码
	 * @param securityCode 安全码
	 * @param clientIP 客户端IP
	 * @param httpSession
	 * @return
	 */
	public ResponseResult register( String username, String password, String nickname, String securityCode, String clientIP, HttpSession httpSession ) ;
	
	/**
	 * 退出
	 * @param username
	 * @return
	 */
	public ResponseResult signout( HttpSession httpSession, String username ) ;

}
