package com.artup.service.schedule;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import com.artup.dao.OrderDao;

@Slf4j
@Configuration
//@EnableScheduling // 启用定时任务
public class UpdateOrderSchedule {

	@Autowired
	private OrderDao orderDao;
	
	@Scheduled(initialDelay=1000*60,fixedDelay=1000*60*5)
	public void scheduler() { //第一次延迟1分钟，上次执行完毕后5分钟再次执行
		log.info(">>>>>>>>>>>>> scheduled ... "+orderDao);
	}
	
}
