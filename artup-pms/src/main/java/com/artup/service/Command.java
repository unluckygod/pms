package com.artup.service;
import java.util.List;

import com.artup.pojo.MaterialEdit;

/**
 * 命令模式接口
 * 
 * @author zzg
 *
 * @param <P> 传入方法 execute 的参数类型
 * @param <T> 方法执行完成后返回结果类型
 */
public interface Command <P, T> {

	void execute(P objects, List<MaterialEdit> materialEditList) throws Exception;
}