package com.artup.service;

import com.artup.common.ResponseResult;
import com.artup.pojo.Member;

/**
 * 会员
 * @author hapday
 * @date 2017年7月11日 @Time 上午9:29:22
 */
public interface MemberService {
	/**
	 * 保存【会员】
	 * @param member
	 * @return
	 */
	ResponseResult saveMember(Member member);
	
	/**
	 * 根据【ID】查询【会员】
	 * @param id 【ID】
	 * @return 【会员】
	 */
	ResponseResult queryMemberById(String id);
	
	/**
	 * 查询【会员列表】
	 * @param member 会员
	 * @return 【会员列表】
	 */
	ResponseResult queryMemberList(Member member);
	
	/**
	 * 更新【会员】
	 * @param member
	 * @return
	 */
	ResponseResult updateMemberById(Member member);

	/**
	 * 删除【会员】
	 * @param id
	 * @return
	 */
	ResponseResult deleteMemberById(Long id);
}
