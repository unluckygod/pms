package com.artup.service;

import com.artup.common.ResponseResult;
import com.artup.pojo.User;

/**
 * 用户管理
 * @author hapday
 * @date 2017年3月8日 @Time 下午2:28:17
 */
public interface UserService {
	/**
	 * 根据【ID】查询【用户】
	 * @param id
	 * @return
	 */
	public ResponseResult queryUserById(long id) ;

	/**
	 * 模糊查询【用户列表】
	 * @param user
	 * @return
	 */
	public ResponseResult queryUserList(User user) ;
	
	/**
	 * 保存【用户】
	 * @param user
	 * @return
	 */
	public ResponseResult saveUser(User user);

	/**
	 * 根据【ID】更新【用户】
	 * @param user
	 * @return
	 */
	public ResponseResult updateUserById(User user);

	/**
	 * 根据【ID】删除【用户】
	 * @param user
	 * @return
	 */
	public ResponseResult deleteUserById(long id);

}
