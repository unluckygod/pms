package com.artup.service.task;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.artup.service.OrderService;

@Slf4j
@Component
public class ExecutorTask {
	
	@Autowired
	private OrderService orderService;
	
	static long times = 0;
	
	@Async("async")  
    public void updateOrderDeliverStatusTask(String orderCode){  
		log.debug("thread-{}.updateOrderDeliverStatusTask.orderCode={}",times,orderCode);  
	 	orderService.updateOrderDeliverStatus(orderCode);
	 	times++;
    } 
	
}
