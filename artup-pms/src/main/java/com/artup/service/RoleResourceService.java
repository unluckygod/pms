package com.artup.service;

import com.artup.common.ResponseResult;
import com.artup.pojo.Resource;
import com.artup.pojo.RoleResource;

/**
 * 角色资源管理
 * @author hapday
 * @date 2017年3月8日 @Time 下午2:28:17
 */
public interface RoleResourceService {
	/**
	 * 根据【ID】查询【角色资源】
	 * @param id
	 * @return
	 */
	public ResponseResult queryRoleResourceById(long id) ;

	/**
	 * 模糊查询【角色资源列表】
	 * @param roleResource
	 * @return
	 */
	public ResponseResult queryRoleResourceList(RoleResource roleResource) ;
	
	/**
	 * 根据【角色ID】查询【资源】列表
	 * @param roleId 【角色ID】
	 * @return 【资源】列表
	 */
	public ResponseResult queryResourceListByRoleId( long roleId ) ;
	
	/**
	 * 保存【角色资源】
	 * @param roleResource
	 * @return
	 */
	public ResponseResult saveRoleResource(RoleResource roleResource);

	/**
	 * 根据【ID】更新【角色资源】
	 * @param roleResource
	 * @return
	 */
	public ResponseResult updateRoleResourceById(RoleResource roleResource);
	
	/**
	 * 根据【ID】更新【角色资源】为无效的 
	 * @param id
	 * @param roleId
	 * @return
	 */
	public ResponseResult updateRoleResourceInvalidById( Long id, Long roleId );
	
	/**
	 * 模糊查询【资源列表】
	 * @param role
	 * @return
	 */
	public ResponseResult queryResourceList(Resource resource, long roleId) ;

	/**
	 * 根据【角色ID】更新【角色资源】
	 * @param roleResource
	 * @return
	 */
	public ResponseResult updateRoleResourceByRoleId(long roleId, String roleIds);

	/**
	 * 根据【ID】删除【角色资源】
	 * @param roleResource
	 * @return
	 */
	public ResponseResult deleteRoleResourceById(long id);

}
