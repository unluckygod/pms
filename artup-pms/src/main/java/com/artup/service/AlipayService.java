package com.artup.service;

import javax.servlet.http.HttpServletRequest;

import com.artup.common.ResponseResult;

/**
 * 支付宝
 * @author hapday
 * @date 2017年7月20日 @Time 下午3:17:14
 */
public interface AlipayService {
	/**
	 * 创建支付宝【预备参数】
	 * @param orderCode 订单号
	 * @return
	 */
	ResponseResult createAlipayOrder(String orderCode, float totalFee, String worksName);

	/**
	 * 查询【支付宝支付订单】
	 * @param orderCode 订单号
	 * @param alipayOrderCode 支付宝订单号
	 * @return 【支付宝支付订单】
	 */
	ResponseResult queryAlipayOrder(String orderCode, String alipayOrderCode);

	/**
	 * 支付宝支付后的回调
	 * @param request
	 * @return
	 */
	ResponseResult aliPayCallback(HttpServletRequest request);
}
