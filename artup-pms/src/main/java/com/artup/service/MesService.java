package com.artup.service;

/**
 * MES
 * @author hapday
 * @date 2017年8月9日 @Time 下午3:23:47
 */
public interface MesService {
	/**
	 * 通知 MES 订单
	 * @param orderCode 订单号
	 */
	void noticeMesOrder(String orderCode) ;
}
