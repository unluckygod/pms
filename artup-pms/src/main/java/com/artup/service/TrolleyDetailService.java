package com.artup.service;

import com.artup.common.ResponseResult;
import com.artup.pojo.TrolleyDetail;

/**
 * 购物车详情
 * @author hapday
 * @date 2017年7月24日 @Time 下午5:21:15
 */
public interface TrolleyDetailService {
	/**
	 * 根据【IDs】更新【购物车详情】的【状态】
	 * @param passportId 【通行证ID】
	 * @param ids 【IDs】
	 * @return
	 */
	ResponseResult batchUpdateTrolleyDetailStatusByIds(Integer passportId, String ids);

	/**
	 * 保存购物车详情
	 * @param trolleyDetail 购物车详情
	 * @return
	 */
	ResponseResult saveTrolley(TrolleyDetail trolleyDetail);
	
	/**
	 * 根据【ID】更新【购物车详情】中【作品数量】
	 * @param id【ID】
	 * @param quantity【作品数量】
	 * @return
	 */
	ResponseResult updateTrolleyDetailQuantityById(String id, int quantity);
}
