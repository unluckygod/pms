package com.artup.service;

import com.artup.common.ResponseResult;
import com.artup.pojo.Resource;

/**
 * 资源管理
 * @author hapday
 * @date 2017年3月8日 @Time 下午2:28:17
 */
public interface ResourceService {
	/**
	 * 根据【ID】查询【资源】
	 * @param id
	 * @return
	 */
	public ResponseResult queryResourceById(long id) ;

	/**
	 * 模糊查询【资源列表】
	 * @param resource
	 * @return
	 */
	public ResponseResult queryResourceList(Resource resource) ;

	/**
	 * 查询全部的【资源】列表
	 * @return
	 */
	public ResponseResult queryAllResourceList() ;
	
	/**
	 * 保存【资源】
	 * @param resource
	 * @return
	 */
	public ResponseResult saveResource(Resource resource);

	/**
	 * 根据【ID】更新【资源】
	 * @param resource
	 * @return
	 */
	public ResponseResult updateResourceById(Resource resource);

	/**
	 * 根据【ID】删除【资源】
	 * @param resource
	 * @return
	 */
	public ResponseResult deleteResourceById(long id);

}
