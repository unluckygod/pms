package com.artup.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.artup.common.ResponseResult;

/**
 * 支付
 * @author hapday
 * @date 2017年7月20日 @Time 下午3:17:14
 */
public interface PayService {
	public ResponseResult findWeChatPayResult(HttpServletRequest request);
	public ResponseResult findWeChatPayResult(Map<String, Object> payNoticeMap);		//[微信支付二次验证]
	public ResponseResult findAliPayResult(Map<String, Object> payNoticeMap);		// 【支付宝支付二次验证】
}
