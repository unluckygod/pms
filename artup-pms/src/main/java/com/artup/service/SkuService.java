package com.artup.service;

import com.artup.common.ResponseResult;
import com.artup.pojo.Sku;

/**
 * SKU
 * @author hapday
 * @date 2017年8月23日 @Time 上午11:49:05
 */
public interface SkuService {
	ResponseResult querySkuList(Sku sku) ;
}
