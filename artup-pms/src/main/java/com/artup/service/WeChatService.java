package com.artup.service;

import java.util.Map;

import com.artup.common.ResponseResult;

/**
 * 微信
 * @author hapday
 * @date 2017年7月25日 @Time 上午10:00:52
 */
public interface WeChatService {
	/**
	 * 创建微信支付订单
	 * @param orderCode 订单号
	 * @param totalFee 总金额，单位：分。
	 * @return
	 */
	ResponseResult createWeChatMobilePayOrder(String orderCode, float totalFee);

	/**
	 * 二次验证微信移动支付的回调结果
	 * @param payResultMap
	 * @return
	 * 2016年4月19日 上午11:05:25
	 */
	ResponseResult weChatMobilePayCallback(Map<String, Object> payResultMap);

	/**
	 * 查询【微信订单】
	 * @param orderCode 订单号
	 * @param weChatOrderCode 微信订单号
	 * @return 微信订单
	 */
	ResponseResult queryWeChatMobilePayOrder(String orderCode, String weChatOrderCode);
}
