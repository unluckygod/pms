package com.artup.service;

import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.artup.common.ResponseResult;
import com.artup.pojo.DeskCalendar;

/**
 * 台历
 * @author hapday
 * @date 2017年9月7日 @Time 下午8:20:14
 */
public interface DeskCalendarService {
	/**
	 * 查询【台历】列表
	 * @param deskCalendar 台历
	 * @return 【台历】列表
	 */
	ResponseResult queryDeskCalendarList(DeskCalendar deskCalendar);

	/**
	 * 保存【台历】
	 * @param name 名称
	 * @param skuCode SKU 编号
	 * @param multipartRequest
	 * @return
	 */
	ResponseResult saveDeskCalendar(Integer name, String skuCode, MultipartHttpServletRequest multipartRequest);
}
