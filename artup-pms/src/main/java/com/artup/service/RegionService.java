package com.artup.service;

import com.artup.common.ResponseResult;

/**
 * 地区
 * @author hapday
 * @date 2017年7月13日 @Time 下午3:48:39
 */
public interface RegionService {
	/**
	 * 查询全部的地区
	 * @return
	 */
	ResponseResult queryAllRegion();
	
	/**
	 * 查询全部地区
	 * @return
	 */
	ResponseResult queryAllRegion2();
}
