package com.artup.service;

import com.artup.common.ResponseResult;
import com.artup.pojo.Works;

/**
 * 作品
 * @author hapday
 * @date 2017年7月21日 @Time 下午6:11:21
 */
public interface WorksService {
	/**
	 * 根据【ID】查询【作品】
	 * @param id 【ID】
	 * @return 【作品】
	 */
	ResponseResult queryWorksById(String id) ;

	/**
	 * 根据【IDs】批量删除【作品】
	 * @param ids 【IDs】
	 * @return
	 */
	ResponseResult batchDeleteWorksByIds(Integer passportId, String ids);

	/**
	 * 保存作品
	 * @param worksJSONObject
	 * @return
	 */
	ResponseResult saveWorks(String worksJSONObject);

	/**
	 * 查询【作品列表】
	 * @param works
	 * @return 【作品列表】
	 */
	ResponseResult queryWorksList(Works works);
	
	/**
	 * 根据【通行证ID】查询【购物车中作品】的数量
	 * @param passportId 【通行证ID】
	 * @return 【购物车中作品】的数量
	 */
	ResponseResult queryTrolleyWorksCountByPassportId(Integer passportId);
}
