package com.artup.service;

import com.artup.common.ResponseResult;
import com.artup.pojo.Feedback;

/**
 * 意见反馈
 */
public interface FeedbackService {

	/**
	 * 保存【意见反馈】
	 * @param feedback 意见
	 * @return
	 * @throws Exception
	 */
	ResponseResult saveFeedback(Feedback feedback) ;

	/**
	 * 查询【意见反馈】列表
	 * @param feedback 【意见反馈】
	 * @return 【意见反馈】列表
	 */
	ResponseResult queryFeedbackList(Feedback feedback);
}
