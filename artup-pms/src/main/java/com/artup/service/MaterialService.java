package com.artup.service;

import com.artup.common.ResponseResult;
import com.artup.pojo.Material;
import com.artup.to.MaterialTo;

/**
 * 素材
 * @author hapday
 * @date 2017年7月12日 @Time 上午8:58:22
 */
public interface MaterialService {
	/**
	 * 根据【通行证ID】查询【素材列表】
	 * @param passportId 【通行证ID】
	 * @param currentPageIndex 【当前页码】
	 * @param pageCount 【每页多少条】
	 * @return 【素材列表】
	 */
	ResponseResult queryMaterialListByPassportId(int passportId, Integer width, Integer height, Integer currentPageIndex, Integer pageCount) ;

	/**
	 * 根据【通行证ID】查询【素材列表】
	 * @param passportId 【通行证ID】
	 * @param currentPageIndex 【当前页码】
	 * @param pageCount 【每页多少条】
	 * @return 【素材列表】
	 */
	ResponseResult queryMaterialListByPassportId(int passportId, Integer currentPageIndex, Integer pageCount) ;
	
	/**
	 * 保存素材
	 * @param MaterialVo 素材
	 * @return
	 */
	ResponseResult saveMaterial(Material material);

	/**
	 * 上传并保存素材
	 * @param materialTo
	 * @return
	 */
	ResponseResult saveMaterial(MaterialTo materialTo);

	/**
	 * 根据【ID】查询【素材】
	 * @param id 【ID】
	 * @return 【素材】
	 */
	ResponseResult queryMaterialById(String id, Integer width, Integer height);

	/**
	 * 根据【ID】查询【素材】
	 * @param id 【ID】
	 * @return 【素材】
	 */
	ResponseResult queryMaterialById(String id);
	
	/**
	 * 根据【IDs】批量删除【素材】
	 * @param ids 【IDs】
	 * @param passportId 【通行证ID】
	 * @return
	 */
	ResponseResult batchDeleteMaterialById(String ids, int passportId);
	
	/**
	 * 查询【素材列表】
	 * @param material 【素材】
	 * @return 【素材列表】
	 */
	ResponseResult queryMaterialList(Material material);
}
