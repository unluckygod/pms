package com.artup.service;

import com.artup.common.ResponseResult;
import com.artup.pojo.Role;

/**
 * 角色管理
 * @author hapday
 * @date 2017年3月8日 @Time 下午2:28:17
 */
public interface RoleService {
	/**
	 * 根据【ID】查询【角色】
	 * @param id
	 * @return
	 */
	public ResponseResult queryRoleById(long id) ;

	/**
	 * 模糊查询【角色列表】
	 * @param role
	 * @return
	 */
	public ResponseResult queryRoleList(Role role) ;
	
	/**
	 * 保存【角色】
	 * @param role
	 * @return
	 */
	public ResponseResult saveRole(Role role);

	/**
	 * 根据【ID】更新【角色】
	 * @param role
	 * @return
	 */
	public ResponseResult updateRoleById(Role role);

	/**
	 * 根据【ID】删除【角色】
	 * @param role
	 * @return
	 */
	public ResponseResult deleteRoleById(long id);

}
