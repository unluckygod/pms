package com.artup.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.artup.common.Constants;
import com.artup.common.ResponseResult;
import com.artup.dao.FeedbackDao;
import com.artup.pojo.Feedback;
import com.artup.service.FeedbackService;
import com.artup.util.CommonUtils;

@Service
public class FeedbackServiceImpl implements FeedbackService {
	
	protected static final Logger LOGGER = LoggerFactory.getLogger(FeedbackServiceImpl.class);
	
	@Autowired
	private FeedbackDao userFeedbackDao;

	@Override
	public ResponseResult saveFeedback(Feedback feedback) {
		LOGGER.info("feedback = {}", JSONObject.toJSONString(feedback));
		ResponseResult responseResult = new ResponseResult();
		if(null == feedback) {
			responseResult.setStatus(Constants.ACTION_STATUS_PARAMETER_EMPTY);
			responseResult.setMessage("无参数！");
			return responseResult;
		}  
		if(StringUtils.isBlank(feedback.getPassId())) {
			responseResult.setStatus(Constants.ACTION_STATUS_PARAMETER_EMPTY);
			responseResult.setMessage("【通行证ID】不可为空！");
			return responseResult;
		}
		if(StringUtils.isBlank(feedback.getComment())) {
			responseResult.setStatus(Constants.ACTION_STATUS_PARAMETER_NOT_POSITIVE_INTEGER);
			responseResult.setMessage("反馈意见不可为空！");
			return responseResult;
		}
		try {
			feedback.setId(CommonUtils.UUIDGenerator());
			this.userFeedbackDao.insertUserFeedback(feedback);
		} catch (Exception e) {
			LOGGER.error("保存意见反馈 - 失败！", e);
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("保存意见反馈 - 失败！");
			return responseResult;
		}
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("保存意见反馈  - 成功。");
		return responseResult;
	}

	@Override
	public ResponseResult queryFeedbackList(Feedback feedback) {
		LOGGER.info("feedback = {}", JSONObject.toJSONString(feedback));
		
		ResponseResult responseResult = new ResponseResult();
		Map<String, Object> resultMap = new HashMap<String, Object>(); // 结果集
		
		long totalCount = 100;
		
		resultMap.put("recordsTotal", totalCount);
		resultMap.put("recordsFiltered", totalCount);// 返回总条数
		
		List<Feedback> feedbackList = null;
		try {
			feedbackList = this.userFeedbackDao.selectFeedbackList(feedback);
		} catch (Exception e) {
			LOGGER.error("查询【意见反馈】列表 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("查询【意见反馈】列表 - 失败！");
			
			return responseResult;
		}
		
		resultMap.put("data", feedbackList);
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("查询【意见反馈】列表  - 成功。");
		responseResult.setData(resultMap);
		
		return responseResult;
	}

}
