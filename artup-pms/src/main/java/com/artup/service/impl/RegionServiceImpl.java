package com.artup.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.artup.common.Constants;
import com.artup.common.ResponseResult;
import com.artup.dao.RegionDao;
import com.artup.pojo.Region;
import com.artup.service.RegionService;
import com.artup.util.CommonUtils;
import com.artup.util.cache.RedisUtils;
import com.artup.vo.RegionVo;

import redis.clients.jedis.Jedis;

/**
 * 地区
 * @author hapday
 * @date 2017年7月20日 @Time 下午12:33:35
 */
@Service( value = "regionService" )
public class RegionServiceImpl implements RegionService {
	protected static final Logger LOGGER = LoggerFactory.getLogger(RegionServiceImpl.class);
	
	@Autowired
	private RegionDao regionDao;
	
	private static Jedis jedis = RedisUtils.getJedis();;

	@Override
	public ResponseResult queryAllRegion() {
		ResponseResult responseResult = new ResponseResult();
		List<Region> regionList = null;
		
		try {
			regionList = this.regionDao.selectAllRegion();
		} catch (Exception e) {
			LOGGER.error("查询全部的地区 - 失败！", e);

			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("查询全部的地区 - 失败！");
			
			return responseResult;
		}
		
		if(CommonUtils.isNotEmpty(regionList)) {
			List<RegionVo> regionVoList = new ArrayList<RegionVo>();
			
			for(Region region : regionList) {
				if(null == region) {
					continue;
				}
				
				RegionVo regionVo = new RegionVo();
				regionVo.setId(region.getId());
				regionVo.setParentId(region.getParentId());
				regionVo.setType(region.getType());
				regionVo.setName(region.getName());
				
				regionVoList.add(regionVo);
			}

			responseResult.setData(regionVoList);
		}
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("查询全部的地区 - 成功。");
		
		return responseResult;
	}

	@Override
	public ResponseResult queryAllRegion2() {
		ResponseResult responseResult = new ResponseResult();
		List<Region> regionList = null;
		
		String regions = jedis.get(Constants.CACHE_TYPE_REGION_LIST);
		
		/*String regions = null;
		List<String> regionList_ = jedis.mget(Constants.CACHE_TYPE_REGION_LIST);
		if(null != regionList_) {
			regions = regionList_.get(0);
		}*/
		
		if(StringUtils.isNotBlank(regions)) {
			regionList = JSONArray.parseArray(regions, Region.class);
		} else {
			try {
				regionList = this.regionDao.selectAllRegion();
			} catch (Exception e) {
				LOGGER.error("查询全部的地区 - 失败！", e);
				
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("查询全部的地区 - 失败！");
				
				return responseResult;
			}
		}
		
		if(CommonUtils.isNotEmpty(regionList)) {
			List<Region> provinceList = new ArrayList<Region>();
			List<Region> cityList = new ArrayList<Region>();
			List<Region> districtList = new ArrayList<Region>();
			
			for(Region region : regionList) {
				if(null == region) {
					continue;
				}
				
				if(1 == region.getType()) {
					Region province = new Region();
					province.setId(region.getId());
					province.setParentId(region.getParentId());
					province.setName(region.getName());
					
					provinceList.add(province);
				}
				if(2 == region.getType()) {
					Region city = new Region();
					city.setId(region.getId());
					city.setParentId(region.getParentId());
					city.setName(region.getName());
					
					cityList.add(city);
				}
				if(3 == region.getType()) {
					Region district = new Region();
					district.setId(region.getId());
					district.setParentId(region.getParentId());
					district.setName(region.getName());
					
					districtList.add(district);
				}
			}
			
			List<RegionVo> provinceVoList = new ArrayList<RegionVo>();
			
			for(Region province : provinceList) {
				List<RegionVo> cityVoList = new ArrayList<RegionVo>();
				
				for(Region city : cityList) {
					List<RegionVo> districtVoList = new ArrayList<RegionVo>();
					
					for(Region district : districtList) {
						if(city.getId() == district.getParentId()) {
							RegionVo districtVo = new RegionVo();
							districtVo.setId(district.getId());
							districtVo.setName(district.getName());
							
							districtVoList.add(districtVo);
						}
					}
					
					if(province.getId() == city.getParentId()) {
						RegionVo cityVo = new RegionVo();
						cityVo.setId(city.getId());
						cityVo.setName(city.getName());
						cityVo.setSubregionVo(districtVoList);
						
						cityVoList.add(cityVo);
					}
				}
				
				RegionVo provinceVo = new RegionVo();
				provinceVo.setId(province.getId());
				provinceVo.setName(province.getName());
				provinceVo.setSubregionVo(cityVoList);
				
				provinceVoList.add(provinceVo);
			}
			
			responseResult.setData(provinceVoList);
		}
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("查询全部的地区 - 成功。");
		
		return responseResult;
	}
}
