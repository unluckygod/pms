package com.artup.service.impl;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.scheduling.concurrent.CustomizableThreadFactory;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ThreadExecutorService {

	/**
	 * 线程池大小
	 */
	protected int threadPoolSize = 15;
	protected ExecutorService executorService; 

	public void destroy() throws Exception { 
		shutdown();
	}
	
	/**
	 * 关闭线程池服务
	 */
	public void shutdown() { 
		executorService.shutdownNow();
	}
	
	public void init() throws Exception { 
		if (this.executorService == null) {
			executorService = Executors.newFixedThreadPool(threadPoolSize,	new CustomizableThreadFactory(getClass().getSimpleName() + "-"));
		} 
		
		log.info("创建线程服务 - 成功。");
	}

	public ExecutorService getExecutorService() {
		return executorService;
	}
}
