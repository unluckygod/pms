package com.artup.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.artup.common.Constants;
import com.artup.common.ResponseResult;
import com.artup.service.CouponService;

public class CouponServiceImpl implements CouponService {
	private static final Logger LOGGER = LoggerFactory.getLogger(CouponServiceImpl.class);

	@Override
	public ResponseResult cancelUseCoupon(String couponCodes, int passportId) {
		LOGGER.info("couponCodes = {}, passportId = {}", couponCodes, passportId);
		
		ResponseResult responseResult = new ResponseResult();
		
		
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("取消使用券 - 成功。");
		
		return responseResult;
	}

}
