package com.artup.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.artup.common.Constants;
import com.artup.common.ResponseResult;
import com.artup.dao.MemberDao;
import com.artup.pojo.Member;
import com.artup.service.MemberService;
import com.artup.util.CommonUtils;
import com.artup.util.file.PropertiesUtils;

@Service( value = "memberService" )
public class MemberServiceImpl implements MemberService {
	protected static final Logger LOGGER = LoggerFactory.getLogger(MemberServiceImpl.class);
	
	@Autowired
	private MemberDao memberDao;

	@Override
	public ResponseResult saveMember(Member member) {
//		LOGGER.info("member = ", JsonGenerator.);
		try {
//			this.memberDao.insertMember(member);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ResponseResult updateMemberById(Member member) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseResult deleteMemberById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseResult queryMemberById(String id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public ResponseResult queryMemberList(Member member) {
		LOGGER.info("member = {}", JSONObject.toJSONString(member));
		
		// 定义返回的结果集
		ResponseResult responseResult = new ResponseResult();
		
		// 设置初始条件
		if (null != member) {
			if (CommonUtils.isNotEmpty(member.getBeginRegisterTime())) {
				member.setBeginRegisterTime(member.getBeginRegisterTime()
						+ " 00:00:00");
			}
			if (CommonUtils.isNotEmpty(member.getEndRegisterTime())) {
				member.setEndRegisterTime(member.getEndRegisterTime()
						+ " 23:59:59");
			}
		}

		// 模糊查询【资源】总记录数
		long totalCount = 0;
		try {
			totalCount = this.memberDao.selectMemberTotalCount(member);
		} catch (Exception e) {
			LOGGER.error("模糊查询【会员】总记录数 - 失败！", e);
		}

		Map<String, Object> resultMap = new HashMap<String, Object>(); // 结果集
		// 设置返回的结果集
		responseResult.setData(resultMap);
		
		resultMap.put("totalCount", totalCount);
		
		if(0 == totalCount) {
			resultMap.put("memberList", null);
			
			responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
			responseResult.setMessage("未找到会员！");
			
			return responseResult;
		}
		
		if(null == member.getPageIndex() || 0 >= member.getPageIndex()) {
			member.setPageIndex(1);
		}
		if(null == member.getPageSize() || 0 >= member.getPageSize()) {
			member.setPageSize(Integer.valueOf(PropertiesUtils.getValue("pagination.pageSize")));
		}
		
		member.setOffset( (long) ( (member.getPageIndex() - 1) * member.getPageSize() ) ); // 设置分页的偏移量，即起始记录数

		// 模糊查询【资源】列表
		List<Member> memberList = null;
		try {
			memberList = this.memberDao.selectMemberList(member);
		} catch (Exception e) {
			LOGGER.error("模糊查询【会员】列表 - 失败！", e);
		}

		resultMap.put("memberList", memberList);

		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("模糊查询【会员】列表 - 成功。");

		// 返回结果集
		return responseResult;
	}
}
