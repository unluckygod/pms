package com.artup.service.impl;

/**
 * <p>
 * 	1、出血是指在裁切时，为了避免由于机器和操作造成的误差，而预留的安全距离，一般多出裁切线大约3mm-5mm；
 *  2、作品框是指页面内容至裁切线的距离，可看做页边距；
 *  3、从外至内的排列顺序就是：最外边是出血线，中间是裁切线，最里边是作品框
 * </p>
 */

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.artup.pojo.MaterialEdit;
import com.artup.pojo.TextEdit;
import com.artup.util.CommonUtils;
import com.artup.util.pdf.PdfUtil;
import com.artup.util.pdf.PdfUtil.PdfParam;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.Barcode128;
import com.itextpdf.text.pdf.BarcodePDF417;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfRectangle;
import com.itextpdf.text.pdf.PdfWriter;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
@Getter
@Service( value = "pdfCreate" )
public class PdfCreator {
	/**
	 * 出血线与裁切框间距3mm
	 */
	public static final int BLEED_SPACE = 3;
	
	/**
	 * pdf页面边与裁切框间距15mm
	 */
	public static final int PDF_PAGE_SPACE = 15;
	
	/**
	 * 脚线长度5mm
	 * 即四个角的黑色线条
	 */
	public static final int FOOT_LINE_LENGTH = 5;
	
	/**
	 * 订单信息框高度为5mm
	 */
	public static final int ORDER_HEIGHT = 5;
	
	/**
	 * 条形码框宽25mm
	 */
	public static final int BARCODE_WIDTH = 25;
	
	/**
	 * 条形码框高8mm
	 */
	public static final int BARCODE_HEIGHT = 8;
	
	/**
	 * pdf模板编码
	 */
//	protected String code;
	
	/**
	 * pdf模板名称
	 */
//	protected String name;
	
	/**
	 * 裁切框(成品框)宽，单位：毫米
	 */
	private float cropWidth = 194f; 
	
	/**
	 * 裁切框(成品框)高，单位：毫米
	 */
	protected float cropHeight = 282f;
	
	/**
	 * 画芯图片框宽
	 */
	protected Float picWidth; 
	
	/**
	 * 画芯图片框高
	 */
	protected Float picHeight; 
	
	/**
	 * 计算pdf页面大小
	 * @return
	 */
	protected Rectangle getPdfBoxRectangle(){
		return null;
	};

	public void execute(PdfParam param, List<MaterialEdit> materialEditList) throws Exception {
		log.info("param = {}", JSONObject.toJSONString(param));
		log.info("materialEditList = {}", JSONObject.toJSONString(materialEditList));
		
		param.setRectangle(getPdfBoxRectangle());	// 设置 PDF 页面尺寸
		
		param.setDocumentBuilder(new PdfUtil.PdfDocumentBuilder(){
			@Override
			public void beforeOpen(Document doc, PdfParam param) throws Exception {
				beforeOpenDocument(doc, param);
			}
			
			@Override
			public void postOpen(PdfWriter writer, Document document, PdfParam param) throws Exception {
				log.info("<<<  ---------------------------- 开始添加内容 --------------------------------------------");
				
				//设置pdf内容
				doBuildContent(writer, document, materialEditList);
				
				log.info("---------------------------- 添加内容完成 -------------------------------------------- >>>");
			}
		});
		
		
		PdfUtil.buildPdf(null, param);
	}
	
	/**
	 * 设置pdf内容
	 * 
	 * @param writer
	 * @param document
	 * @param param
	 * @throws Exception
	 */
	private void doBuildContent(PdfWriter writer, Document document, List<MaterialEdit> materialEditList)throws Exception{
		try{
			if(CommonUtils.isNotEmpty(materialEditList)){
				for(MaterialEdit materialEdit : materialEditList){
					if(!document.newPage()){
						log.error("创建 PDF 新页面失败！");
						
						continue;
					}
					
					// 设置pdf内容
					addImageContent(document, materialEdit);
					
					/*TextEdit textEdit = new TextEdit();
					textEdit.setContent("固定式文本。One!");
					textEdit.setX(100F);
					textEdit.setY(200F);
					textEdit.setWidth(150F);
					textEdit.setHeight(300F);
					textEdit.setFontSize(36F);
					
					this.addTextContent(writer, document, textEdit);*/
					
					//设置出血框
					setBleedBox(writer, document);
					
					// 设置裁切框
					setArtBox(writer, document);
					
					// 设置脚线
					setFootLine(writer, document);
				}
				
				//内容创建完成后进行清理工作
				this.afterContentBuild(writer, document);
				
				writer.setViewerPreferences(PdfWriter.PageModeUseThumbs);
				
//				this.noticeMesOrder("");
			}
		}catch(Exception e){
			log.error("设置 PDF 各项参数出错了！", e);
			
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * 内容创建完成后进行清理工作
	 * 
	 * @param writer
	 * @param document
	 * @param param
	 */
	private void afterContentBuild(PdfWriter writer, Document document){
		
	}
	
	/**
	 * 打开document之前回调函数，可设置一些pdf属性
	 * 
	 * @param doc
	 * @param param
	 */
	private void beforeOpenDocument(Document document, PdfParam param){}
	
	/**
	 * 添加文字信息
	 * 
	 * fontSize = 8 时，
	 * 	数字字符宽度 ：4.4160004
	 * 	英文字符宽度 ：4.688
	 * 	汉字宽度	： 8.0
	 * 
	 * @param document
	 * @param textEdit
	 * @param cnf
	 * @throws Exception
	 */
	private void addTextContent(PdfWriter writer, Document document, TextEdit textEdit) throws Exception{
		String txtContent = textEdit.getContent();
		if(StringUtils.isBlank(txtContent)){
			log.error("{} No textContent ");
			return;
		}
		txtContent = txtContent.replaceAll("LF", "\\\n");
		float llx = PdfUtil.convertMm2Pt(textEdit.getX())  + PdfUtil.convertMm2Pt(BLEED_SPACE + FOOT_LINE_LENGTH);
		float lly = PdfUtil.convertMm2Pt(textEdit.getY()) + PdfUtil.convertMm2Pt(BLEED_SPACE + FOOT_LINE_LENGTH);
		
		float width = PdfUtil.convertMm2Pt(textEdit.getWidth());
		float height = PdfUtil.convertMm2Pt(textEdit.getHeight());
		
		//右上角坐标
		float rux = llx + width;
		float ruy = lly + height;
		
		float fontSize = textEdit.getFontSize(); //PdfUtil.convertMm2Pt(cnf.getFontSize());
		String fontName = textEdit.getFontName();
		fontName = StringUtils.isBlank(fontName) ? PdfParam.DEFAULT_FONT : fontName;
		fontName = fontName.startsWith("fonts") ? fontName : "fonts/" + fontName;
		Phrase phrase = new Phrase(txtContent, this.getChineseFont(fontName, (int)fontSize));
		
		//行间距
		float maxLeadding = phrase.getTotalLeading();
		int align = Element.ALIGN_CENTER;
		
		PdfContentByte content = writer.getDirectContent(); 
		content.beginText();
		
		ColumnText columnText = new ColumnText(content);	// 列文本
		 
		columnText.setSimpleColumn(phrase, llx, lly, rux, ruy, maxLeadding, align);
		
		//文字透明度
		PdfGState gs = new PdfGState();
		gs.setFillOpacity(1f);
		content.setGState(gs);
		
		columnText.setInheritGraphicState(true);
		columnText.go();
		
		content.endText();
		content.stroke();
		
		document.add(phrase);
		
		log.debug("添加文本页面 [ \"txtContent\" : \"{}\", \"llx\" : {}pt, \"lly\" : {}pt, \"width\" : {}pt, \"height\" : {}pt ] ", txtContent, llx, lly, width, height);
	}
	
	private void addImageContent(Document document, MaterialEdit materialEdit) throws Exception{
		if(null == materialEdit) {
			log.warn("无参数！");
			
			return;
		}
		if(StringUtils.isBlank(materialEdit.getEditedMaterialPath())){
			log.warn("无编辑的素材图片！");
			
			return;
		}
		
		// 作品图片
		Image image = Image.getInstance(materialEdit.getEditedMaterialPath());
		float width = PdfUtil.pixel2point_150ppi(materialEdit.getWidth());	// 图片框宽
		float height = PdfUtil.pixel2point_150ppi(materialEdit.getHeight());	// 图片框高

		image.scaleAbsolute(width, height);	// 设置“作品图片”的缩放“宽度”和“高度”
		
		Rectangle pdfBoxRectangle = document.getPageSize();
		float lowerLeftAbscissa = (pdfBoxRectangle.getWidth() - PdfUtil.convertMm2Pt(BLEED_SPACE + FOOT_LINE_LENGTH + PDF_PAGE_SPACE) - width) / 2 + PdfUtil.convertMm2Pt(11);
		float lowerLeftOrdinate = (pdfBoxRectangle.getHeight() - PdfUtil.convertMm2Pt(BLEED_SPACE + FOOT_LINE_LENGTH + PDF_PAGE_SPACE) - height) / 2 + PdfUtil.convertMm2Pt(11);
		
		image.setAbsolutePosition(lowerLeftAbscissa, lowerLeftOrdinate);		// 设置图片的“横坐标”和“纵坐标”的绝对地址
		
		log.debug("添加图片到 PDF 页面中 [ \"image\" : \"{}\", \"llx\" : {}pt, \"lly\" : {}pt, \"width\" : {}pt, \"height\" : {}pt ] ", materialEdit.getEditedMaterialPath(), lowerLeftAbscissa, lowerLeftOrdinate, width, height);
		
		document.add(image);
	}
	
	/**
	 * 默认中文字体，微软雅黑
	 * @param fontSize
	 * @return
	 * @throws IOException
	 * @throws DocumentException
	 */
	protected Font getDefaultChineseFont(int fontSize)throws IOException,DocumentException{
		return this.getChineseFont(PdfParam.DEFAULT_FONT, fontSize);
	}
	
	protected Font getChineseFont(String fontName, int fontSize)throws IOException,DocumentException{
		fontName = StringUtils.isBlank(fontName) ? PdfParam.DEFAULT_FONT : fontName;
		BaseFont bfChinese = BaseFont.createFont(fontName, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
		bfChinese = BaseFont.createFont(PdfUtil.PdfParam.DEFAULT_FONT, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
	    Font fontChinese = new Font(bfChinese, fontSize, Font.NORMAL);
	    return fontChinese;
	}
	
	/**
	 * 生成条形码
	 * 
	 * @param content
	 * @param barCodeInfo
	 * @param width
	 * @param height
	 * @param encode
	 * @param format
	 * @return
	 * @throws Exception
	 */
	protected Image generateBarCode128(PdfContentByte content, String barCodeInfo)throws Exception{
		if (StringUtils.isBlank(barCodeInfo)) {
			throw new Exception("条形码的文本信息参数不能为空！");
		}
		com.itextpdf.text.pdf.Barcode128 barcodePDF = new com.itextpdf.text.pdf.Barcode128();
		barcodePDF.setCode(barCodeInfo);
		barcodePDF.setCodeType(Barcode128.CODE128);
		//barcodePDF.setFont(null);
		barcodePDF.setChecksumText(false);
		barcodePDF.setGenerateChecksum(false);
		return barcodePDF.createImageWithBarcode(content, BaseColor.BLACK, BaseColor.BLACK);
	}
	
	/**
	 * 
	 * @param barCodeInfo
	 * @return
	 * @throws Exception
	 */
	public byte[] generateBarCode417(String barCodeInfo)throws Exception{
		int width = (int)PdfUtil.convertMm2Pt(BARCODE_WIDTH);
		int height = (int)PdfUtil.convertMm2Pt(BARCODE_HEIGHT);
		return generateBarCode417(barCodeInfo, width, height, "UTF-8", "jpg");
	}
	
	/**
	 * 
	 * @param barCodeInfo
	 * @param encode
	 * @param format
	 * @return
	 * @throws Exception
	 */
	protected byte[] generateBarCode417(String barCodeInfo, int width, int height, String encode, String format)throws Exception{
		if (StringUtils.isBlank(barCodeInfo)) {
			throw new Exception("条形码的文本信息参数不能为空！");
		} 
		 
		BarcodePDF417 barcodePDF = new BarcodePDF417();
		barcodePDF.setText(barCodeInfo.getBytes(encode));
		java.awt.Image pdfImg = barcodePDF.createAwtImage(Color.black, Color.white);
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		java.awt.Graphics g = img.getGraphics();
		g.drawImage(pdfImg, 0, 0, Color.white, null);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try{
			ImageIO.write(img, format, os);
			return os.toByteArray();
		}catch(Exception e){
			log.error(e.getMessage(), e);
			throw new Exception(e.getMessage());
		}finally{
			if(os != null){
				os.close();
			}
		}
	}
	
//	protected byte[] generateBarCodeJpgByZxing(String barCodeInfo)throws Exception{
//		int width = (int)PdfUtil.convertMm2Pt(BARCODE_WIDTH);
//		int height = (int)PdfUtil.convertMm2Pt(BARCODE_HEIGHT);
//		return generateBarCodeJpgByZxing(barCodeInfo, width, height, "UTF-8", "jpg");
//	}
	
//	/**
//	 * 使用zxing库生成条形码
//	 * 
//	 * @param barCodeInfo
//	 * @param width
//	 * @param height
//	 * @param encode
//	 * @param format
//	 * @return
//	 * @throws Exception
//	 */
//	protected byte[] generateBarCodeJpgByZxing(String barCodeInfo, int width, int height, String encode, String format)throws Exception{
//		if (StringUtils.isBlank(barCodeInfo)) {
//			throw new Exception("条形码的文本信息参数不能为空！");
//		}
//		
//		ByteArrayOutputStream os = null;
//		try{
//			// 文字编码  
//	        Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();  
//	        hints.put(EncodeHintType.CHARACTER_SET, encode);
//	        BitMatrix bitMatrix = new MultiFormatWriter().encode(barCodeInfo, BarcodeFormat.CODE_128, height, height, hints);  
//	        
//	        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
//	        os = new ByteArrayOutputStream();
//	        for (int x = 0; x < width; x++){  
//	            for (int y = 0; y < height; y++){  
//	            	img.setRGB(x, y, bitMatrix.get(x, y) ? BLACK : WHITE);  
//	            }  
//	        }  
//	        ImageIO.write(img, format, os);
//			return os.toByteArray();
//		}catch(Exception e){
//			log.error(e.getMessage(), e);
//			throw new Exception(e.getMessage());
//		}finally{
//			if(os != null){
//				os.close();
//			}
//		}
//		 
//	}
	
	/**
	 * 根据用户的操作对原图进行操作
	 * 
	 * @param actions
	 * @param pdfParam
	 */
//	protected void doExecuteInterceptor(Document doc, PdfParam param, PdfContentEntity content, int currentPage){
//		//操作 
//		Collection<Action> actions = content.getActions();
//		
//		if(actions == null || actions.isEmpty()){
//			log.warn("{} No action for building pdf ", Mars.LOG_PRE);
//			return;
//		}
//		
////		InterceptorHandlerChain<PdfContentEntity, String> chain = new DefaultInterceptorHandlerChain<PdfContentEntity, String>();
////		for(Action action : actions){
////			String actionCode = action.getCode();
////			if(StringUtils.isBlank(actionCode)){
////				continue;
////			}
////			
////			InterceptorHandler<PdfContentEntity, String> inter = SpringContextHolder.getBean(actionCode + "InterceptorHandler");
////			chain.addLast(actionCode, inter);
////		}
//		
//		InterceptorExecutor.doExecuteWithContextName("pdfCreatorInterceptorContext", content);//.doExecute(chain, content);
//	}
	
	/**
	 * 设置出血框
	 * @param writer
	 * @param document
	 * @param param
	 */
	protected void setBleedBox(PdfWriter writer, Document document){
		Rectangle bleedBox = this.getBleedBox(document);
		
//		bleedBox.setLeft(10);	// test
//		bleedBox.setTop(10);	// test
//		bleedBox.setRight(50);	// test
//		bleedBox.setBottom(100);	// test
		
		PdfRectangle pdfBleedBox = new PdfRectangle(bleedBox.getLeft(), bleedBox.getBottom(),bleedBox.getRight(), bleedBox.getTop());
		writer.getPageDictEntries().put(PdfName.BLEEDBOX, pdfBleedBox);
		log.debug("设置【出血框】 [ width : {} pt, height : {} pt ] ", pdfBleedBox.width(), pdfBleedBox.height());
	}
	
	/**
	 * 设置裁切(成品)框
	 * 
	 * @param writer
	 * @param document
	 * @param param
	 * @param curPage
	 */
	private void setArtBox(PdfWriter writer, Document document){
		Rectangle artBox = getArtBox(document);
//		artBox.setBackgroundColor(BaseColor.BLACK);		// test
		
		PdfRectangle pdfArtBox = new PdfRectangle(artBox.getLeft(), artBox.getBottom(),artBox.getRight(), artBox.getTop());
		
		//成品框
		writer.getPageDictEntries().put(PdfName.ARTBOX, pdfArtBox);
		
		//裁切框
		writer.getPageDictEntries().put(PdfName.TRIMBOX, pdfArtBox);
		
		log.debug("设置【裁切框】- [ width : {} pt, height : {} pt ] ", pdfArtBox.width(), pdfArtBox.height());
	}
	
	/**
	 * 设置脚线
	 * 
	 * @param writer
	 * @param document
	 * @param param
	 */
	protected void setFootLine(PdfWriter writer, Document document){
		PdfContentByte pdfContentByte = writer.getDirectContent();	// PDF 内容

		float footLine = PdfUtil.convertMm2Pt(FOOT_LINE_LENGTH);	// 脚线长
		
		// 出血框
		Rectangle bleedBoxRectangle = this.getBleedBox(document);
		float bleedWidth = bleedBoxRectangle.getWidth();
		float bleedHeight = bleedBoxRectangle.getHeight();
		
		Rectangle pdfBoxRectangle = document.getPageSize();	// PDF 页面大小，默认采用的是 A4 纸的尺寸
		float pdfWidth = pdfBoxRectangle.getWidth();
		float pdfHeight = pdfBoxRectangle.getHeight();
		
		// 左下角 x 方向上脚线起点
		float llxX = (pdfWidth - bleedWidth) / 2 - footLine;
		float llyX = (pdfHeight - PdfUtil.convertMm2Pt(this.getCropHeight())) / 2;
		pdfContentByte.moveTo(llxX, llyX);
		pdfContentByte.lineTo(llxX + footLine, llyX);
		
		// 右下角 x 方向上脚线起点
		float rlxX = pdfWidth - (pdfWidth - bleedWidth) / 2;
		float rlyX = llyX;
		pdfContentByte.moveTo(rlxX, rlyX);
		pdfContentByte.lineTo(rlxX + footLine, rlyX);
				
		// 左上角 x 方向上脚线起点
		float luxX = llxX;
		float luyX = pdfHeight - (pdfHeight - PdfUtil.convertMm2Pt(this.getCropHeight())) / 2;
		pdfContentByte.moveTo(luxX, luyX);
		pdfContentByte.lineTo(luxX + footLine, luyX);
		
		// 右上角 x 方向上脚线起点
		float ruxX = rlxX; 
		float ruyX = luyX;
		pdfContentByte.moveTo(ruxX, ruyX);
		pdfContentByte.lineTo(ruxX + footLine, ruyX);
		
		//左下角y方向上脚线起点
		float llxY = (pdfWidth - PdfUtil.convertMm2Pt(this.getCropWidth())) / 2;
		float llyY = (pdfHeight - bleedHeight) / 2 - footLine;
		pdfContentByte.moveTo(llxY, llyY);
		pdfContentByte.lineTo(llxY, llyY + footLine);
		
		//左上角y方向上脚线起点
		float luxY = llxY;
		float luyY = pdfHeight - (pdfHeight - bleedHeight) / 2;
		pdfContentByte.moveTo(luxY, luyY);
		pdfContentByte.lineTo(luxY, luyY + footLine);
		
		//右下角y方向上脚线起点
		float rlxY = pdfWidth - (pdfWidth - PdfUtil.convertMm2Pt(this.getCropWidth())) / 2;
		float rlyY = llyY;
		pdfContentByte.moveTo(rlxY, rlyY);
		pdfContentByte.lineTo(rlxY, rlyY + footLine);
		
		//右上角y方向上脚线起点
		float ruxY = rlxY;
		float ruyY = pdfHeight - (pdfHeight - bleedHeight) / 2;
		pdfContentByte.moveTo(ruxY, ruyY);
		pdfContentByte.lineTo(ruxY, ruyY + footLine);
		
		pdfContentByte.stroke();
		
		log.debug("设置【脚线】 - 完成。");
	}
	
	/**
	 * 获取【出血框】
	 * @param document
	 * @return
	 */
	private Rectangle getBleedBox(Document document){
		/*
		 * 出血框
		 */
		float bleedBoxWidth = this.getCropWidth() + 2 * BLEED_SPACE;	// 出血框宽度（单位：毫米）
		float bleedBoxHeight = this.getCropHeight() + 2 * BLEED_SPACE;		// 出血框高度（单位：毫米）
		
		float bleedBoxWidthPt = PdfUtil.convertMm2Pt(bleedBoxWidth);		// 出血框宽度（单位：点）
		float bleedBoxHeightPt = PdfUtil.convertMm2Pt(bleedBoxHeight);	// 出血框高度（单位：点）
		
		Rectangle pdfBoxRectangle = document.getPageSize();		// 获取 PDF 页面大小
		
		float lowerLeftAbscissa = (pdfBoxRectangle.getWidth() - bleedBoxWidthPt) / 2;	// 左下角的横坐标
		float lowerLeftOrdinate = (pdfBoxRectangle.getHeight() - bleedBoxHeightPt) / 2;		// 左下角的纵坐标
		
		Rectangle bleedBoxRectangle = new Rectangle(lowerLeftAbscissa, lowerLeftOrdinate, lowerLeftAbscissa + bleedBoxWidthPt, lowerLeftOrdinate + bleedBoxHeightPt);	// 出血框
		
		log.debug("【出血框】横坐标：{}，纵坐标：{}，宽：{}，高：{}", lowerLeftAbscissa, lowerLeftOrdinate, (lowerLeftAbscissa + bleedBoxWidthPt), (lowerLeftOrdinate + bleedBoxHeightPt));
		
		return bleedBoxRectangle;
	}
	
	/**
	 * 成品框
	 * 
	 * @param document
	 * @return
	 */
	private Rectangle getArtBox(Document document){
		//出血框宽单位pt
		float artWidthPt = PdfUtil.convertMm2Pt(this.getCropWidth());
		float artHeightPt = PdfUtil.convertMm2Pt(this.getCropHeight());
		Rectangle pdfBox = document.getPageSize();
		float llx = (pdfBox.getWidth() - artWidthPt) / 2;
		float lly = (pdfBox.getHeight() - artHeightPt) / 2;
		
		return new Rectangle(llx, lly, llx + artWidthPt, lly + artHeightPt);
	}
	
	/**
	 * 在指定位置增加增加条形码
	 * 
	 * @param document
	 * @param llx
	 * @param lly
	 * @param barCode
	 */
	protected void addBarCode128Content(PdfWriter writer, Document document, float llx, float lly, int align, String barCode){
		try{
			log.debug("添加条形码，\"{}\" at [ {},{} ] align : {} ", barCode, llx, lly, align);
			
			int width = (int)PdfUtil.convertMm2Pt(BARCODE_WIDTH);
			int height = (int)PdfUtil.convertMm2Pt(BARCODE_HEIGHT);
			
			com.itextpdf.text.Image img = generateBarCode128(writer.getDirectContent(), barCode); 
			img.setAlignment(align);
			img.setAbsolutePosition(llx, lly);
			img.scaleAbsolute(width, height);
			document.add(img);
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
	}
	
	/**
	 * 在指定位置宽高矩形内增加文本内容
	 * 
	 * @param writer
	 * @param llx 左下角x坐标
	 * @param lly 左下角y坐标
	 * @param width 宽
	 * @param height 高
	 * @param txtContent 文字内容
	 * @param fontSize 字体大小
	 * @param align 对齐方式 0:左对齐； 1：居中对齐； 2：右对齐
	 * 
	 * @throws Exception
	 */
	protected void addTextContent(PdfWriter writer, float llx, float lly, float width, float height, String txtContent, int fontSize, int align)throws Exception{
		PdfContentByte content = writer.getDirectContent();
		
		if(StringUtils.isNotBlank(txtContent)){
			txtContent = txtContent.replaceAll("LF", "\\\n");
		}
		content.beginText();
		ColumnText columnTxt = new ColumnText(content);
		Phrase orderTxt = new Phrase(txtContent, this.getDefaultChineseFont((int)fontSize));
//		//右上角坐标
		float rux = llx + width;
		float ruy = lly + height;
		
		//log.debug("{} add txtContent \"{}\" at [ {},{} {},{} ] ", Mars.LOG_PRE, txtContent, llx, lly, rux, ruy);
		
		columnTxt.setSimpleColumn(orderTxt, llx, lly, rux, ruy, 16, align);
		
		//文字透明度
		PdfGState gs = new PdfGState();
		gs.setFillOpacity(1f);
		content.setGState(gs);
		
		columnTxt.setInheritGraphicState(true);
		columnTxt.go();
		
		content.endText();
		content.stroke();
	}
}
