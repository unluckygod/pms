package com.artup.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.artup.common.Constants;
import com.artup.common.OrderStatus;
import com.artup.common.PaymentStatus;
import com.artup.common.ResponseResult;
import com.artup.dao.AddressDao;
import com.artup.dao.InvoiceDao;
import com.artup.dao.MaterialDao;
import com.artup.dao.MaterialEditDao;
import com.artup.dao.OrderDao;
import com.artup.dao.OrderWorksRelationDao;
import com.artup.dao.PaymentDao;
import com.artup.dao.SkuDao;
import com.artup.dao.TrolleyDetailDao;
import com.artup.dao.WorksDao;
import com.artup.pojo.Address;
import com.artup.pojo.Invoice;
import com.artup.pojo.Material;
import com.artup.pojo.MaterialEdit;
import com.artup.pojo.Order;
import com.artup.pojo.OrderWorksRelation;
import com.artup.pojo.Payment;
import com.artup.pojo.Region;
import com.artup.pojo.TrolleyDetail;
import com.artup.pojo.Works;
import com.artup.service.OrderService;
import com.artup.service.task.ExecutorTask;
import com.artup.util.CommonUtils;
import com.artup.util.cache.RedisUtils;
import com.artup.util.excel.OrderExport;
import com.artup.util.file.FolderUtils;
import com.artup.util.file.PropertiesUtils;
import com.artup.util.http.HttpClientUtil;
import com.artup.util.image.ImageUtil;
import com.artup.util.os.OperationSystemInfo;
import com.artup.util.pdf.PdfUtil;
import com.artup.util.shell.ChmodShell;
import com.artup.vo.InvoiceVo;
import com.artup.vo.PaymentVo;
import com.artup.vo.TrolleyDetailVo;

import redis.clients.jedis.Jedis;

/**
 * 订单
 * @author hapday
 * @date 2017年7月17日 @Time 下午12:07:23
 */
@Service( value = "orderService" )
public class OrderServiceImpl implements OrderService {
	protected static final Logger LOGGER = LoggerFactory.getLogger(OrderServiceImpl.class);
	
	private final int WORKS_BOX_WIDTH = 968;		// 作品框的宽度（单位：像素）
	private final int WORKS_BOX_HEIGHT = 1482;		// 作品框的高度（单位：像素）
	
	private static Jedis jedis = RedisUtils.getJedis();
	
	@Autowired
	private WorksDao worksDao;

	@Autowired
	private SkuDao skuDao;

	@Autowired
	private TrolleyDetailDao trolleyDetailDao;

	@Autowired
	private MaterialDao materialDao;

	@Autowired
	private MaterialEditDao materialEditDao;

	@Autowired
	private OrderDao orderDao;

	@Autowired
	private OrderWorksRelationDao orderWorksRelationDao;

	@Autowired
	private AddressDao addressDao;

	@Autowired
	private InvoiceDao invoiceDao;

	@Autowired
	private PaymentDao paymentDao;
	
	@Autowired
	private ExecutorTask executorTask;
	
	@Autowired
	private HttpClientUtil httpClientUtil;

	@Override
	public ResponseResult saveTrolley(String worksJSONObject) {
		LOGGER.info("worksJSONObject = {}", worksJSONObject);
		
		ResponseResult responseResult = new ResponseResult();
		
		if(StringUtils.isBlank(worksJSONObject)) {
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("无参数！");
			
			return responseResult;
		}
		
		JSONObject worksJSONObject_ = JSONObject.parseObject(worksJSONObject);
		
		Works works = new Works();	// 作品
		String worksId = CommonUtils.UUIDGenerator(); 	// 作品ID
		works.setId(worksId);
		works.setStatus((byte) 2);
		
		TrolleyDetail trolleyDetail = new TrolleyDetail();		// 购物车明细
		String trolleyDetailId = CommonUtils.UUIDGenerator(); 	// 购物车明细ID
		trolleyDetail.setId(trolleyDetailId);	
		trolleyDetail.setWorksId(worksId);
		
		String skuCode2 = null;		// 编辑过后重新选择的 SKU
		
		if(worksJSONObject_.containsKey("passportId")) {
			works.setPassportId(worksJSONObject_.getIntValue("passportId"));

			trolleyDetail.setPassportId(worksJSONObject_.getIntValue("passportId"));
		}
		
		if(worksJSONObject_.containsKey("worksName")) {
			works.setName(worksJSONObject_.getString("worksName"));
		}
		if(worksJSONObject_.containsKey("worksSize")) {
			works.setSize(worksJSONObject_.getString("worksSize"));
		}
		if(worksJSONObject_.containsKey("worksQuantity")) {
			works.setQuantity(worksJSONObject_.getIntValue("worksQuantity"));
			
			trolleyDetail.setQuantity(worksJSONObject_.getIntValue("worksQuantity"));
		}
		if(worksJSONObject_.containsKey("worksTypeCode")) {
			works.setTypeCode(worksJSONObject_.getString("worksTypeCode"));
		}
		if(worksJSONObject_.containsKey("worksShape")) {
			works.setShape(worksJSONObject_.getString("worksShape"));
		}
		if(worksJSONObject_.containsKey("worksColor")) {
			works.setColor(worksJSONObject_.getString("worksColor"));
		}
		if(worksJSONObject_.containsKey("worksSize")) {
			works.setSize(worksJSONObject_.getString("worksSize"));
		}
		if(worksJSONObject_.containsKey("worksThumbnailId")) {
			works.setThumbnailId(worksJSONObject_.getString("worksThumbnailId"));
		}
		if(!worksJSONObject_.containsKey("worksChannelCode") || StringUtils.isBlank(worksJSONObject_.getString("worksChannelCode"))) {
			works.setChannelCode("artron");
			
			trolleyDetail.setChannelCode("artron");
		} else {
			works.setChannelCode(worksJSONObject_.getString("worksChannelCode"));
			
			trolleyDetail.setChannelCode(worksJSONObject_.getString("worksChannelCode"));
		}
		if(!worksJSONObject_.containsKey("worksClientCode") || StringUtils.isBlank(worksJSONObject_.getString("worksClientCode"))) {
			works.setClientCode("ios");
			
			trolleyDetail.setClientCode("ios");
		} else {
			works.setClientCode(worksJSONObject_.getString("worksClientCode"));
			
			trolleyDetail.setClientCode(worksJSONObject_.getString("worksClientCode"));
		}
		if(worksJSONObject_.containsKey("skuCode")) {
			works.setSkuCode(worksJSONObject_.getString("skuCode"));
			
			trolleyDetail.setSkuCode(worksJSONObject_.getString("skuCode"));
		}
		if(worksJSONObject_.containsKey("skuCode2")) {
			skuCode2 = worksJSONObject_.getString("skuCode2");
		}
		if(StringUtils.isNotBlank(works.getTypeCode()) && StringUtils.isNotBlank(works.getSize())) {
			works.setTemplateId(works.getTypeCode() + "_" + works.getSize());
		}
		
		Float serverPrice = null;
		try {
			serverPrice = this.skuDao.selectPriceByCode(works.getSkuCode());
		} catch (Exception e) {
			LOGGER.error("根据【编号】查询【单价】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("根据【编号】查询【单价】 - 失败！");
			
			return responseResult;
		}
		
		if(null != serverPrice) {
			works.setPrice(serverPrice);
			
			trolleyDetail.setPrice(serverPrice);
			
			if(0 < trolleyDetail.getQuantity()) {
				trolleyDetail.setSubtotal(serverPrice * trolleyDetail.getQuantity());
			}
		}
		
		if(worksJSONObject_.containsKey("materialEdit") && StringUtils.isNotBlank(worksJSONObject_.getString("materialEdit"))) {
			JSONArray materialEditJSONArray = JSONArray.parseArray(worksJSONObject_.getString("materialEdit"));
			
			if(null != materialEditJSONArray) {
				for(int cursor = 0; cursor < materialEditJSONArray.size(); cursor++) {
					JSONObject materialEditJSONObject = materialEditJSONArray.getJSONObject(cursor);
					
					if(null == materialEditJSONObject) {
						continue;
					}
					
					MaterialEdit materialEdit = new MaterialEdit();
					String worksEditId = CommonUtils.UUIDGenerator();
					materialEdit.setId(worksEditId);
					materialEdit.setWorksId(worksId);
					materialEdit.setStatus((byte) 1);
					
					String materialId = null;	// 素材ID
					int x_cut = 0;	// X 起始坐标
					int y_cut = 0;	// Y 起始坐标
					int width_cut = 0;	// （裁切）宽度
					int height_cut = 0;	// （裁切）高度
					int rotate = 0;	// 旋转角度
					float scale = 1;	// 放大或缩小的倍数
					
					if(materialEditJSONObject.containsKey("materialId")) {
						if(StringUtils.isNotBlank(materialEditJSONObject.getString("materialId"))) {
							materialId = materialEditJSONObject.getString("materialId");
							materialEdit.setMaterialId(materialId);
						}
					}
					
					if(materialEditJSONObject.containsKey("materialIndex")) {
						if(0 < materialEditJSONObject.getInteger("materialIndex")) {
							materialEdit.setMaterialIndex(materialEditJSONObject.getInteger("materialIndex"));
						}
					}
					
					JSONObject actionJSONObject = new JSONObject();		// 动作/操作步骤
					
					if(materialEditJSONObject.containsKey("x")) {
						x_cut = materialEditJSONObject.getIntValue("x");
						LOGGER.debug("x_cut = {}", x_cut);
						
						actionJSONObject.put("x", x_cut);
					}
					if(materialEditJSONObject.containsKey("y")) {
						y_cut = materialEditJSONObject.getIntValue("y");
						LOGGER.debug("y_cut = {}", y_cut);
						
						actionJSONObject.put("y", y_cut);
					}
					if(materialEditJSONObject.containsKey("width")) {
						width_cut = materialEditJSONObject.getIntValue("width");
						LOGGER.debug("width_cut = {}", width_cut);
						
						actionJSONObject.put("width", width_cut);
					}
					if(materialEditJSONObject.containsKey("height")) {
						height_cut = materialEditJSONObject.getIntValue("height");
						LOGGER.debug("height_cut = {}", height_cut);
						
						actionJSONObject.put("height", height_cut);
					}
					if(materialEditJSONObject.containsKey("rotate")) {	// 旋转
						rotate = materialEditJSONObject.getIntValue("rotate");	// 旋转角度
						actionJSONObject.put("rotate", rotate);

						LOGGER.debug("rotate = {}", rotate);
					}
					
					if(materialEditJSONObject.containsKey("scale")) {	// 放大/缩小
						scale = materialEditJSONObject.getFloat("scale");
						
						actionJSONObject.put("scale", scale);
						LOGGER.debug("scale = {}", scale);
					}
					
					if(StringUtils.isBlank(skuCode2)) {
						this.buildPdfImage(responseResult, materialEdit, works, x_cut, y_cut, width_cut, height_cut, rotate, scale);
					} else {
//						skuCode2 = "B01005";	// test
//						skuCode2 = "B01001";	// test
						
						this.buildPdfImage2(responseResult, materialEdit, works, x_cut, y_cut, width_cut, height_cut, rotate, scale, skuCode2);
					}
					
					if(null != actionJSONObject) {
						materialEdit.setAction(actionJSONObject.toJSONString());
					}
					
					try {
						this.materialEditDao.insertMaterialEdit(materialEdit);
					} catch (Exception e) {
						LOGGER.error("添加【素材编辑】 - 失败！", e);
						
						responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
						responseResult.setMessage("添加【素材编辑】 - 失败！");
						
						return responseResult;
					}
				}
			}
		}
		
		try {
			this.worksDao.insertWorks(works);
		} catch (SQLException e) {
			LOGGER.error("添加【作品】 - 失败！");
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("添加【作品】 - 失败！");
		}
		
		try {
			this.trolleyDetailDao.insertTrolleyDetail(trolleyDetail);
		} catch (Exception e) {
			LOGGER.error("添加【购物车详情】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
			responseResult.setMessage("添加【购物车详情】 - 失败！");
			
			return responseResult;
		}
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("添加【购物车】 - 成功。");
		
		return responseResult;
	}

	public ResponseResult updateTrolleyNum(Integer passportId,String edtId,Integer num){
		LOGGER.info("passportId={},edtId={},num={}",passportId,edtId,num);
		ResponseResult responseResult = new ResponseResult();
		if(null == passportId || 0 >= passportId) {
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("【通行证ID】不合法！");
			return responseResult;
		}
		if(StringUtils.isBlank(edtId)) {
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("作为ID无参数！");
			return responseResult;
		}
		if(null == num || 0 >= num) {
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("【购物车数量】不合法！");
			return responseResult;
		}
		try {
			this.trolleyDetailDao.updateTrolleyNum(String.valueOf(passportId), edtId, num);
			//this.trolleyDetailDao.updateTrolleyNum(String.valueOf(passportId), edtId, num);
		} catch (Exception e) {
			LOGGER.error("根据【passportId, edtId】修改【购物车数量】 - 失败！", e);
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("修改购物车数量 - 失败！");
			return responseResult;
		}
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("修改购物车数量 - 成功。");
		return responseResult;
	}
	
	@Override
	public ResponseResult createOrder(Order order) {
		LOGGER.info("order = {}", JSONObject.toJSONString(order));
		
		ResponseResult responseResult = new ResponseResult();
		
		if(null == order) {
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("无参数！");
			
			return responseResult;
		}
		
		if(StringUtils.isNotBlank(order.getInvoiceTitle()) && StringUtils.isNotBlank(order.getInvoiceContent())) {
			// 发票
			Invoice invoice = new Invoice();
			invoice.setPassportId(order.getPassportId());
			invoice.setTitle(order.getInvoiceTitle());
			invoice.setContent(order.getInvoiceContent());
			
			try {
				this.invoiceDao.insertInvoice(invoice);
				
				order.setInvoiceId(invoice.getId());
			} catch (SQLException e) {
				LOGGER.error("添加【发票】 - 失败！", e);
				
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("添加【发票】 - 失败！");
				
				return responseResult;
			}
		}

		String orderId = CommonUtils.UUIDGenerator();  	// 订单ID
		String code = CommonUtils.orderCodeGenerator(); 		// 订单编号
		order.setId(orderId);
		order.setCode(code);
		order.setStatus(OrderStatus.getValue(OrderStatus.CREATED));
		
		try {
			this.orderDao.insertOrder(order);
		} catch (Exception e) {
			LOGGER.error("添加【订单】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("添加【订单】 - 失败！");
			
			return responseResult;
		}
		
		Payment payment = new Payment();
		payment.setId(CommonUtils.UUIDGenerator());
		payment.setOrderId(orderId);
		payment.setRemark("普通订单");
		payment.setStatus(PaymentStatus.getValue(PaymentStatus.UNPAID));
		
		try {
			this.paymentDao.insertPayment(payment);
		} catch (SQLException e) {
			LOGGER.error("添加【支付流水】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("添加【支付流水】 - 失败！");
			
			return responseResult;
		}
		
		List<TrolleyDetail> trolleyDetailList = null;
		
		if(StringUtils.isNotBlank(order.getTrolleyDetailIds())) {
			try {
				trolleyDetailList = this.trolleyDetailDao.selectTrolleyDetailListByIds(order.getTrolleyDetailIds().split(","));
			} catch (SQLException e) {
				LOGGER.error("根据【IDs】查询【购物车详情】列表 - 失败！", e);
				
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("根据【IDs】查询【购物车详情】列表 - 失败！");
				
				return responseResult;
			}
			
			if(CommonUtils.isEmpty(trolleyDetailList)) {
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("未找到【购物车详情】列表！");
				
				return responseResult;
			}
			
			try {
				this.trolleyDetailDao.batchUpdateTrolleyDetailStatusAndOrderIdByIds(order.getTrolleyDetailIds().split(","), (byte) 2, orderId);
			} catch (Exception e) {
				LOGGER.error("根据【IDs】更新【购物车详情】的【状态】 - 失败！", e);
				
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("根据【IDs】更新【购物车详情】的【状态】 - 失败！");
				
				return responseResult;
			}
		}
		
		if(StringUtils.isNotBlank(order.getWorksIds())) {
			String worksIds [] = order.getWorksIds().split(",");
			for(int index = 0; index < worksIds.length; index++) {
				String worksId = worksIds[index];
				
				if(StringUtils.isBlank(worksId)) {
					continue;
				}
				
				TrolleyDetail trolleyDetail = trolleyDetailList.get(index);
				
				OrderWorksRelation orderWorksRelation = new OrderWorksRelation();
				orderWorksRelation.setId(CommonUtils.UUIDGenerator());
				orderWorksRelation.setOrderId(orderId);
				orderWorksRelation.setWorksId(worksId);
				if(null != trolleyDetail) {
					orderWorksRelation.setWorksQuantity(trolleyDetail.getQuantity());
				}
				
				try {
					this.orderWorksRelationDao.insertOrderWorksRelation(orderWorksRelation);
				} catch (Exception e) {
					LOGGER.error("添加【订单与作品关系】 - 失败！", e);
					
					responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
					responseResult.setMessage("添加【订单与作品关系】 - 失败！");
					
					return responseResult;
				}
			}
		}
		
		Map<String, Object> orderMap = new HashMap<String, Object>();
		orderMap.put("id", orderId);
		orderMap.put("code", code);
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("创建【订单】 - 成功。");
		responseResult.setData(orderMap);
		
		return responseResult;
	}

	@SuppressWarnings("unused")
	@Override
	public ResponseResult createRapidOrder(String orderJSONObject) {
		LOGGER.info("orderJSONObject = {}", orderJSONObject);
		
		ResponseResult responseResult = new ResponseResult();
		
		if(StringUtils.isBlank(orderJSONObject)) {
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("无参数！");
			
			return responseResult;
		}
		
		JSONObject orderJSONObject_ = JSONObject.parseObject(orderJSONObject);
		JSONObject worksJSONObject = null;		// 作品
		JSONObject invoiceJSONObject = null;		// 发票
		String addressId = null;	// 收货地址ID
		
		if(!orderJSONObject_.containsKey("works")) {
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("无【作品】！");
			
			return responseResult;
		} 

		worksJSONObject = orderJSONObject_.getJSONObject("works");

		/*if(!orderJSONObject_.containsKey("invoice")) {
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("无【发票】！");
			
			return responseResult;
		}*/ 
		
		invoiceJSONObject = orderJSONObject_.getJSONObject("invoice");
		
		if(!orderJSONObject_.containsKey("addressId")) {
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("无【收货地址ID】！");
			
			return responseResult;
		}
		
		addressId = orderJSONObject_.getString("addressId");
		
		Works works = new Works();	// 作品
		String worksId = CommonUtils.UUIDGenerator(); 	// 作品ID
		works.setId(worksId);
		works.setStatus((byte) 2);
		
		Order order = new Order();
		String orderId = CommonUtils.UUIDGenerator();  	// 订单ID
		String code = CommonUtils.orderCodeGenerator(); 		// 订单编号
		order.setId(orderId);
		order.setCode(code);
		order.setStatus(OrderStatus.getValue(OrderStatus.CREATED));
		order.setAddressId(addressId);
		if(orderJSONObject_.containsKey("expressFee")) {
			order.setExpressFee(orderJSONObject_.getFloatValue("expressFee"));
		}
		
		String worksTypeCode = null;	// 作品类型编号
		String skuCode2 = null;		// 编辑确认后重新选择框型的 SKU
		
		if(worksJSONObject.containsKey("passportId")) {
			works.setPassportId(worksJSONObject.getIntValue("passportId"));
			
			order.setPassportId(worksJSONObject.getIntValue("passportId"));
		}
		
		if(worksJSONObject.containsKey("worksName")) {
			works.setName(worksJSONObject.getString("worksName"));
		}
		if(worksJSONObject.containsKey("worksSize")) {
			works.setSize(worksJSONObject.getString("worksSize"));
		}
		if(worksJSONObject.containsKey("worksQuantity")) {
			works.setQuantity(worksJSONObject.getIntValue("worksQuantity"));
			
			order.setTotalQuantity(worksJSONObject.getIntValue("worksQuantity"));
		}
		if(worksJSONObject.containsKey("worksTypeCode")) {
			works.setTypeCode(worksJSONObject.getString("worksTypeCode"));
		}
		if(worksJSONObject.containsKey("worksShape")) {
			works.setShape(worksJSONObject.getString("worksShape"));
		}
		if(worksJSONObject.containsKey("worksColor")) {
			works.setColor(worksJSONObject.getString("worksColor"));
		}
		if(worksJSONObject.containsKey("worksSize")) {
			works.setSize(worksJSONObject.getString("worksSize"));
		}
		if(worksJSONObject.containsKey("worksThumbnailId")) {
			works.setThumbnailId(worksJSONObject.getString("worksThumbnailId"));
		}
		if(!worksJSONObject.containsKey("worksChannelCode") || StringUtils.isBlank(worksJSONObject.getString("worksChannelCode"))) {
			works.setChannelCode("artron");

			order.setChannelCode("artron");
		} else {
			works.setChannelCode(worksJSONObject.getString("worksChannelCode"));

			order.setChannelCode(worksJSONObject.getString("worksChannelCode"));
		}
		if(!worksJSONObject.containsKey("worksClientCode") || StringUtils.isBlank(worksJSONObject.getString("worksClientCode"))) {
			works.setClientCode("ios");

			order.setClientCode("ios");
		} else {
			works.setClientCode(worksJSONObject.getString("worksClientCode"));

			order.setClientCode(worksJSONObject.getString("worksClientCode"));
		}
		if(worksJSONObject.containsKey("skuCode")) {
			works.setSkuCode(worksJSONObject.getString("skuCode"));
		}
		if(worksJSONObject.containsKey("skuCode2")) {
			skuCode2 = worksJSONObject.getString("skuCode2");
		}
		
		Float serverPrice = null;
		try {
			serverPrice = this.skuDao.selectPriceByCode(works.getSkuCode());
		} catch (Exception e) {
			LOGGER.error("根据【SKU编号】查询【单价】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("根据【SKU编号】查询【单价】 - 失败！");
			
			return responseResult;
		}
		
		if(null == serverPrice) {
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("作品 SKUCode 有误！");
			
			return responseResult;
		}

		works.setPrice(serverPrice);
		
		if(0 < works.getQuantity()) {
			order.setTotalPrice(serverPrice * works.getQuantity());
		}
		
		if(worksJSONObject.containsKey("materialEdit") && StringUtils.isNotBlank(worksJSONObject.getString("materialEdit"))) {
			JSONArray materialEditJSONArray = JSONArray.parseArray(worksJSONObject.getString("materialEdit"));
			
			if(null != materialEditJSONArray) {
				for(int cursor = 0; cursor < materialEditJSONArray.size(); cursor++) {
					JSONObject materialEditJSONObject = materialEditJSONArray.getJSONObject(cursor);
					
					if(null == materialEditJSONObject) {
						continue;
					}
					
					MaterialEdit materialEdit = new MaterialEdit();
					String worksEditId = CommonUtils.UUIDGenerator();
					materialEdit.setId(worksEditId);
					materialEdit.setWorksId(worksId);
					materialEdit.setStatus((byte) 1);
					
					if(materialEditJSONObject.containsKey("materialId")) {
						if(StringUtils.isNotBlank(materialEditJSONObject.getString("materialId"))) {
							materialEdit.setMaterialId(materialEditJSONObject.getString("materialId"));
						}
					}
					
					int x_cut = 0;	// X 起始坐标
					int y_cut = 0;	// Y 起始坐标
					int width_cut = 0;	// （裁切）宽度
					int height_cut = 0;	// （裁切）高度
					int rotate = 0;		// 旋转度数
					float scale = 1;	// 放大或缩小的倍数

					JSONObject actionJSONObject = new JSONObject();		// 操作步骤
					
					if(materialEditJSONObject.containsKey("materialIndex")) {
						if(0 < materialEditJSONObject.getInteger("materialIndex")) {
							materialEdit.setMaterialIndex(materialEditJSONObject.getInteger("materialIndex"));
						}
					}
					
					if(materialEditJSONObject.containsKey("x")) {
						x_cut = materialEditJSONObject.getIntValue("x");
						LOGGER.debug("x_cut = {}", x_cut);
						
						actionJSONObject.put("x", x_cut);
					}
					if(materialEditJSONObject.containsKey("y")) {
						y_cut = materialEditJSONObject.getIntValue("y");
						LOGGER.debug("y_cut = {}", y_cut);
						
						actionJSONObject.put("y", y_cut);
					}
					if(materialEditJSONObject.containsKey("width")) {
						width_cut = materialEditJSONObject.getIntValue("width");
						LOGGER.debug("width_cut = {}", width_cut);
						
						actionJSONObject.put("width", width_cut);
					}
					if(materialEditJSONObject.containsKey("height")) {
						height_cut = materialEditJSONObject.getIntValue("height");
						LOGGER.debug("height_cut = {}", height_cut);
						
						actionJSONObject.put("height", height_cut);
					}
					if(materialEditJSONObject.containsKey("rotate")) {	// 旋转
						rotate = materialEditJSONObject.getIntValue("rotate");

						actionJSONObject.put("rotate", rotate);
						LOGGER.debug("rotate = {}", rotate);
					}
					if(materialEditJSONObject.containsKey("scale")) {	// 放大/缩小
						scale = materialEditJSONObject.getFloat("scale");
						
						actionJSONObject.put("scale", scale);
						LOGGER.debug("scale = {}", scale);
					}
					
//					this.buildPdfImage(responseResult, materialEdit, works, x_cut, y_cut, width_cut, height_cut, rotate, scale);
//					String skuCode2 = "B01003";	// test2
//					skuCode2 = "B01005";	// test2
//					this.buildPdfImage2(responseResult, materialEdit, works, x_cut, y_cut, width_cut, height_cut, rotate, scale, skuCode2);
					
					if(StringUtils.isBlank(skuCode2)) {
						this.buildPdfImage(responseResult, materialEdit, works, x_cut, y_cut, width_cut, height_cut, rotate, scale);
					} else {
//						skuCode2 = "B01005";	// test
//						skuCode2 = "B01001";	// test
						
						this.buildPdfImage2(responseResult, materialEdit, works, x_cut, y_cut, width_cut, height_cut, rotate, scale, skuCode2);
					}
					
					if(null != actionJSONObject) {
						materialEdit.setAction(actionJSONObject.toJSONString());
					}
					
					try {
						this.materialEditDao.insertMaterialEdit(materialEdit);
					} catch (Exception e) {
						LOGGER.error("添加【素材编辑】 - 失败！", e);
						
						responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
						responseResult.setMessage("添加【素材编辑】 - 失败！");
						
						return responseResult;
					}
				}
			}
		}
		
		Invoice invoice = new Invoice();
		invoice.setPassportId(works.getPassportId());
		if(StringUtils.isNotBlank(works.getTypeCode())) {
			invoice.setContent(Constants.WORKS_TYPE_MAP.get(works.getTypeCode()));
		}
		if(invoiceJSONObject.containsKey("title") && StringUtils.isNotBlank(invoiceJSONObject.getString("title"))) {
			invoice.setTitle(invoiceJSONObject.getString("title"));

			try {
				this.invoiceDao.insertInvoice(invoice);
				
				order.setInvoiceId(invoice.getId());
			} catch (SQLException e) {
				LOGGER.error("添加【发票】 - 失败！", e);
				
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("添加【发票】 - 失败！");
				
				return responseResult;
			}
		}
		
		OrderWorksRelation orderWorksRelation = new OrderWorksRelation();
		orderWorksRelation.setId(CommonUtils.UUIDGenerator());
		orderWorksRelation.setOrderId(orderId);
		orderWorksRelation.setWorksId(worksId);
		orderWorksRelation.setWorksQuantity(works.getQuantity());
		
		try {
			this.worksDao.insertWorks(works);
		} catch (SQLException e) {
			LOGGER.error("添加【作品】 - 失败！");
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("添加【作品】 - 失败！");
		}
		
		try {
			this.orderDao.insertOrder(order);
		} catch (Exception e) {
			LOGGER.error("添加【订单】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("添加【订单】 - 失败！");
			
			return responseResult;
		}
		
		Payment payment = new Payment();
		payment.setId(CommonUtils.UUIDGenerator());
		payment.setOrderId(orderId);
		payment.setRemark("直接购买订单");
		payment.setStatus(PaymentStatus.getValue(PaymentStatus.UNPAID));
		
		try {
			this.paymentDao.insertPayment(payment);
		} catch (SQLException e) {
			LOGGER.error("添加【支付流水】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("添加【支付流水】 - 失败！");
			
			return responseResult;
		}
		
		try {
			this.orderWorksRelationDao.insertOrderWorksRelation(orderWorksRelation);
		} catch (Exception e) {
			LOGGER.error("添加【订单与作品关系】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("添加【订单与作品关系】 - 失败！");
			
			return responseResult;
		}
		
		Map<String, Object> orderMap = new HashMap<String, Object>();
		orderMap.put("id", orderId);
		orderMap.put("code", code);
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("创建【直接购买订单】 - 成功。");
		responseResult.setData(orderMap);
		
		return responseResult;
	}
	
	@Override
	public ResponseResult queryOrderList(Order order) {
		LOGGER.info("order = {}", JSONObject.toJSONString(order));
		
		ResponseResult responseResult = new ResponseResult();
		
		long totalCount = 0;
		try {
			totalCount = this.orderDao.selectOrderTotalCount(order);
		} catch (Exception e) {
			LOGGER.error("根据【通行证ID】查询【订单总记录数】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("根据【通行证ID】查询【订单总记录数】 - 失败！");
			
			return responseResult;
		}
		
		if(0 == totalCount) {
			responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
			responseResult.setMessage("无订单。");
			
			return responseResult;
		}

		Map<String, Object> resultMap = new HashMap<String, Object>();	// 结果集
		resultMap.put("totalCount", totalCount);
		
		if(null == order.getPageIndex() || 0 >= order.getPageIndex()) {
			order.setPageIndex(1);
		}
		if(null == order.getPageSize() || 0 >= order.getPageSize()) {
			order.setPageSize(Integer.valueOf(PropertiesUtils.getValue("pagination.pageSize")));
		}
		
		long offset = (order.getPageIndex() - 1) * order.getPageSize();	// 起始订单行号
		order.setOffset(offset);
		
		List<Order> orderList = null;
		try {
			orderList = this.orderDao.selectOrderList(order);
		} catch (SQLException e) {
			LOGGER.error("查询【订单列表】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("查询【订单列表】 - 失败！");
			
			return responseResult;
		}
		
		if(CommonUtils.isNotEmpty(orderList)) {
			List<Map<String, Object>> orderResultList = new ArrayList<Map<String, Object>>();
			String [] orderIds = new String[orderList.size()];
			StringBuilder addressIds = new StringBuilder();
			
			for(int index = 0; index < orderList.size(); index++) {
				Order order_ = orderList.get(index);
				
				if(null == order_) {
					continue;
				}
				
				orderIds[index] = order_.getId();
				
				if(StringUtils.isNotBlank(order_.getAddressId())) {
					addressIds.append(order_.getAddressId() + ",");
				}
			}
			
			List<Works> worksList = null;
			List<Address> addressList = null;
			List<Payment> paymentList = null;
			
			try {
				worksList = this.worksDao.selectWorksListByOrderIds(orderIds);
			} catch (SQLException e) {
				LOGGER.error("根据【订单IDs】查询【作品列表】 - 失败！", e);
				
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("根据【订单IDs】查询【作品列表】 - 失败！");
				
				return responseResult;
			}
			
			try {
				paymentList = this.paymentDao.selectPaymentListByOrderIds(orderIds);
			} catch (SQLException e) {
				LOGGER.error("根据【订单IDs】查询【支付流水】列表 - 失败！", e);
				
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("根据【订单IDs】查询【支付流水】列表 - 失败！");
				
				return responseResult;
			}
			
			if(null != addressIds && 0 < addressIds.length()){
				try {
					addressList = this.addressDao.selectAddressListByIds(addressIds.toString().split(","));
				} catch (SQLException e) {
					LOGGER.error("根据【IDs】查询【地址列表】 - 失败！", e);
					
					responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
					responseResult.setMessage("根据【IDs】查询【地址列表】 - 失败！");
					
					return responseResult;
				}
			}
			
			for(int index = 0; index < orderList.size(); index++) {
				Order order_ = orderList.get(index);
				
				if(null == order_) {
					continue;
				}
			
				Map<String, Object> orderMap = new HashMap<String, Object>();
				orderMap.put("orderId", order_.getId());
				orderMap.put("orderCode", order_.getCode());
				if(null != order_.getCreateTime()) {
					orderMap.put("createDate", CommonUtils.dateFormat(order_.getCreateTime()));
					String createTime = CommonUtils.dateTimeFormat(order_.getCreateTime());
					
					if(16 < createTime.length())
						orderMap.put("createTime", createTime.substring(0, 16));
					else
						orderMap.put("createTime", createTime);
				}
				orderMap.put("passportId", order_.getPassportId());
				orderMap.put("totalPrice", order_.getTotalPrice());
				orderMap.put("totalQuantity", order_.getTotalQuantity());
				orderMap.put("status", order_.getStatus());
				orderMap.put("statusName", OrderStatus.getOrderStatusName(order_.getStatus()));
				if(1 == order_.getCheckStatus()){
					orderMap.put("checkStatusName", "未审核");
				} else if(2 == order_.getCheckStatus()){
					orderMap.put("checkStatusName", "已通过");
				} else if(3 == order_.getCheckStatus()){
					orderMap.put("checkStatusName", "已拒绝");
				}
				
				if(CommonUtils.isNotEmpty(paymentList)) {
					for(Payment payment : paymentList){
						if(null == payment)
							continue;
						
						if(order_.getId().equals(payment.getOrderId())) {
							Map<String, Object> paymentMap = new HashMap<String, Object>();
							paymentMap.put("payId", payment.getId());
							paymentMap.put("payType", payment.getType());
							if(Constants.PAY_TYPE_WECHAT.equals(payment.getType())) {
								paymentMap.put("payTypeName", "微信");
							} else if(Constants.PAY_TYPE_ALIPAY.equals(payment.getType())) {
								paymentMap.put("payTypeName", "支付宝");
							}
							paymentMap.put("payStatus", payment.getStatus());
							if(1 == payment.getStatus()) {
								paymentMap.put("payStatusName", "待支付");
							} else if(2 == payment.getStatus()) {
								paymentMap.put("payStatusName", "已支付");
							} 
							paymentMap.put("payTotalPrice", payment.getStatus());
							paymentMap.put("paytTime", payment.getCreateTime());
							if(null != payment.getPayFinishTime()){
								paymentMap.put("payFinishTime", CommonUtils.dateFormat(payment.getPayFinishTime(), "yyyy-MM-dd HH:mm"));
							}
							
							orderMap.put("payment", paymentMap);
						}
					}
				}

				if(CommonUtils.isNotEmpty(worksList)) {
					StringBuilder worksTypeNames = new StringBuilder();
					List<Map<String, Object>> worksResultList = new ArrayList<Map<String, Object>>();
					
					for(int cursor = 0; cursor < worksList.size(); cursor++) {
						Works works = worksList.get(cursor);
						
						if(null == works) {
							continue;
						}
						
						if(order_.getId().equals(works.getOrderId())) {
							Map<String, Object> worksMap = new HashMap<String, Object>();
							worksMap.put("worksId", works.getId());
							worksMap.put("worksName", works.getName());
							worksMap.put("worksPrice", works.getPrice());
							worksMap.put("worksQuantity", works.getQuantity());
							worksMap.put("worksTypeCode", works.getTypeCode());
							worksTypeNames.append(Constants.WORKS_TYPE_MAP.get(works.getTypeCode()) + "/");
							worksMap.put("worksSize", works.getSize());
							worksMap.put("worksShape", works.getShape());
							worksMap.put("worksColor", works.getColor());
							
							if(StringUtils.isNotBlank(works.getThumbnailPath())) {
								String worksThumbnailPath = works.getThumbnailPath();
								
								int worksThumbnailIndex = worksThumbnailPath.indexOf("edited");
								if(0 < worksThumbnailIndex) {
									worksMap.put("thumbnailPath", PropertiesUtils.getValue("artup.mobile.url.perfix") + worksThumbnailPath.substring(worksThumbnailIndex, worksThumbnailPath.length()));
								}
							}

							worksResultList.add(worksMap);
						}
					}
					orderMap.put("worksList", worksResultList);
					
					if(0 < worksTypeNames.length() && worksTypeNames.toString().endsWith(("/"))) {
						worksTypeNames.setLength(worksTypeNames.length() - 1);
					}
					
					if(CommonUtils.isNotEmpty(addressList)){
						for(Address address : addressList){
							if(null == address)
								continue;
							
							if(order_.getAddressId().equals(address.getId())) {
								orderMap.put("address", address);
							}
						}
					}
					
					orderMap.put("worksTypeNames", worksTypeNames);
				}
				orderResultList.add(orderMap);
				//--如果状态“成功推送到MES”，需调用MES获取发货状态，并更新到本地Order表
				if(order_.getStatus()==OrderStatus.getValue(OrderStatus.PUSHED_MES)){
					executorTask.updateOrderDeliverStatusTask(order_.getCode());
				}
			}
			resultMap.put("orderList", orderResultList);
		}
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("查询【订单列表】 - 成功。");
		responseResult.setData(resultMap);
		
		return responseResult;
	}

	@Override
	public ResponseResult exportExcel(Order order) {
		LOGGER.info("order = {}", JSONObject.toJSONString(order));
		
		ResponseResult responseResult = new ResponseResult();
		
		order.setPageSize(999999);
		order.setOffset(0L);
		
		List<Order> orderList = null;
		try {
			orderList = this.orderDao.selectOrderList(order);
		} catch (SQLException e) {
			LOGGER.error("查询【订单列表】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("查询【订单列表】 - 失败！");
			
			return responseResult;
		}
		
		if(CommonUtils.isNotEmpty(orderList)) {
			List<Map<String, Object>> orderResultList = new ArrayList<Map<String, Object>>();
			String [] orderIds = new String[orderList.size()];
			StringBuilder addressIds = new StringBuilder();
			StringBuilder invoiceIds = new StringBuilder();
			
			for(int index = 0; index < orderList.size(); index++) {
				Order order_ = orderList.get(index);
				
				if(null == order_) {
					continue;
				}
				
				orderIds[index] = order_.getId();
				
				if(StringUtils.isNotBlank(order_.getAddressId())) {
					addressIds.append(order_.getAddressId() + ",");
				}
				if(null != order_.getInvoiceId()) {
					invoiceIds.append(order_.getInvoiceId() + ",");
				}
			}
			
			List<Works> worksList = null;
			List<Address> addressList = null;
			List<Payment> paymentList = null;
			List<Invoice> invoiceList = null;
			
			if(null != orderIds && 0 < orderIds.length){
				try {
					worksList = this.worksDao.selectWorksListByOrderIds(orderIds);
				} catch (SQLException e) {
					LOGGER.error("根据【订单IDs】查询【作品列表】 - 失败！", e);
					
					responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
					responseResult.setMessage("根据【订单IDs】查询【作品列表】 - 失败！");
					
					return responseResult;
				}
				
				try {
					paymentList = this.paymentDao.selectPaymentListByOrderIds(orderIds);
				} catch (SQLException e) {
					LOGGER.error("根据【订单IDs】查询【支付流水】列表 - 失败！", e);
					
					responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
					responseResult.setMessage("根据【订单IDs】查询【支付流水】列表 - 失败！");
					
					return responseResult;
				}
			}
			
			if(null != invoiceIds && 0 < invoiceIds.length()) {
				String [] _invoiceIds = invoiceIds.toString().split(",");
				
				long invoiceIds_ [] = new long[_invoiceIds.length];
				
				for(int index = 0; index < _invoiceIds.length; index++){
					String invoiceId = _invoiceIds[index];
					
					if(StringUtils.isBlank(invoiceId))
						continue;
					
					invoiceIds_[index] = Long.parseLong(invoiceId);
				}
				
				try {
					invoiceList = this.invoiceDao.selectInvoiceListByIds(invoiceIds_);
				} catch (SQLException e) {
					LOGGER.error("根据【IDs】查询【发票列表】 - 失败！", e);
					
					responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
					responseResult.setMessage("根据【IDs】查询【发票列表】 - 失败！");
					
					return responseResult;
				}
			}
			
			if(null != addressIds && 0 < addressIds.length()){
				try {
					addressList = this.addressDao.selectAddressListByIds(addressIds.toString().split(","));
				} catch (SQLException e) {
					LOGGER.error("根据【IDs】查询【地址列表】 - 失败！", e);
					
					responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
					responseResult.setMessage("根据【IDs】查询【地址列表】 - 失败！");
					
					return responseResult;
				}
			}
			
			for(int index = 0; index < orderList.size(); index++) {
				Order order_ = orderList.get(index);
				
				if(null == order_) {
					continue;
				}
				
				Map<String, Object> orderMap = new HashMap<String, Object>();
				orderMap.put("orderId", order_.getId());
				orderMap.put("orderCode", order_.getCode());
				orderMap.put("tradeCode", order_.getTransactionCode());
				orderMap.put("couponCode", order_.getCouponCode());
				orderMap.put("clientCode", order_.getClientCode());
				if(null != order_.getCreateTime()) {
					String createTime = CommonUtils.dateTimeFormat(order_.getCreateTime());
					
					if(16 < createTime.length())
						orderMap.put("createTime", createTime.substring(0, 16));
					else
						orderMap.put("createTime", createTime);
				}
				orderMap.put("passportId", order_.getPassportId());
				orderMap.put("totalPrice", order_.getTotalPrice());
				orderMap.put("totalQuantity", order_.getTotalQuantity());
				orderMap.put("statusName", OrderStatus.getOrderStatusName(order_.getStatus()));
				if(null != order_.getCheckTime()){
					orderMap.put("checkTime", CommonUtils.dateFormat(order_.getCheckTime(), "yyyy-MM-dd HH:mm"));
				}
				orderMap.put("status", order_.getStatus());
				orderMap.put("statusName", OrderStatus.getOrderStatusName(order_.getStatus()));
				if(1 == order_.getCheckStatus()){
					orderMap.put("checkStatusName", "未审核");
				} else if(2 == order_.getCheckStatus()){
					orderMap.put("checkStatusName", "已通过");
				} else if(3 == order_.getCheckStatus()){
					orderMap.put("checkStatusName", "已拒绝");
				}
				orderMap.put("totalDiscount", order_.getTotalDiscount());
				
				if(CommonUtils.isNotEmpty(paymentList)) {
					for(Payment payment : paymentList){
						if(null == payment)
							continue;
						
						if(order_.getId().equals(payment.getOrderId())) {
							Map<String, Object> paymentMap = new HashMap<String, Object>();
							paymentMap.put("payId", payment.getId());
							paymentMap.put("payType", payment.getType());
							if(Constants.PAY_TYPE_WECHAT.equals(payment.getType())) {
								paymentMap.put("payTypeName", "微信");
							} else if(Constants.PAY_TYPE_ALIPAY.equals(payment.getType())) {
								paymentMap.put("payTypeName", "支付宝");
							}
							paymentMap.put("payStatus", payment.getStatus());
							if(1 == payment.getStatus()) {
								paymentMap.put("payStatusName", "待支付");
							} else if(2 == payment.getStatus()) {
								paymentMap.put("payStatusName", "已支付");
							} 
							paymentMap.put("payTotalPrice", payment.getTotalPrice());
							paymentMap.put("paytTime", payment.getCreateTime());
							if(null != payment.getPayFinishTime()){
								paymentMap.put("payFinishTime", CommonUtils.dateFormat(payment.getPayFinishTime(), "yyyy-MM-dd HH:mm"));
							}
							
							orderMap.put("payment", paymentMap);
						}
					}
				}
				
				if(CommonUtils.isNotEmpty(worksList)) {
					StringBuilder worksTypeNames = new StringBuilder();
					StringBuilder worksSizes = new StringBuilder();
					StringBuilder worksQuantities = new StringBuilder();
					StringBuilder worksPrices = new StringBuilder();
					StringBuilder worksSkuCodes = new StringBuilder();
					StringBuilder worksShapes = new StringBuilder();
					StringBuilder worksColors = new StringBuilder();
					
					List<Map<String, Object>> worksResultList = new ArrayList<Map<String, Object>>();
					
					for(int cursor = 0; cursor < worksList.size(); cursor++) {
						Works works = worksList.get(cursor);
						
						if(null == works) {
							continue;
						}
						
						if(order_.getId().equals(works.getOrderId())) {
							Map<String, Object> worksMap = new HashMap<String, Object>();
							worksMap.put("worksId", works.getId());
							worksMap.put("worksName", works.getName());
							worksMap.put("worksPrice", works.getPrice());
							worksPrices.append(works.getPrice() + " / ");
							worksMap.put("worksQuantity", works.getQuantity());
							worksQuantities.append(works.getQuantity() + " / ");
							worksMap.put("worksTypeCode", works.getTypeCode());
							worksTypeNames.append(Constants.WORKS_TYPE_MAP.get(works.getTypeCode()) + " / ");
							worksMap.put("worksSize", works.getSize());
							worksSizes.append(works.getSize() + " / ");
							worksMap.put("worksShape", works.getShape());
							worksShapes.append(works.getShape() + " / ");
							worksMap.put("worksColor", works.getColor());
							worksColors.append(works.getColor() + " / ");
							worksMap.put("skuCode", works.getSkuCode());
							worksSkuCodes.append(works.getSkuCode() + " / ");
							
							worksResultList.add(worksMap);
						}
					}
					orderMap.put("worksList", worksResultList);
					
					if(0 < worksTypeNames.length() && worksTypeNames.toString().endsWith(("/ "))) {
						worksTypeNames.setLength(worksTypeNames.length() - 3);
					}
					if(0 < worksSizes.length() && worksSizes.toString().endsWith(("/ "))) {
						worksSizes.setLength(worksSizes.length() - 3);
					}
					if(0 < worksQuantities.length() && worksQuantities.toString().endsWith(("/ "))) {
						worksQuantities.setLength(worksQuantities.length() - 3);
					}
					if(0 < worksPrices.length() && worksPrices.toString().endsWith(("/ "))) {
						worksPrices.setLength(worksPrices.length() - 3);
					}
					if(0 < worksSkuCodes.length() && worksSkuCodes.toString().endsWith(("/ "))) {
						worksSkuCodes.setLength(worksSkuCodes.length() - 3);
					}
					if(0 < worksShapes.length() && worksShapes.toString().endsWith(("/ "))) {
						worksShapes.setLength(worksShapes.length() - 3);
					}
					if(0 < worksColors.length() && worksColors.toString().endsWith(("/ "))) {
						worksColors.setLength(worksColors.length() - 3);
					}
					
					orderMap.put("worksTypeNames", worksTypeNames);
					orderMap.put("worksSizes", worksSizes);
					orderMap.put("worksQuantities", worksQuantities);
					orderMap.put("worksPrices", worksPrices);
					orderMap.put("worksSkuCodes", worksSkuCodes);
					orderMap.put("worksShapes", worksShapes);
					orderMap.put("worksColors", worksColors);
				}
				
				if(CommonUtils.isNotEmpty(addressList)){
					for(Address address : addressList){
						if(null == address)
							continue;
						
						if(order_.getAddressId().equals(address.getId())) {
							orderMap.put("address", address);
						}
					}
				}
				
				if(CommonUtils.isNotEmpty(invoiceList)) {
					for(Invoice invoice : invoiceList){
						if(null == invoice)
							continue;
						
						if(order_.getInvoiceId().equals(invoice.getId()))
							orderMap.put("invoice", invoice);
					}
				}
				
				orderResultList.add(orderMap);
			}
			
			OrderExport orderExport = null; 
			try {
				orderExport = new OrderExport();
			} catch (Exception e) {
				LOGGER.error("创建 orderExport 失败！", e);
			}
			
			orderExport.excelGenerator(orderResultList, null);
		}
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("查询【订单列表】 - 成功。");
		
		return responseResult;
	}
	
	
	@Transactional
	public void updateOrderDeliverStatus(String orderCode){
		LOGGER.debug("run updateOrderDeliverStatusTask. orderCode={}",orderCode);  
		String url = PropertiesUtils.getValue("mes.url") + "/serve/getOrderStatus";
		Map<String, String > parameterMap = new HashMap<String, String>();
		parameterMap.put("orderId", orderCode);
		Map<String, Object> rmap = this.httpClientUtil.getMap(url, parameterMap);
		//判断rmap 
		if(rmap!=null&&rmap.get("code")!=null){
			if(rmap.get("code").equals("-1")){
				LOGGER.error("MES系统中未找到订单号{}！",orderCode);
			} 
			if(rmap.get("code").equals("0")){ //1：未发货，0:已发货(需要同步本地库)
				try{
					orderDao.updateOrderStatus(orderCode,OrderStatus.getValue(OrderStatus.DELIVERED));
				}catch (SQLException e) {
					LOGGER.error("根据【订单编号】更新【发货状态】 - 失败！", e);
				}
			}
		}else{
			LOGGER.error("url:{}接口返回数据错误",url);
		}
	}
	
	@Override
	public ResponseResult queryOrderById(String id) {
		LOGGER.info("id = {}", id);
		
		ResponseResult responseResult = new ResponseResult();
		
		if(StringUtils.isBlank(id)) {
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("【ID】不为空！");
			
			return responseResult;
		}
		
		Order order = null;
		try {
			order = this.orderDao.selectOrderById(id);
		} catch (SQLException e) {
			LOGGER.error("根据【ID】查询【订单详情】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("根据【ID】查询【订单详情】 - 失败！");
			
			return responseResult;
		}
		
		Map<String, Object> orderMap = new HashMap<String, Object>();
		
		if(null != order) {
			orderMap.put("orderId", order.getId());
			orderMap.put("orderCode", order.getCode());
			if(null != order.getCreateTime()) {
				orderMap.put("createDate", CommonUtils.dateTimeFormat(order.getCreateTime()));
				orderMap.put("createTime", CommonUtils.dateFormat(order.getCreateTime(), "yyyy-MM-dd HH:mm"));
			}
			orderMap.put("passportId", order.getPassportId());
			orderMap.put("totalPrice", order.getTotalPrice());
			orderMap.put("totalQuantity", order.getTotalQuantity());
			orderMap.put("status", order.getStatus());
			orderMap.put("statusName", OrderStatus.getOrderStatusName(order.getStatus()));
			
			List<Works> worksList = null;
			try {
				worksList = this.worksDao.selectWorksListByOrderId(order.getId());
			} catch (SQLException e) {
				LOGGER.error("根据【订单ID】查询【作品列表】 - 失败！", e);
				
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("根据【订单ID】查询【作品列表】 - 失败！");
				
				return responseResult;
			}
			
			if(CommonUtils.isNotEmpty(worksList)) {
				List<Map<String, Object>> worksResultList = new ArrayList<Map<String, Object>>();
				
				for(int cursor = 0; cursor < worksList.size(); cursor++) {
					Works works = worksList.get(cursor);
					
					if(null == works) {
						continue;
					}
					
					Map<String, Object> worksMap = new HashMap<String, Object>();
					worksMap.put("worksId", works.getId());
					worksMap.put("worksName", works.getName());
					worksMap.put("worksPrice", works.getPrice());
					worksMap.put("worksQuantity", works.getQuantity());
					worksMap.put("worksTypeCode", works.getTypeCode());
					worksMap.put("worksSize", works.getSize());
					worksMap.put("worksShape", works.getShape());
					worksMap.put("worksColor", works.getColor());
					
					if(StringUtils.isNotBlank(works.getThumbnailPath())) {
						String worksThumbnailPath = works.getThumbnailPath();
						
						int worksThumbnailIndex = worksThumbnailPath.indexOf("edited");
						if(0 < worksThumbnailIndex) {
							worksMap.put("thumbnailPath", PropertiesUtils.getValue("artup.mobile.url.perfix") + worksThumbnailPath.substring(worksThumbnailIndex, worksThumbnailPath.length()));
						}
					}

					worksResultList.add(worksMap);
				}
				
				orderMap.put("worksList", worksResultList);
			}
			
			Address address = null;
			try {
				address = this.addressDao.selectAddressById(order.getAddressId());
			} catch (SQLException e) {
				LOGGER.error("根据【ID】查询【地址】 - 失败！", e);
				
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("根据【ID】查询【地址】 - 失败！");
				
				return responseResult;
			}
			
			if(null != address) {
				Map<String, Object> addressMap = new HashMap<String, Object>();
				addressMap.put("name", address.getName());
				addressMap.put("mobileCode", address.getMobileCode());
				addressMap.put("detail", address.getDetail());
				
				String regions = jedis.get(Constants.CACHE_TYPE_REGION_LIST);
				
				if(CommonUtils.isNotEmpty(regions)) {
					StringBuilder detail = new StringBuilder();
					List<Region> regionList = JSONArray.parseArray(regions, Region.class);
					for(Region region : regionList) {
						if(address.getProvinceId() == region.getId()){
							detail.append(region.getName());
						}
					}
					for(Region region : regionList) {
						if(address.getCityId() == region.getId()){
							detail.append(region.getName());
						}
					}
					for(Region region : regionList) {
						if(address.getDistrictId() == region.getId()){
							detail.append(region.getName());
						}
					}
					
					detail.append(address.getDetail());
					addressMap.put("detail", detail);
				}
				
				orderMap.put("address", addressMap);
			}
			
			if(null != order && null != order.getInvoiceId() && 0 < order.getInvoiceId()) {
				Invoice invoice = null;
				try {
					invoice = this.invoiceDao.selectInvoiceById(order.getInvoiceId());
				} catch (SQLException e) {
					LOGGER.error("根据【ID】查询【发票详情】 - 失败！", e);
					
					responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
					responseResult.setMessage("根据【ID】查询【发票详情】 - 失败！");
					
					return responseResult;
				}
				
				if(null != invoice) {
					InvoiceVo invoiceVo = new InvoiceVo();
					invoiceVo.setId(invoice.getId());
					invoiceVo.setPassportId(invoice.getPassportId());
					invoiceVo.setTitle(invoice.getTitle());
					invoiceVo.setContent(invoice.getContent());
					
					orderMap.put("invoice", invoiceVo);
				}
			}
			
			if(StringUtils.isNotBlank(order.getId())) {
				Payment payment = null;
				try {
					payment = this.paymentDao.selectPaymentByOrderId(order.getId());
				} catch (SQLException e) {
					LOGGER.error(" 根据【订单ID】查询【支付流水】 - 失败！", e);
					
					responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
					responseResult.setMessage(" 根据【订单ID】查询【支付流水】 - 失败！");
					
					return responseResult;
				}
				
				if(null != payment) {
					PaymentVo paymentVo = new PaymentVo();
					paymentVo.setId(payment.getId());
					paymentVo.setOrderId(payment.getOrderId());
					if(StringUtils.isNotBlank(payment.getType())) {
						paymentVo.setType(payment.getType());
						
						if("wechat".equalsIgnoreCase(payment.getType())) {
							paymentVo.setTypeName("微信");
						} else if("alipay".equalsIgnoreCase(payment.getType())) {
							paymentVo.setTypeName("支付宝");
						}
					}
					paymentVo.setStatus(payment.getStatus());
					if(payment.getStatus() == PaymentStatus.getValue(PaymentStatus.UNPAID)) {
						paymentVo.setStatusName("未支付");
					} else if(payment.getStatus() == PaymentStatus.getValue(PaymentStatus.PAID)) {
						paymentVo.setStatusName("已支付");
					}
					paymentVo.setPayTime(CommonUtils.dateTimeFormat(payment.getPayTime()));
					paymentVo.setPayFinishDate(CommonUtils.dateTimeFormat(payment.getPayFinishTime()));
					paymentVo.setRemark(payment.getRemark());
					
					orderMap.put("payment", paymentVo);
				}
			}
		}
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("查询【订单详情】 - 成功。");
		responseResult.setData(orderMap);
		
		return responseResult;
	}
	
	@Override
	public ResponseResult queryTrolleyDetailListByPassportId(Integer passportId, Integer pageIndex, Integer pageSize) {
		LOGGER.info("passportId = {}, pageIndex = {}, pageSize = {}", passportId, pageIndex, pageSize);
		
		ResponseResult responseResult = new ResponseResult();
		
		if(null == passportId || 0 >= passportId) {
			responseResult.setStatus(Constants.ACTION_STATUS_PARAMETER_EMPTY);
			responseResult.setMessage("【通行证ID】不可为空且为正整数！");
			
			return responseResult;
		}
		
		if(null == pageIndex || 0 >= pageIndex) {
			pageIndex = 1;
		}
		if(null == pageSize || 0 >= pageSize) {
			pageSize = 10;
		}
		
		long totalCount = 0;
		try {
			totalCount = this.trolleyDetailDao.selectTrolleyDetailTotalCountByPassportId(passportId);
		} catch (Exception e) {
			LOGGER.error("根据【通行证ID】查询【购物车详情总记录数】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("根据【通行证ID】查询【购物车详情总记录数】 - 失败！");
			
			return responseResult;
		}
		
		if(0 == totalCount) {
			responseResult.setStatus(Constants.ACTION_STATUS_RESULT_EMPTY);
			responseResult.setMessage("购物车无作品！");
			
			return responseResult;
		}
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		int totalPageCount = (int) (0 < totalCount % pageSize ? totalCount / pageSize + 1 : totalCount / pageSize);
		
		if(pageIndex > totalPageCount - 1) {
			resultMap.put("isMore", false);
		} else {
			resultMap.put("isMore", true);
		}

		long offset = (pageIndex - 1) * pageSize;
		
		List<TrolleyDetail> trolleyDetailList = null;
		try {
			trolleyDetailList = this.trolleyDetailDao.selectTrolleyDetailListByPassportId(passportId, offset, pageSize);
		} catch (Exception e) {
			LOGGER.error("根据【通行证ID】查询【购物车详情列表】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("根据【通行证ID】查询【购物车详情列表】 - 失败！");
			
			return responseResult;
		}
		
		List<TrolleyDetailVo> trolleyDetailVoList = new ArrayList<TrolleyDetailVo>();
		
		if(CommonUtils.isNotEmpty(trolleyDetailList)) {
			for(TrolleyDetail trolleyDetail : trolleyDetailList) {
				if(null == trolleyDetail) {
					continue;
				}
				
				TrolleyDetailVo trolleyDetailVo = new TrolleyDetailVo();
				trolleyDetailVo.setId(trolleyDetail.getId());
				trolleyDetailVo.setTrolleyId(trolleyDetail.getTrolleyId());
				trolleyDetailVo.setWorksId(trolleyDetail.getWorksId());
				trolleyDetailVo.setWorksName(trolleyDetail.getWorksName());
				trolleyDetailVo.setWorksSize(trolleyDetail.getWorksSize());
				trolleyDetailVo.setWorksShape(trolleyDetail.getWorksShape());
				trolleyDetailVo.setWorksColor(trolleyDetail.getWorksColor());
				trolleyDetailVo.setWorksTypeCode(trolleyDetail.getWorksTypeCode());
				if(StringUtils.isNotBlank(trolleyDetail.getWorksThumbnailUrl())) {
					String worksThumbnailPath = trolleyDetail.getWorksThumbnailUrl();
					
					int worksThumbnailIndex = worksThumbnailPath.indexOf("edited");
					if(0 < worksThumbnailIndex) {
						trolleyDetailVo.setWorksThumbnailUrl(PropertiesUtils.getValue("artup.mobile.url.perfix") + worksThumbnailPath.substring(worksThumbnailIndex, worksThumbnailPath.length()));
					}
				}
				trolleyDetailVo.setPrice(trolleyDetail.getPrice());
				trolleyDetailVo.setQuantity(trolleyDetail.getQuantity());
				trolleyDetailVo.setSubtotal(trolleyDetail.getSubtotal());
				trolleyDetailVo.setDiscount(trolleyDetail.getDiscount());
				trolleyDetailVo.setDiscountSubtotal(trolleyDetail.getDiscountSubtotal());
				trolleyDetailVo.setSkuCode(trolleyDetail.getSkuCode());
				trolleyDetailVo.setCouponId(trolleyDetail.getCouponId());
				
				trolleyDetailVoList.add(trolleyDetailVo);
			}
		}
		
		resultMap.put("trolleyDetailList", trolleyDetailVoList);
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("查询【购物车详情列表】 - 成功。");
		responseResult.setData(resultMap);
		
		return responseResult;
	}
	
	public ResponseResult selectOrderQuantityByPassportId(Integer passportId){
		LOGGER.info("passportId={}",passportId);
		ResponseResult responseResult = new ResponseResult();
		if(null == passportId || 0 >= passportId) {
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("【通行证ID】不合法！");
			return responseResult;
		}
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try {
			resultMap.put("order_quantity",this.orderDao.selectOrderQuantityByPassportId(String.valueOf(passportId)));
		} catch (Exception e) {
			LOGGER.error("根据【passportId】获取【订单数量】 - 失败！", e);
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("获取订单数量 - 失败！");
			return responseResult;
		}
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("获取订单数量 - 成功。");
		responseResult.setData(resultMap);
		return responseResult;
	}
	
	@Override
	public ResponseResult cancelOrderByCode(String code) {
		LOGGER.info("code = {}", code);
		
		ResponseResult responseResult = new ResponseResult();
		
		if(StringUtils.isBlank(code)) {
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("【订单号】不为空！");
			
			return responseResult;
		}
		
		byte status = -1;
		try {
			status = this.orderDao.selectOrderStatusByCode(code);
		} catch (SQLException e) {
			LOGGER.error("根据【订单号】查询【订单状态】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("根据【订单号】查询【订单状态】 - 失败！");
			
			return responseResult;
		}
		
		if(OrderStatus.getValue(OrderStatus.CREATED) != status){
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("非待付款的订单，不可取消！");
			
			return responseResult;
		}
		
		try {
			this.orderDao.updateOrderStatus(code, OrderStatus.getValue(OrderStatus.CANCEL));
		} catch (SQLException e) {
			LOGGER.error("根据【订单号】更新【订单状态】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("根据【订单号】更新【订单状态】 - 失败！");
			
			return responseResult;
		}
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("取消订单 - 成功。");
		
		return responseResult;
	}

	@Override
	public ResponseResult recycleOrderByCode(String code) {
		LOGGER.info("code = {}", code);
		
		ResponseResult responseResult = new ResponseResult();
		
		if(StringUtils.isBlank(code)) {
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("【订单号】不为空！");
			
			return responseResult;
		}
		
		byte status = -1;
		try {
			status = this.orderDao.selectOrderStatusByCode(code);
		} catch (SQLException e) {
			LOGGER.error("根据【订单号】查询【订单状态】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("根据【订单号】查询【订单状态】 - 失败！");
			
			return responseResult;
		}
		
		if(OrderStatus.getValue(OrderStatus.CANCEL) != status){
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("非已取消的订单，不可删除！");
			
			return responseResult;
		}
		
		try {
			this.orderDao.updateOrderRecycleByCode(code, (byte) 1);
		} catch (SQLException e) {
			LOGGER.error("根据【订单号】更新【回收状态】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("根据【订单号】更新【回收状态】 - 失败！");
			
			return responseResult;
		}
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("删除订单 - 成功。");
		
		return responseResult;
	}

	/**
	 * 构建 PDF 图片
	 * @param responseResult
	 * @param materialEdit
	 * @param works 作品
	 * @param abscissa 横坐标
	 * @param ordinate 纵坐标
	 * @param width （裁切）宽度
	 * @param height （裁切）高度
	 * @param rotate 旋转角度
	 * @param scale 缩放比例
	 * @return
	 */
	private ResponseResult buildPdfImage(ResponseResult responseResult, MaterialEdit materialEdit, Works works, int abscissa, int ordinate, int width, int height, int rotate, float scale) {
		LOGGER.info("abscissa = {}, ordinate = {}, width = {}, height = {}, rotate = {}, scale = {}", abscissa, ordinate, width, height, rotate, scale);
		
		Material material = null;
		try {
			material = this.materialDao.selectMaterialById(materialEdit.getMaterialId());
		} catch (SQLException e) {
			LOGGER.error("根据【ID】查询【素材】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("根据【ID】查询【素材】 - 失败！");
			
			return responseResult;
		}
		
		if(null == material) {
			responseResult.setStatus(Constants.ACTION_STATUS_RESULT_EMPTY);
			responseResult.setMessage("未找到的【素材】！");
			
			return responseResult;
		}
		
		String orignalImagePath = material.getOrignalStoragePath();	// 原图路径
		
		if(StringUtils.isBlank(orignalImagePath)){
			responseResult.setStatus(Constants.ACTION_STATUS_RESULT_EMPTY);
			responseResult.setMessage("【素材无原图】！");
			
			return responseResult;
		}
		
		String rotatedImagePath = null;		// 旋转后的图片路径
		String scaledImagePath = null;		// 旋转后的图片路径
		String cutedImagePath = null;		// 裁切后的图片路径
		String worksImagePath = null;		// 作品图片路径
		String worksThumbnailImagePath = null;		// 作品缩略图图路径
		
		String expandedName = orignalImagePath.substring(orignalImagePath.lastIndexOf("."), orignalImagePath.length());
		
		String rotatedSystemFileName = CommonUtils.UUIDGenerator() + expandedName;	// 旋转后的系统文件名
		String scaledSystemFileName = CommonUtils.UUIDGenerator() + expandedName;	// 缩放后的系统文件名
		String cutedSystemFileName = CommonUtils.UUIDGenerator() + expandedName;	// 裁切后的系统文件名
		String worksSystemFileName = CommonUtils.UUIDGenerator() + expandedName;	// 适应框型后的作品的系统文件名
		String worksThumbnailImageSystemFileName = CommonUtils.UUIDGenerator() + "_200" + expandedName;	// 裁切后的作品缩略图的系统文件名
		
		String rotatedImagePrefix = PropertiesUtils.getValue("works.image.edited.rotated.path");
		String scaledImagePrefix = PropertiesUtils.getValue("works.image.edited.scaled.path");
		String cutedImagePrefix = PropertiesUtils.getValue("works.image.edited.cuted.path");
		String worksImagePrefix = PropertiesUtils.getValue("works.image.edited.final.path");
		
		rotatedImagePath = FolderUtils.getStoragePath(rotatedImagePrefix, works.getPassportId()) + rotatedSystemFileName;
		scaledImagePath = FolderUtils.getStoragePath(scaledImagePrefix, works.getPassportId()) + scaledSystemFileName;
		cutedImagePath = FolderUtils.getStoragePath(cutedImagePrefix, works.getPassportId()) + cutedSystemFileName;
		worksImagePath = FolderUtils.getStoragePath(worksImagePrefix, works.getPassportId()) + worksSystemFileName;
		worksThumbnailImagePath = FolderUtils.getStoragePath(worksImagePrefix, works.getPassportId()) + worksThumbnailImageSystemFileName;
		
		int A4PageWorksBoxWidth = 0;	// 缩放至 A4 页面作品框宽度
		int A4PageWorksBoxHeight = 0;	// 缩放至 A4 页面作品框高度
		
		if(0 == width && 0 == height){	// 默认编辑
			String pictureSize = null; 	// 图片尺寸（内框尺寸）
			try {
				pictureSize = this.skuDao.selectPictureSizeByCode(works.getSkuCode());
			} catch (Exception e) {
				LOGGER.error("根据【编号】查询【普通尺寸】 - 失败！", e);
				
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("根据【编号】查询【普通尺寸】 - 失败！");
				
				return responseResult;
			}
			
			if(StringUtils.isBlank(pictureSize)) {
				responseResult.setStatus(Constants.ACTION_STATUS_RESULT_EMPTY);
				responseResult.setMessage("未找到的【作品尺寸】！");
				
				return responseResult;
			}
			
			String worksSize_ [] = pictureSize.split("X");
			int worksSizeWidth = 0;	// 单位：像素
			int worksSizeHeight = 0;	// 单位：像素
			
			if(0 < worksSize_.length) {
				worksSizeWidth = Integer.parseInt(worksSize_[0]);
			}
			if(0 < worksSize_.length) {
				worksSizeHeight = Integer.parseInt(worksSize_[1]);
			}
			
			if(0 == worksSizeWidth || 0 == worksSizeHeight){
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("【作品尺寸】有误！");
				
				return responseResult;
			}

			// 由 毫米 转换为 像素
			worksSizeWidth = PdfUtil.millimeter2pixel_150ppi(worksSizeWidth);		
			worksSizeHeight = PdfUtil.millimeter2pixel_150ppi(worksSizeHeight);
			LOGGER.debug("worksSizeWidth = {}, worksSizeHeight = {}", worksSizeWidth, worksSizeHeight);
			
			int orginalImageWidth = ImageUtil.getImageWidth(orignalImagePath);	// 原图宽度
			int orginalImageHeight = ImageUtil.getImageHeight(orignalImagePath);	// 原图高度
			LOGGER.debug("orginalImageWidth = {}, orginalImageHeight = {}", orginalImageWidth, orginalImageHeight);
			
			int targetWidth = (int) ( (float) orginalImageWidth * (float) worksSizeHeight / (float) orginalImageHeight ); 	// 按高缩放
			int targetHeight = (int) ( (float) orginalImageHeight * (float) worksSizeWidth / (float) orginalImageWidth );	// 按宽缩放
			LOGGER.debug("targetWidth = {}, targetHeight = {}", targetWidth, targetHeight);
			
			LOGGER.debug("================== 第一步：缩放  ================");
			boolean isZoomSuccess = true;	// 是否缩放成功
			if(targetWidth >= worksSizeWidth) {	// 按框型的高度缩放
				abscissa = (targetWidth - worksSizeWidth) / 2;
				ordinate = 0;
				
				isZoomSuccess = ImageUtil.zoomImage(orignalImagePath, scaledImagePath, null, worksSizeHeight);
				
				if(!isZoomSuccess){
					responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
					responseResult.setMessage("默认缩放至框型的高度 - 失败！");
					
					return responseResult;
				}
			} else if(targetHeight >= worksSizeHeight) {	// 按框型的宽度缩放
				abscissa = 0;
				ordinate = (targetHeight - worksSizeHeight) / 2;
				
//				isZoomSuccess = ImageUtil.zoomImage(orignalImagePath, scaledImagePath, worksSizeWidth, null);
				isZoomSuccess = ImageUtil.zoomImage(orignalImagePath, scaledImagePath, worksSizeWidth, targetHeight);

				if(!isZoomSuccess){
					responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
					responseResult.setMessage("默认缩放至框型的宽度 - 失败！");
					
					return responseResult;
				}
			}
			
			// 第二步：裁切
			LOGGER.debug("================== 第二步：裁剪  ================");
			LOGGER.debug("以左上角为原点，从横坐标：{}，纵坐标：{} 开始裁切，裁切宽度为：{} 像素，裁切高度为：{} 像素。", abscissa, ordinate, worksSizeWidth, worksSizeHeight);
			boolean isCutSuccess = ImageUtil.cutImage(scaledImagePath, cutedImagePath, abscissa, ordinate, worksSizeWidth, worksSizeHeight);
			
			if(!isCutSuccess){
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("默认裁切图片 - 失败！");
				
				return responseResult;
			}
			
			A4PageWorksBoxWidth = (int) ( (float) worksSizeWidth * (float) WORKS_BOX_HEIGHT / (float) worksSizeHeight ); 	// 按高缩放
			A4PageWorksBoxHeight = (int) ( (float) worksSizeHeight * (float) WORKS_BOX_WIDTH / (float) worksSizeWidth );	// 按宽缩放
			LOGGER.debug("targetWidth = {}, targetHeight = {}", targetWidth, targetHeight);
			
			materialEdit.setWidth(worksSizeWidth);
			materialEdit.setHeight(worksSizeHeight);
		} else {	// 会员编辑
			LOGGER.debug("================== 第一步：旋转  ================");
			boolean isRotateSuccess = ImageUtil.rotate(orignalImagePath, rotatedImagePath, rotate);
			if(isRotateSuccess) {
				materialEdit.setEditedMaterialPath(rotatedImagePath);
				
				LOGGER.debug("以原图为基准，旋转 {} 度 - 成功。", rotate);
			}
			
			LOGGER.debug("================== 第二步：裁剪  ================");
			LOGGER.debug("以左上角为原点，从横坐标为：{}，纵坐标为：{} 开始裁切，裁切宽度为：{} 像素，裁切高度为：{} 像素。", abscissa, ordinate, (int) (width * scale), (int) (height * scale));
			boolean isCutSuccess = ImageUtil.cutImage(rotatedImagePath, cutedImagePath, (int) (abscissa * scale), (int) (ordinate * scale), (int) (width * scale), (int) (height * scale));
//			int offset = PdfUtil.millimeter2pixel_150ppi(50);
//			int offset2 = PdfUtil.millimeter2pixel_150ppi(100);
//			boolean isCutSuccess = ImageUtil.cutImage(rotatedImagePath, cutedImagePath, (int) (abscissa * scale - offset), (int) (ordinate * scale), (int) (width * scale + offset2), (int) (height * scale));
			
			if(!isCutSuccess){
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("按会员编辑裁切图片 - 失败！");
				
				return responseResult;
			}
			
			int cutedImageWidth = ImageUtil.getImageWidth(cutedImagePath);
			int cutedImageHeight = ImageUtil.getImageHeight(cutedImagePath);
			
			A4PageWorksBoxWidth = (int) ( (float) cutedImageWidth * (float) WORKS_BOX_HEIGHT / (float) cutedImageHeight ); 	// 按高缩放
			A4PageWorksBoxHeight = (int) ( (float) cutedImageHeight * (float) WORKS_BOX_WIDTH / (float) cutedImageWidth );	// 按宽缩放
			LOGGER.debug("targetWidth = {}, targetHeight = {}", A4PageWorksBoxWidth, A4PageWorksBoxHeight);
			
			materialEdit.setWidth(cutedImageWidth);
			materialEdit.setHeight(cutedImageHeight);
		}
		
		boolean isZoomSuccess = true;	// 是否缩放成功
		
		LOGGER.debug("================== 第三步：缩放以适应 A4 纸张  ================");
		if(A4PageWorksBoxWidth > WORKS_BOX_WIDTH) {	// 按 A4 纸张高度缩放
			LOGGER.debug("以固定宽度 {} 为基准缩放图片；缩放后的作品图片路径 {}", WORKS_BOX_WIDTH, worksImagePath);
			isZoomSuccess = ImageUtil.zoomImage(cutedImagePath, worksImagePath, WORKS_BOX_WIDTH, null);
			if(!isZoomSuccess) {
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("以宽为基准，制作符合 A4 纸张作品框尺寸的作品图 - 失败！");
				
				return responseResult;
			}

			cutedImagePath = worksImagePath;
			
			LOGGER.debug("================== 第四步：制作作品缩略图  ================");
			isZoomSuccess = ImageUtil.zoomImage(cutedImagePath, worksThumbnailImagePath, 200, null);	// 创建作品的缩略图
			if(!isZoomSuccess){
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("以宽为基准，制作作品缩略图 - 失败！");
				
				return responseResult;
			}
		} else if(A4PageWorksBoxHeight > WORKS_BOX_HEIGHT) {	// 按 A4 纸张宽度缩放
			LOGGER.debug("以固定高度 {} 为基准缩放图片；缩放后的作品图片路径 {}", WORKS_BOX_HEIGHT, worksImagePath);
			isZoomSuccess = ImageUtil.zoomImage(cutedImagePath, worksImagePath, null, WORKS_BOX_HEIGHT);
			if(!isZoomSuccess) {
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("以高为基准，制作符合 A4 纸张作品框尺寸的作品图 - 失败！");
				
				return responseResult;
			}
			
			cutedImagePath = worksImagePath;

			LOGGER.debug("================== 第四步：制作作品缩略图  ================");
			isZoomSuccess = ImageUtil.zoomImage(cutedImagePath, worksThumbnailImagePath, null, 200);	// 创建作品的缩略图
			if(!isZoomSuccess){
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("以高为基准，制作作品缩略图 - 失败！");
				
				return responseResult;
			}
		}

		LOGGER.debug("================== 第五步：更新“作品图片”访问权限  ================");
		if(OperationSystemInfo.isLinux()) {
			boolean isUpdateFileAuthoritySuccess = ChmodShell.updateFileAuthority(cutedImagePath, 775);
			if(isUpdateFileAuthoritySuccess) {
				materialEdit.setEditedMaterialPath(cutedImagePath);
			}
			
			isUpdateFileAuthoritySuccess = ChmodShell.updateFileAuthority(worksThumbnailImagePath, 775);
			if(isUpdateFileAuthoritySuccess){
				works.setThumbnailPath(worksThumbnailImagePath);
			}
		} else if(OperationSystemInfo.isWindows()) {
			materialEdit.setEditedMaterialPath(cutedImagePath);
			works.setThumbnailPath(worksThumbnailImagePath);
		}
		
		return responseResult;
	}

	private ResponseResult buildPdfImage2(ResponseResult responseResult, MaterialEdit materialEdit, Works works, int abscissa, int ordinate, int width, int height, int rotate, float scale, String skuCode2) {
		LOGGER.info("abscissa = {}, ordinate = {}, width = {}, height = {}, rotate = {}, scale = {}, skuCode2 = {}", abscissa, ordinate, width, height, rotate, scale, skuCode2);
		
		Material material = null;
		try {
			material = this.materialDao.selectMaterialById(materialEdit.getMaterialId());
		} catch (SQLException e) {
			LOGGER.error("根据【ID】查询【素材】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("根据【ID】查询【素材】 - 失败！");
			
			return responseResult;
		}
		
		if(null == material) {
			responseResult.setStatus(Constants.ACTION_STATUS_RESULT_EMPTY);
			responseResult.setMessage("未找到的【素材】！");
			
			return responseResult;
		}
		
		String orignalImagePath = material.getOrignalStoragePath();	// 原图路径
		
		if(StringUtils.isBlank(orignalImagePath)){
			responseResult.setStatus(Constants.ACTION_STATUS_RESULT_EMPTY);
			responseResult.setMessage("【素材无原图】！");
			
			return responseResult;
		}
		
		String rotatedImagePath = null;		// 旋转后的图片路径
		String scaledImagePath = null;		// 旋转后的图片路径
		String cutedImagePath = null;		// 裁切后的图片路径
		String worksImagePath = null;		// 作品图片路径
		String worksThumbnailImagePath = null;		// 作品缩略图图路径
		
		String expandedName = orignalImagePath.substring(orignalImagePath.lastIndexOf("."), orignalImagePath.length());
		
		String rotatedSystemFileName = CommonUtils.UUIDGenerator() + expandedName;	// 旋转后的系统文件名
		String scaledSystemFileName = CommonUtils.UUIDGenerator() + expandedName;	// 缩放后的系统文件名
		String cutedSystemFileName = CommonUtils.UUIDGenerator() + expandedName;	// 裁切后的系统文件名
		String worksSystemFileName = CommonUtils.UUIDGenerator() + expandedName;	// 适应框型后的作品的系统文件名
		String worksThumbnailImageSystemFileName = CommonUtils.UUIDGenerator() + "_200" + expandedName;	// 裁切后的作品缩略图的系统文件名
		
		String rotatedImagePrefix = PropertiesUtils.getValue("works.image.edited.rotated.path");
		String scaledImagePrefix = PropertiesUtils.getValue("works.image.edited.scaled.path");
		String cutedImagePrefix = PropertiesUtils.getValue("works.image.edited.cuted.path");
		String worksImagePrefix = PropertiesUtils.getValue("works.image.edited.final.path");
		
		rotatedImagePath = FolderUtils.getStoragePath(rotatedImagePrefix, works.getPassportId()) + rotatedSystemFileName;
		scaledImagePath = FolderUtils.getStoragePath(scaledImagePrefix, works.getPassportId()) + scaledSystemFileName;
		cutedImagePath = FolderUtils.getStoragePath(cutedImagePrefix, works.getPassportId()) + cutedSystemFileName;
		worksImagePath = FolderUtils.getStoragePath(worksImagePrefix, works.getPassportId()) + worksSystemFileName;
		worksThumbnailImagePath = FolderUtils.getStoragePath(worksImagePrefix, works.getPassportId()) + worksThumbnailImageSystemFileName;
		
		int A4PageWorksBoxWidth = 0;	// 缩放至 A4 页面作品框宽度
		int A4PageWorksBoxHeight = 0;	// 缩放至 A4 页面作品框高度
		
		String pictureSize = null; 	// 图片尺寸（内框尺寸）
		String pictureSize2 = null; 	// 图片尺寸（内框尺寸）
		
		try {
			pictureSize = this.skuDao.selectPictureSizeByCode(works.getSkuCode());
			pictureSize2 = this.skuDao.selectPictureSizeByCode(skuCode2);
		} catch (Exception e) {
			LOGGER.error("根据【编号】查询【普通尺寸】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("根据【编号】查询【普通尺寸】 - 失败！");
			
			return responseResult;
		}
		
		if(StringUtils.isBlank(pictureSize) || StringUtils.isBlank(pictureSize2)) {
			responseResult.setStatus(Constants.ACTION_STATUS_RESULT_EMPTY);
			responseResult.setMessage("未找到的【作品尺寸】！");
			
			return responseResult;
		}
		
		String worksSize_ [] = pictureSize.split("X");
		String worksSize2 [] = pictureSize2.split("X");
		int worksSizeWidth = 0;	// 单位：像素
		int worksSizeWidth2 = 0;	// 单位：像素
		int worksSizeHeight = 0;	// 单位：像素
		int worksSizeHeight2 = 0;	// 单位：像素
		
		if(0 < worksSize_.length) {
			worksSizeWidth = Integer.parseInt(worksSize_[0]);
		}
		if(0 < worksSize_.length) {
			worksSizeHeight = Integer.parseInt(worksSize_[1]);
		}
		if(0 < worksSize2.length) {
			worksSizeWidth2 = Integer.parseInt(worksSize2[0]);
		}
		if(0 < worksSize2.length) {
			worksSizeHeight2 = Integer.parseInt(worksSize2[1]);
		}
		
		if(0 == worksSizeWidth || 0 == worksSizeHeight || 0 == worksSizeWidth2 || 0 == worksSizeHeight2){
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("【作品尺寸】有误！");
			
			return responseResult;
		}
		
		if(0 == width && 0 == height){	// 默认编辑
			
			// 由 毫米 转换为 像素
			worksSizeWidth = PdfUtil.millimeter2pixel_150ppi(worksSizeWidth);		
			worksSizeHeight = PdfUtil.millimeter2pixel_150ppi(worksSizeHeight);
			LOGGER.debug("worksSizeWidth = {}, worksSizeHeight = {}", worksSizeWidth, worksSizeHeight);
			
			int orginalImageWidth = ImageUtil.getImageWidth(orignalImagePath);	// 原图宽度
			int orginalImageHeight = ImageUtil.getImageHeight(orignalImagePath);	// 原图高度
			LOGGER.debug("orginalImageWidth = {}, orginalImageHeight = {}", orginalImageWidth, orginalImageHeight);
			
			int targetWidth = (int) ( (float) orginalImageWidth * (float) worksSizeHeight / (float) orginalImageHeight ); 	// 按高缩放
			int targetHeight = (int) ( (float) orginalImageHeight * (float) worksSizeWidth / (float) orginalImageWidth );	// 按宽缩放
			LOGGER.debug("targetWidth = {}, targetHeight = {}", targetWidth, targetHeight);
			
			LOGGER.debug("================== 第一步：缩放  ================");
			boolean isZoomSuccess = true;	// 是否缩放成功
			if(targetWidth >= worksSizeWidth) {	// 按框型的高度缩放
				abscissa = (targetWidth - worksSizeWidth) / 2;
				ordinate = 0;
				
				isZoomSuccess = ImageUtil.zoomImage(orignalImagePath, scaledImagePath, null, worksSizeHeight);
				
				if(!isZoomSuccess){
					responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
					responseResult.setMessage("默认缩放至框型的高度 - 失败！");
					
					return responseResult;
				}
			} else if(targetHeight >= worksSizeHeight) {	// 按框型的宽度缩放
				abscissa = 0;
				ordinate = (targetHeight - worksSizeHeight) / 2;
				
				isZoomSuccess = ImageUtil.zoomImage(orignalImagePath, scaledImagePath, worksSizeWidth, targetHeight);	// 兼容 CentOS 和 Windows
				
				if(!isZoomSuccess){
					responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
					responseResult.setMessage("默认缩放至框型的宽度 - 失败！");
					
					return responseResult;
				}
			}
			
			// 第二步：裁切
			LOGGER.debug("================== 第二步：裁剪  ================");
			LOGGER.debug("以左上角为原点，从横坐标：{}，纵坐标：{} 开始裁切，裁切宽度为：{} 像素，裁切高度为：{} 像素。", abscissa, ordinate, worksSizeWidth, worksSizeHeight);
			boolean isCutSuccess = ImageUtil.cutImage(scaledImagePath, cutedImagePath, abscissa, ordinate, worksSizeWidth, worksSizeHeight);
			
			if(!isCutSuccess){
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("默认裁切图片 - 失败！");
				
				return responseResult;
			}
			
			A4PageWorksBoxWidth = (int) ( (float) worksSizeWidth * (float) WORKS_BOX_HEIGHT / (float) worksSizeHeight ); 	// 按高缩放
			A4PageWorksBoxHeight = (int) ( (float) worksSizeHeight * (float) WORKS_BOX_WIDTH / (float) worksSizeWidth );	// 按宽缩放
			LOGGER.debug("targetWidth = {}, targetHeight = {}", targetWidth, targetHeight);
			
			materialEdit.setWidth(worksSizeWidth);
			materialEdit.setHeight(worksSizeHeight);
		} else {	// 会员编辑
			LOGGER.debug("================== 第一步：旋转  ================");
			boolean isRotateSuccess = ImageUtil.rotate(orignalImagePath, rotatedImagePath, rotate);
			if(isRotateSuccess) {
				materialEdit.setEditedMaterialPath(rotatedImagePath);
				
				LOGGER.debug("以原图为基准，旋转 {} 度 - 成功。", rotate);
			}
			
			LOGGER.debug("================== 第二步：裁剪  ================");
			LOGGER.debug("以左上角为原点，从横坐标为：{}，纵坐标为：{} 开始裁切，裁切宽度为：{} 像素，裁切高度为：{} 像素。", abscissa, ordinate, (int) (width * scale), (int) (height * scale));
			
			int widthDifference = worksSizeWidth2 - worksSizeWidth;		// 不同 SKU 尺寸之间的差值，单位：像素
			int heightDifference = worksSizeHeight2 - worksSizeHeight;		// 不同 SKU 尺寸之间的差值，单位：像素
			
			if(0 > widthDifference){
				widthDifference = -PdfUtil.millimeter2pixel_150ppi(-widthDifference);
			} else {
				widthDifference = PdfUtil.millimeter2pixel_150ppi(widthDifference);
			}
			if(0 > heightDifference){
				heightDifference = -PdfUtil.millimeter2pixel_150ppi(-heightDifference);
			} else {
				heightDifference = PdfUtil.millimeter2pixel_150ppi(heightDifference);
			}
			
			abscissa = (int) (abscissa * scale - widthDifference / 2);		// 横坐标
			ordinate = (int) (ordinate * scale - heightDifference / 2);		// 纵坐标
			int cutWidth = (int) (width * scale + widthDifference);		// 裁切的宽度
			int cutHeight = (int) (height * scale + heightDifference);	// 裁切的高度
			
			boolean isCutSuccess = ImageUtil.cutImage(rotatedImagePath, cutedImagePath, abscissa, ordinate, cutWidth, cutHeight);
			
			if(!isCutSuccess){
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("按会员编辑裁切图片 - 失败！");
				
				return responseResult;
			}
			
			int cutedImageWidth = ImageUtil.getImageWidth(cutedImagePath);
			int cutedImageHeight = ImageUtil.getImageHeight(cutedImagePath);
			
			A4PageWorksBoxWidth = (int) ( (float) cutedImageWidth * (float) WORKS_BOX_HEIGHT / (float) cutedImageHeight ); 	// 按高缩放
			A4PageWorksBoxHeight = (int) ( (float) cutedImageHeight * (float) WORKS_BOX_WIDTH / (float) cutedImageWidth );	// 按宽缩放
			LOGGER.debug("targetWidth = {}, targetHeight = {}", A4PageWorksBoxWidth, A4PageWorksBoxHeight);
			
			materialEdit.setWidth(cutedImageWidth);
			materialEdit.setHeight(cutedImageHeight);
		}
		
		boolean isZoomSuccess = true;	// 是否缩放成功
		
		LOGGER.debug("================== 第三步：缩放以适应 A4 纸张  ================");
		if(A4PageWorksBoxWidth > WORKS_BOX_WIDTH) {	// 按 A4 纸张高度缩放
			LOGGER.debug("以固定宽度 {} 为基准缩放图片；缩放后的作品图片路径 {}", WORKS_BOX_WIDTH, worksImagePath);
			isZoomSuccess = ImageUtil.zoomImage(cutedImagePath, worksImagePath, WORKS_BOX_WIDTH, null);
			if(!isZoomSuccess) {
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("以宽为基准，制作符合 A4 纸张作品框尺寸的作品图 - 失败！");
				
				return responseResult;
			}
			
			cutedImagePath = worksImagePath;
			
			LOGGER.debug("================== 第四步：制作作品缩略图  ================");
			isZoomSuccess = ImageUtil.zoomImage(cutedImagePath, worksThumbnailImagePath, 200, null);	// 创建作品的缩略图
			if(!isZoomSuccess){
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("以宽为基准，制作作品缩略图 - 失败！");
				
				return responseResult;
			}
		} else if(A4PageWorksBoxHeight > WORKS_BOX_HEIGHT) {	// 按 A4 纸张宽度缩放
			LOGGER.debug("以固定高度 {} 为基准缩放图片；缩放后的作品图片路径 {}", WORKS_BOX_HEIGHT, worksImagePath);
			isZoomSuccess = ImageUtil.zoomImage(cutedImagePath, worksImagePath, null, WORKS_BOX_HEIGHT);
			if(!isZoomSuccess) {
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("以高为基准，制作符合 A4 纸张作品框尺寸的作品图 - 失败！");
				
				return responseResult;
			}
			
			cutedImagePath = worksImagePath;
			
			LOGGER.debug("================== 第四步：制作作品缩略图  ================");
			isZoomSuccess = ImageUtil.zoomImage(cutedImagePath, worksThumbnailImagePath, null, 200);	// 创建作品的缩略图
			if(!isZoomSuccess){
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("以高为基准，制作作品缩略图 - 失败！");
				
				return responseResult;
			}
		}
		
		LOGGER.debug("================== 第五步：更新“作品图片”访问权限  ================");
		if(OperationSystemInfo.isLinux()) {
			boolean isUpdateFileAuthoritySuccess = ChmodShell.updateFileAuthority(cutedImagePath, 775);
			if(isUpdateFileAuthoritySuccess) {
				materialEdit.setEditedMaterialPath(cutedImagePath);
			}
			
			isUpdateFileAuthoritySuccess = ChmodShell.updateFileAuthority(worksThumbnailImagePath, 775);
			if(isUpdateFileAuthoritySuccess){
				works.setThumbnailPath(worksThumbnailImagePath);
			}
		} else if(OperationSystemInfo.isWindows()) {
			materialEdit.setEditedMaterialPath(cutedImagePath);
			works.setThumbnailPath(worksThumbnailImagePath);
		}
		
		return responseResult;
	}
	
	@Override
	public ResponseResult updateOrderCheckStatusByCode(String code, Byte checkStatus) {
		LOGGER.info("code = {}, checkStatus = {}", code, checkStatus);
		
		ResponseResult responseResult = new ResponseResult();
		
		if(StringUtils.isEmpty(code)) {
			responseResult.setStatus(Constants.ACTION_STATUS_PARAMETER_EMPTY);
			responseResult.setMessage("【订单号】不可为空！");
			
			return responseResult;
		} else if(null == checkStatus){
			responseResult.setStatus(Constants.ACTION_STATUS_PARAMETER_EMPTY);
			responseResult.setMessage("【审核状态】不可为空！");
			
			return responseResult;
		}
		
		try {
			this.orderDao.updateOrderCheckStatusByCode(code, checkStatus);
		} catch (SQLException e) {
			LOGGER.error("根据【编号】更新【审核状态】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("根据【编号】更新【审核状态】 - 失败！");
			
			return responseResult;
		}
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("根据【编号】更新【审核状态】 - 成功。！");
		
		return responseResult;
	}

	@Override
	public ResponseResult updateOrderCheckStatusById(String id, Byte checkStatus) {
		LOGGER.info("id = {}, checkStatus = {}", id, checkStatus);
		
		ResponseResult responseResult = new ResponseResult();
		
		if(StringUtils.isEmpty(id)) {
			responseResult.setStatus(Constants.ACTION_STATUS_PARAMETER_EMPTY);
			responseResult.setMessage("【ID】不可为空！");
			
			return responseResult;
		} else if(null == checkStatus){
			responseResult.setStatus(Constants.ACTION_STATUS_PARAMETER_EMPTY);
			responseResult.setMessage("【审核状态】不可为空！");
			
			return responseResult;
		}
		
		try {
			this.orderDao.updateOrderCheckStatusById(id, checkStatus);
		} catch (SQLException e) {
			LOGGER.error("根据【ID】更新【审核状态】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("根据【ID】更新【审核状态】 - 失败！");
			
			return responseResult;
		}
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("审核成功");
		
		return responseResult;
	}
}
