package com.artup.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.artup.common.Constants;
import com.artup.common.ResponseResult;
import com.artup.dao.SkuDao;
import com.artup.pojo.Sku;
import com.artup.service.SkuService;

@Service( value = "skuService" )
public class SkuServiceImpl implements SkuService {
	protected static final Logger LOGGER = LoggerFactory.getLogger(SkuServiceImpl.class);
	
	@Autowired
	private SkuDao skuDao;

	@Override
	public ResponseResult querySkuList(Sku sku) {
		LOGGER.info("sku = {}", JSONObject.toJSONString(sku));
		
		ResponseResult responseResult = new ResponseResult();
		Map<String, Object> resultMap = new HashMap<String, Object>(); // 结果集
		
		long totalCount = 100;
		
		resultMap.put("totalCount", totalCount);
		
		List<Sku> skuList = null;
		try {
			skuList = this.skuDao.selectSkuList(sku);
		} catch (Exception e) {
			LOGGER.error("查询【SKU】列表 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("查询【SKU】列表 - 失败！");
			
			return responseResult;
		}
		
		resultMap.put("data", skuList);
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("查询【SKU】列表  - 成功。");
		responseResult.setData(resultMap);
		
		return responseResult;
	}

}
