package com.artup.service.impl;

import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.artup.common.Constants;
import com.artup.common.ResponseResult;
import com.artup.dao.SkuDao;
import com.artup.dao.TrolleyDetailDao;
import com.artup.pojo.TrolleyDetail;
import com.artup.service.TrolleyDetailService;
import com.artup.util.CommonUtils;

/**
 * 购物车详情
 * @author hapday
 * @date 2017年7月24日 @Time 下午5:23:11
 */
@Service( value = "trolleyDetailService" )
public class TrolleyDetailServiceImpl implements TrolleyDetailService {
	protected static final Logger LOGGER = LoggerFactory.getLogger(TrolleyDetailServiceImpl.class);
	
	@Autowired
	private TrolleyDetailDao trolleyDetailDao;
	
	@Autowired
	private SkuDao skuDao;

	@Override
	public ResponseResult batchUpdateTrolleyDetailStatusByIds(Integer passportId, String ids) {
		LOGGER.info("passportId = {}, ids = {}", passportId, ids);
		
		ResponseResult responseResult = new ResponseResult();
		
		if(null == passportId || 0 >= passportId){
			responseResult.setStatus(Constants.ACTION_STATUS_PARAMETER_EMPTY);
			responseResult.setMessage("【通行证ID】不合法！");
			
			return responseResult;
		}
		if(StringUtils.isBlank(ids)){
			responseResult.setStatus(Constants.ACTION_STATUS_PARAMETER_EMPTY);
			responseResult.setMessage("【IDs】不可为空！");
			
			return responseResult;
		}
		
		try {
			this.trolleyDetailDao.batchUpdateTrolleyDetailStatusByIds(ids.split(","), (byte) 2);
		} catch (Exception e) {
			LOGGER.error("根据【IDs】更新【购物车详情】的【状态】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_PARAMETER_EMPTY);
			responseResult.setMessage("根据【IDs】更新【购物车详情】的【状态】 - 失败！");
			
			return responseResult;
		}
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("批量从【购物车】中删除【作品】 - 成功。");
		
		return responseResult;
	}

	@Override
	public ResponseResult saveTrolley(TrolleyDetail trolleyDetail) {
		LOGGER.info("trolleyDetail = {}", JSONObject.toJSONString(trolleyDetail));
		
		ResponseResult responseResult = new ResponseResult();
		
		if(null == trolleyDetail) {
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("无参数！");
			
			return responseResult;
		} else if(StringUtils.isBlank(trolleyDetail.getWorksId())) {
			responseResult.setStatus(Constants.ACTION_STATUS_PARAMETER_EMPTY);
			responseResult.setMessage("【作品ID】不可为空！");
			
			return responseResult;
		}
		
		String trolleyDetailId = null;
		TrolleyDetail trolleyDetail_ = null;
		try {
			trolleyDetail_ = this.trolleyDetailDao.selectTrolleyDetailIdByWorksId(trolleyDetail.getWorksId(), trolleyDetail.getPassportId());
		} catch (SQLException e) {
			LOGGER.error("根据【作品ID】和【通行证ID】查询【购物车详情】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_PARAMETER_EMPTY);
			responseResult.setMessage("根据【作品ID】和【通行证】查询【购物车详情ID】 - 失败！");
			
			return responseResult;
		}
		
		if(StringUtils.isBlank(trolleyDetail.getChannelCode())) {
			trolleyDetail.setChannelCode("artron");
		}
		if(StringUtils.isBlank(trolleyDetail.getClientCode())) {
			trolleyDetail.setClientCode("ios");
		}
		
		Float serverPrice = null;
		try {
			serverPrice = this.skuDao.selectPriceByCode(trolleyDetail.getSkuCode());
		} catch (Exception e) {
			LOGGER.error("根据【SKU 编号】查询【单价】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("根据【SKU 编号】查询【单价】 - 失败！");
			
			return responseResult;
		}
		
		if(null != trolleyDetail_) {
			trolleyDetailId = trolleyDetail_.getId();
		}
		
		if(StringUtils.isNotBlank(trolleyDetailId)) {
			trolleyDetail.setId(trolleyDetail_.getId());
			trolleyDetail.setQuantity(trolleyDetail_.getQuantity() + 1);
			
			if(null != serverPrice) {
				trolleyDetail.setSubtotal(trolleyDetail_.getSubtotal() + serverPrice);
			}
			
			try {
				this.trolleyDetailDao.updateTrolleyDetailById(trolleyDetail);
			} catch (SQLException e) {
				LOGGER.error("根据【ID】更新【购物车详情】 - 失败！", e);
				
				responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
				responseResult.setMessage("根据【ID】更新【购物车详情】 - 失败！");
				
				return responseResult;
			}
		} else {
			trolleyDetailId = CommonUtils.UUIDGenerator(); 	// 购物车明细ID
			trolleyDetail.setId(trolleyDetailId);	
			
			if(null != serverPrice) {
				trolleyDetail.setPrice(serverPrice);
				
				if(0 < trolleyDetail.getQuantity()) {
					trolleyDetail.setSubtotal(serverPrice * trolleyDetail.getQuantity());
				}
			}
			
			try {
				this.trolleyDetailDao.insertTrolleyDetail(trolleyDetail);
			} catch (Exception e) {
				LOGGER.error("添加【购物车详情】 - 失败！", e);
				
				responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
				responseResult.setMessage("添加【购物车详情】 - 失败！");
				
				return responseResult;
			}
		}
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("添加【购物车】 - 成功。");
		responseResult.setData(trolleyDetailId);
		
		return responseResult;
	}
	
	@Override
	public ResponseResult updateTrolleyDetailQuantityById(String id, int quantity) {
		LOGGER.info("id = {}", id);
		
		ResponseResult responseResult = new ResponseResult();
		
		if(StringUtils.isBlank(id)) {
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("【ID】不可为空！");
			
			return responseResult;
		} 
		
		try {
			this.trolleyDetailDao.updateTrolleyDetailQuantityById(id, quantity);
		} catch (SQLException e) {
			LOGGER.error("根据【ID】更新【购物车详情】中【作品数量】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("根据【ID】更新【购物车详情】中【作品数量】 - 失败！");
			
			return responseResult;
		}
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("更新【购物车详情】中【作品数量】 - 成功。");
		
		return responseResult;
	}
}
