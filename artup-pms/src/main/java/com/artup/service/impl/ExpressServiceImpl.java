package com.artup.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.artup.common.Constants;
import com.artup.common.ResponseResult;
import com.artup.pojo.Express;
import com.artup.service.ExpressService;
import com.artup.util.file.PropertiesUtils;
import com.artup.util.http.HttpClientUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 物流
 * @author hapday
 * @date 2017年8月2日 @Time 上午11:05:02
 */
@Slf4j
@Service( value = "expressService" )
public class ExpressServiceImpl implements ExpressService {
	
	@Autowired
	private HttpClientUtil httpClientUtil;

	@Override
	public ResponseResult queryExpress(String orderCode) {
		log.info("orderCode = {}", orderCode);
		ResponseResult responseResult = new ResponseResult();
		
		String url = PropertiesUtils.getValue("mes.url") + "admin/orderitem/queryOrderLogisticses";
		Map<String, String > parameterMap = new HashMap<String, String>();
		parameterMap.put("orderCode", orderCode);
		
		Express express = this.httpClientUtil.get(url, parameterMap, Express.class);
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("查询【物流列表】 - 成功。");
		responseResult.setData(express);
    	
		return responseResult;
	}

}
