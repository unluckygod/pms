package com.artup.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.artup.common.Constants;
import com.artup.common.ResponseResult;
import com.artup.dao.ResourceDao;
import com.artup.dao.RoleResourceDao;
import com.artup.pojo.Resource;
import com.artup.pojo.RoleResource;
import com.artup.service.ResourceService;
import com.artup.util.CommonUtils;
import com.artup.util.file.PropertiesUtils;

@Service( value = "resourceService" )
@Transactional( readOnly = false )
public class ResourceServiceImpl implements ResourceService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ResourceServiceImpl.class);
	
	@Autowired
	private ResourceDao resourceDao;

	@Autowired
	private RoleResourceDao roleResourceDao;

	@Override
	public ResponseResult queryResourceById(long id) {
		LOGGER.info("id = {}", id);
		ResponseResult responseResult = new ResponseResult();
		
		Resource resource = null;
		try {
			resource = this.resourceDao.selectResourceById(id);

			responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
			responseResult.setMessage("根据【ID】查询【资源】  - 成功。");
			responseResult.setData(resource);
		} catch (Exception e) {
			LOGGER.error("根据【ID】查询【资源】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
			responseResult.setMessage("根据【ID】查询【资源】  - 成功。");
		}

		return responseResult;
	}

	@Override
	public ResponseResult queryResourceList(Resource resource) {
		LOGGER.info("resource = {}", JSONObject.toJSONString(resource));
		
		// 定义返回的结果集
		ResponseResult responseResult = new ResponseResult();
		
		// 设置初始条件
		if (null != resource) {
			if (CommonUtils.isNotEmpty(resource.getBeginCreateTime())) {
				resource.setBeginCreateTime(resource.getBeginCreateTime()
						+ " 00:00:00");
			}
			if (CommonUtils.isNotEmpty(resource.getEndCreateTime())) {
				resource.setEndCreateTime(resource.getEndCreateTime()
						+ " 23:59:59");
			}
		}

		// 模糊查询【资源】总记录数
		long totalCount = 0;
		try {
			totalCount = this.resourceDao.selectResourceTotalCount(resource);
		} catch (Exception e) {
			LOGGER.error("模糊查询【资源】总记录数 - 失败！", e);
		}
		
		if(0 == totalCount) {
			responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
			responseResult.setMessage("未找到【资源】。");
			
			return responseResult;
		}

		Map<String, Object> resultMap = new HashMap<String, Object>(); // 结果集
		resultMap.put("totalCount", totalCount);
		
		if(null == resource.getPageIndex() || 0 >= resource.getPageIndex()) {
			resource.setPageIndex(1);
		}
		if(null == resource.getPageSize() || 0 >= resource.getPageSize()) {
			resource.setPageSize(Integer.valueOf(PropertiesUtils.getValue("pagination.pageSize")));
		}
		
		resource.setOffset( (long) ( (resource.getPageIndex() - 1) * resource.getPageSize() ) ); // 设置分页的偏移量，即起始记录数

		// 4.模糊查询【资源】列表
		List<Resource> resourceList = null;
		try {
			resourceList = this.resourceDao.selectResourceList(resource);
		} catch (Exception e) {
			LOGGER.error("模糊查询【资源】列表 - 失败！", e);
		}

		resultMap.put("resourceList", resourceList);// 返回的总数据

		// 5.设置返回的结果集
		responseResult.setData(resultMap);
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("模糊查询【资源】列表 - 成功。");

		// 6.返回结果集
		return responseResult;
	}

	@Override
	public ResponseResult queryAllResourceList() {
		ResponseResult responseResult = new ResponseResult();
		
		List<Resource> resourceList = null;
		try {
			resourceList = this.resourceDao.selectAllResourceList();
		} catch (Exception e) {
			LOGGER.error("查询全部的【资源】列表 - 失败！", e);
		}
		
		responseResult.setData(resourceList);
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("查询全部的【资源】列表 - 成功。");
		
		return responseResult;
	}

	@Override
	public ResponseResult saveResource(Resource resource) {
		LOGGER.info("resource = " + JSONObject.toJSONString(resource));
		
		ResponseResult responseResult = new ResponseResult();
		
		if(null != resource) {
			try {
				this.resourceDao.insertResource(resource);
			} catch (Exception e) {
				LOGGER.error("保存【资源】 - 失败！", e);
				
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("保存【资源】 - 失败！");
			}
			
			this.saveRoleResource(resource.getId(), responseResult);
		}
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("保存【资源】 - 成功。");
		return responseResult;
	}
	
	private ResponseResult saveRoleResource( long resourceId, ResponseResult responseResult ) {
		RoleResource roleResource = new RoleResource();
		roleResource.setRoleId(1L);
		roleResource.setResourceId(resourceId);
		roleResource.setIsValid((byte) 1);
		roleResource.setIsRecycle((byte) 2);
		roleResource.setRemark("自动创建，不可变更。");
		roleResource.setVersion(1);
		roleResource.setCreatorId(1L);
		roleResource.setModifierId(1L);
		
		try {
			this.roleResourceDao.insertRoleResource(roleResource);
		} catch (Exception e) {
			LOGGER.error("保存【角色资源】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("保存【角色资源】 - 失败！");
		}
		
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("保存【资源】 - 成功。");
		return responseResult;
	}
	
	@Override
	public ResponseResult updateResourceById(Resource resource) {
		LOGGER.info("resource = " + JSONObject.toJSONString(resource));
		ResponseResult responseResult = new ResponseResult();
		
		if(null != resource) {
			try {
				this.resourceDao.updateResourceById(resource);
				
				responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
				responseResult.setMessage("更新【资源】 - 成功。");
			} catch (Exception e) {
				LOGGER.error("根据【ID】更新【资源】 - 失败！", e);
				
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("更新【资源】 - 失败！");
			}
		}
		
		return responseResult;
	}

	@Override
	public ResponseResult deleteResourceById(long id) {
		LOGGER.info("id = " + id);
		ResponseResult responseResult = new ResponseResult();
		
		try {
			this.resourceDao.deleteResourceById(id);
			
			responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
			responseResult.setMessage("删除【资源】 - 成功。");
		} catch (Exception e) {
			LOGGER.error("根据【ID】删除【资源】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("删除【资源】 - 失败！");
		}
		
		return responseResult;
	}
}