package com.artup.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.artup.common.Constants;
import com.artup.common.ResponseResult;
import com.artup.dao.RoleDao;
import com.artup.pojo.Role;
import com.artup.service.RoleService;
import com.artup.util.file.PropertiesUtils;

@Service( value = "roleService" )
@Transactional( readOnly = false )
public class RoleServiceImpl implements RoleService {
	private static final Logger LOGGER = LoggerFactory.getLogger(RoleServiceImpl.class);
	
	@Autowired
	private RoleDao roleDao;

	@Override
	public ResponseResult queryRoleById(long id) {
		LOGGER.info("id = " + id);
		ResponseResult responseResult = new ResponseResult();
		
		Role role = null;
		try {
			role = this.roleDao.selectRoleById(id);

			responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
			responseResult.setMessage("根据【ID】查询【角色】  - 成功。");
			responseResult.setData(role);
		} catch (Exception e) {
			LOGGER.error("根据【ID】查询【角色】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
			responseResult.setMessage("根据【ID】查询【角色】  - 成功。");
		}

		return responseResult;
	}

	/*@Override
	public ResponseResult queryRoleList(Role role) {
		ResponseResult responseResult = new ResponseResult();
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			LOGGER.info("role = " + mapper.writeValueAsString(role));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		List<Role> roleList = null;
		try {
			roleList = this.roleDao.selectRoleList(role);
			
			responseResult.setStatus(1);
			responseResult.setMessage("模糊查询【角色列表】 - 成功。");
			responseResult.setData(roleList);
		} catch (Exception e) {
			LOGGER.error("模糊查询【角色列表】 - 失败！", e);
			
			responseResult.setStatus(2);
			responseResult.setMessage("模糊查询【角色列表】 - 失败！");
		}
		
		return responseResult;
	}*/

	@Override
	public ResponseResult queryRoleList(Role role) {
		LOGGER.info("role = {}", JSONArray.toJSONString(role));
		
		// 1.定义返回的结果集
		ResponseResult responseResult = new ResponseResult();

		// 3.模糊查询【角色】总记录数
		long totalCount = 0;
		try {
			totalCount = this.roleDao.selectRoleTotalCount(role);
		} catch (Exception e) {
			LOGGER.error("模糊查询【角色】总记录数 - 失败！", e);
		}
		
		if(0 == totalCount) {
			responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
			responseResult.setMessage("无角色！");
			
			return responseResult;
		}

		Map<String, Object> resultMap = new HashMap<String, Object>();	// 结果集
		resultMap.put("totalCount", totalCount);
		
		if(null == role.getPageIndex() || 0 >= role.getPageIndex()) {
			role.setPageIndex(1);
		}
		if(null == role.getPageSize() || 0 >= role.getPageSize()) {
			role.setPageSize(Integer.valueOf(PropertiesUtils.getValue("pagination.pageSize")));
		}
		
		long offset = (role.getPageIndex() - 1) * role.getPageSize();
		role.setOffset(offset);		 // 设置分页的偏移量，即起始记录数

		// 模糊查询【角色】列表
		List<Role> roleList = null;
		try {
			roleList = this.roleDao.selectRoleList(role);
		} catch (Exception e) {
			LOGGER.error("模糊查询【角色】列表 - 失败！", e);
		}

		resultMap.put("roleList", roleList);// 返回的总数据

		// 设置返回的结果集
		responseResult.setData(resultMap);
		responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
		responseResult.setMessage("模糊查询【角色】列表 - 成功。");

		// 返回结果集
		return responseResult;
	}

	@Override
	public ResponseResult saveRole(Role role) {
		LOGGER.info("role = " + JSONObject.toJSONString(role));
		ResponseResult responseResult = new ResponseResult();
		
		if(null != role) {
			try {
				this.roleDao.insertRole(role);
				
				responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
				responseResult.setMessage("保存【角色】 - 成功。");
			} catch (Exception e) {
				LOGGER.error("保存【角色】 - 失败！", e);
				
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("保存【角色】 - 失败！");
			}
		}
		
		return responseResult;
	}
	
	@Override
	public ResponseResult updateRoleById(Role role) {
		LOGGER.info("role = " + JSONObject.toJSONString(role));
		ResponseResult responseResult = new ResponseResult();
		
		if(null != role) {
			try {
				this.roleDao.updateRoleById(role);
				
				responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
				responseResult.setMessage("更新【角色】 - 成功。");
			} catch (Exception e) {
				LOGGER.error("根据【ID】更新【角色】 - 失败！", e);
				
				responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
				responseResult.setMessage("更新【角色】 - 失败！");
			}
		}
		
		return responseResult;
	}

	@Override
	public ResponseResult deleteRoleById(long id) {
		LOGGER.info("id = " + id);
		ResponseResult responseResult = new ResponseResult();
		
		try {
			this.roleDao.deleteRoleById(id);
			
			responseResult.setStatus(Constants.ACTION_STATUS_SUCCESS);
			responseResult.setMessage("删除【角色】 - 成功。");
		} catch (Exception e) {
			LOGGER.error("根据【ID】删除【角色】 - 失败！", e);
			
			responseResult.setStatus(Constants.ACTION_STATUS_FAILURE);
			responseResult.setMessage("删除【角色】 - 失败！");
		}
		
		return responseResult;
	}
}