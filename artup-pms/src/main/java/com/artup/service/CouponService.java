package com.artup.service;

import com.artup.common.ResponseResult;

/**
 * 
 * @author hapday
 * @date 2017年8月14日 @Time 下午2:15:52
 */
public interface CouponService {
	/**
	 * 取消使用券
	 * @param couponCodes 券编号s
	 * @param passportId 通行证ID
	 * @return
	 */
	ResponseResult cancelUseCoupon(String couponCodes, int passportId);
}
