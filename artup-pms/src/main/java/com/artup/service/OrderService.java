package com.artup.service;

import com.artup.common.ResponseResult;
import com.artup.pojo.Order;

/**
 * 订单
 * @author hapday
 * @date 2017年7月18日 @Time 上午10:03:20
 */
public interface OrderService {
	
	/**
	 * 保存【购物车】
	 * @param trolleyJSONObject
	 * @return
	 */
	ResponseResult saveTrolley(String worksJSONObject) ;


	/**
	 * 修改【购物车数量】
	 * @param trolleyJSONObject
	 * @return
	 */
	ResponseResult updateTrolleyNum(Integer passportId,String edtId,Integer num);
	
	
	/**
	 * 根据【通行证ID】查询【购物车详情列表】
	 * @param passportId 【通行证ID】
	 * @return 【购物车详情列表】
	 */
	ResponseResult queryTrolleyDetailListByPassportId(Integer passportId, Integer pageIndex, Integer pageSize) ;

	/**
	 * 创建（普通）订单
	 * @param order 订单
	 * @return
	 */
	ResponseResult createOrder(Order order) ;

	/**
	 * 创建直接购买订单
	 * @param orderJSONObject
	 * @return
	 */
	ResponseResult createRapidOrder(String orderJSONObject) ;

	/**
	 * 查询【订单列表】
	 * @param passportId 通行证ID
	 * @return 【订单列表】
	 */
	ResponseResult queryOrderList(Order order) ;

	/**
	 * 导出【订单】到【Excel】
	 * @param order 【订单】
	 * @return
	 */
	ResponseResult exportExcel(Order order) ;

	/**
	 * 根据【ID】查询【订单详情】
	 * @param id 【ID】
	 * @return 【订单详情】
	 */
	ResponseResult queryOrderById(String id);
	
	
	/**
	 * 根据【通行证ID】查询【订单总数】
	 * @param passportId 【通行证ID】
	 * @return 【订单总数】
	 */
	ResponseResult selectOrderQuantityByPassportId(Integer passportId);

	/**
	 * 取消订单
	 * @param code 编号
	 * @return
	 */
	ResponseResult cancelOrderByCode(String code);

	/**
	 * 回收订单/删除订单
	 * @param code 编号
	 * @return
	 */
	ResponseResult recycleOrderByCode(String code);
	
	/**
	 * 更新订单发货状态
	 * @param code 编号
	 * @return
	 */
	void updateOrderDeliverStatus(String orderCode);

	/**
	 * 根据【编号】更新【审核状态】
	 * @param code 【编号】
	 * @param checkStatus 【审核状态】
	 */
	ResponseResult updateOrderCheckStatusByCode(String code, Byte checkStatus);

	/**
	 * 根据【ID】更新【审核状态】
	 * @param id【ID】
	 * @param checkStatus 【审核状态】
	 */
	ResponseResult updateOrderCheckStatusById(String id, Byte checkStatus);
}
