package com.artup.service;

import com.artup.common.ResponseResult;
import com.artup.pojo.Address;

/**
 * 地址
 * @author hapday
 * @date 2017年7月13日 @Time 下午2:35:31
 */
public interface AddressService {
	/**
	 * 保存【地址】
	 * @param address
	 */
	ResponseResult saveAddress(Address address) ;

	/**
	 * 根据【ID】更新【地址】
	 * @param address 【地址】
	 * @return
	 */
	ResponseResult updateAddressById(Address address);

	/**
	 * 根据【ID】删除【地址】
	 * @param id 【ID】
	 * @return
	 */
	ResponseResult deleteAddressById(String id);

	/**
	 * 根据【通行证ID】查询默认【地址】
	 * @param passportId 【通行证ID】
	 * @return 默认【地址】
	 */
	ResponseResult queryDefaultAddressByPassportId(int passportId);

	/**
	 * 根据【通行证ID】查询【地址列表】
	 * @param passportId 【通行证ID】
	 * @return 【地址列表】
	 */
	ResponseResult queryAddressListByPassportId(int passportId);

	/**
	 * 根据【通行证ID】查询【地址列表】
	 * @param passportId 【通行证ID】
	 * @return 【地址列表】
	 */
	ResponseResult queryAddressListByPassportId2(int passportId);

	/**
	 * 根据【ID】更新【地址】为默认
	 * @param id 【ID】
	 * @return
	 */
	ResponseResult updateAddressDefaultById(String id);
}
