package com.artup.pojo;

import com.artup.common.AbstractObject;

/**
 * 角色-资源关系
 * @author hapday
 * @date 2017年2月27日  @time 上午11:42:56
 */
public class RoleResource extends AbstractObject {
	private static final long serialVersionUID = 1L;
	private Long id;			// 主键
	private Long roleId;		// 角色ID
	private Long resourceId;		// 资源ID
	private byte isValid;		// 是否有效（1-是；2-否；）
	private byte isBelong;		// 是否归属（1-是；2-否；）
	private String resourceName;	// 资源名
	private String resourceRemark;	// 资源的备注
	private byte resourceLevel;	// 资源的级别
	
	public RoleResource() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getResourceId() {
		return resourceId;
	}

	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}

	public byte getIsValid() {
		return isValid;
	}

	public void setIsValid(byte isValid) {
		this.isValid = isValid;
	}

	public byte getIsBelong() {
		return isBelong;
	}

	public void setIsBelong(byte isBelong) {
		this.isBelong = isBelong;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getResourceRemark() {
		return resourceRemark;
	}

	public void setResourceRemark(String resourceRemark) {
		this.resourceRemark = resourceRemark;
	}

	public byte getResourceLevel() {
		return resourceLevel;
	}

	public void setResourceLevel(byte resourceLevel) {
		this.resourceLevel = resourceLevel;
	}
}
