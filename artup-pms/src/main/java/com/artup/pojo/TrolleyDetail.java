package com.artup.pojo;

import com.artup.common.AbstractObject;

/**
 * 购物车明细
 * @author hapday
 * @date 2017年7月14日 @Time 下午6:16:07
 */
public class TrolleyDetail extends AbstractObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;	// ID
	private String trolleyId;	// 购物车ID
	private int passportId;	// 通行证ID
	private String worksId;		// 作品ID
	private String worksName;		// 作品名称
	private String worksSize;		// 作品尺寸
	private String worksShape;		// 作品框型
	private String worksColor;		// 作品颜色
	private String worksTypeCode;		// 作品类型编号
	private String worksThumbnailUrl;		// 作品缩略图URL
	private float price;	// 单价（单位：元）
	private float discount;	// 折扣（单位：元）
	private int quantity;	// 数量
	private float subtotal;		// 小计（单位：元）
	private float discountSubtotal;		// 折扣小计（单位：元）
	private String clientCode;	// 客户端编号，默认：ios
	private String channelCode;		// 渠道编号，默认为 artron
	private String couponId;	// 券ID
	private String skuCode;		// SKU 编号
	private String orderId;	// 订单ID
	private Long invoiceId;		// 发票ID
	
	public TrolleyDetail() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTrolleyId() {
		return trolleyId;
	}

	public void setTrolleyId(String trolleyId) {
		this.trolleyId = trolleyId;
	}

	public int getPassportId() {
		return passportId;
	}

	public void setPassportId(int passportId) {
		this.passportId = passportId;
	}

	public String getWorksId() {
		return worksId;
	}

	public void setWorksId(String worksId) {
		this.worksId = worksId;
	}

	public String getWorksName() {
		return worksName;
	}

	public void setWorksName(String worksName) {
		this.worksName = worksName;
	}

	public String getWorksSize() {
		return worksSize;
	}

	public void setWorksSize(String worksSize) {
		this.worksSize = worksSize;
	}

	public String getWorksShape() {
		return worksShape;
	}

	public void setWorksShape(String worksShape) {
		this.worksShape = worksShape;
	}

	public String getWorksColor() {
		return worksColor;
	}

	public void setWorksColor(String worksColor) {
		this.worksColor = worksColor;
	}

	public String getWorksTypeCode() {
		return worksTypeCode;
	}

	public void setWorksTypeCode(String worksTypeCode) {
		this.worksTypeCode = worksTypeCode;
	}

	public String getWorksThumbnailUrl() {
		return worksThumbnailUrl;
	}

	public void setWorksThumbnailUrl(String worksThumbnailUrl) {
		this.worksThumbnailUrl = worksThumbnailUrl;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public float getDiscount() {
		return discount;
	}

	public void setDiscount(float discount) {
		this.discount = discount;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public float getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(float subtotal) {
		this.subtotal = subtotal;
	}

	public float getDiscountSubtotal() {
		return discountSubtotal;
	}

	public void setDiscountSubtotal(float discountSubtotal) {
		this.discountSubtotal = discountSubtotal;
	}

	public String getClientCode() {
		return clientCode;
	}

	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	public String getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}

	public String getCouponId() {
		return couponId;
	}

	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}

	public String getSkuCode() {
		return skuCode;
	}

	public void setSkuCode(String skuCode) {
		this.skuCode = skuCode;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Long getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}
}
