package com.artup.pojo;

import com.artup.common.AbstractObject;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 作品编辑
 * @author hapday
 * @date 2017年7月17日 @Time 下午6:15:18
 */
@Getter
@Setter
@ToString
public class MaterialEdit extends AbstractObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;	// ID
	private String worksId;		// 作品ID
	private String materialId;	// 素材ID
	private int materialIndex;		// 素材编号
	private byte status;	// 状态（0-编辑器；1-已完成；）
	private String editedMaterialPath;	// 在原图上编辑后的素材路径，主要用于生成 PDF
	private String editedMaterialThumbnailPath;	// 在原图上编辑后的生成的素材缩略图路径
	private String action;	// 编辑动作
	
	private int x;	// 横坐标，单位：像素
	private int y;	// 纵坐标，单位：像素
	private int width;	// 宽度，单位：像素
	private int height;	// 高度，单位：像素
	
	public MaterialEdit() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getWorksId() {
		return worksId;
	}

	public void setWorksId(String worksId) {
		this.worksId = worksId;
	}

	public String getMaterialId() {
		return materialId;
	}

	public void setMaterialId(String materialId) {
		this.materialId = materialId;
	}

	public int getMaterialIndex() {
		return materialIndex;
	}

	public void setMaterialIndex(int materialIndex) {
		this.materialIndex = materialIndex;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public String getEditedMaterialPath() {
		return editedMaterialPath;
	}

	public void setEditedMaterialPath(String editedMaterialPath) {
		this.editedMaterialPath = editedMaterialPath;
	}

	public String getEditedMaterialThumbnailPath() {
		return editedMaterialThumbnailPath;
	}

	public void setEditedMaterialThumbnailPath(String editedMaterialThumbnailPath) {
		this.editedMaterialThumbnailPath = editedMaterialThumbnailPath;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
}
