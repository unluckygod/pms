package com.artup.pojo;

/**
 * 快递类型
 * @author hapday
 * @date 2017年8月9日 @Time 上午9:51:53
 */
public enum ExpressType {
	config;
	private String SF = null;
	private String shunfeng = null;
	private String ZTO = null;
	private String zhongtong = null;
	
	private ExpressType() {
		this.SF = "顺丰速运";
		this.shunfeng = "顺丰速运";
		this.ZTO = "中通快递";
		this.zhongtong = "中通快递";
	}
	
	public String getCompanyName(String companyCode) {
		String companyName = null;
		
		if(companyCode.equalsIgnoreCase("SF") || companyCode.equalsIgnoreCase("shunfeng")) {
			companyName = "顺丰速运";
		} else if(companyCode.equalsIgnoreCase("ZTO") || companyCode.equalsIgnoreCase("zhongtong")) {
			companyName = "中通快递";
		}
		
		return companyName;
	}

	public String getSF() {
		return SF;
	}

	public void setSF(String sF) {
		SF = sF;
	}

	public String getShunfeng() {
		return shunfeng;
	}

	public void setShunfeng(String shunfeng) {
		this.shunfeng = shunfeng;
	}

	public String getZTO() {
		return ZTO;
	}

	public void setZTO(String zTO) {
		ZTO = zTO;
	}

	public String getZhongtong() {
		return zhongtong;
	}

	public void setZhongtong(String zhongtong) {
		this.zhongtong = zhongtong;
	}
}
