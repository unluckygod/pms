package com.artup.pojo;

import java.util.Date;

import com.artup.common.AbstractObject;

import lombok.Getter;
import lombok.Setter;

/**
 * 订单
 * @author hapday
 * @date 2017年7月18日 @Time 上午9:15:14
 */
@Setter
@Getter
public class Order extends AbstractObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;		// ID
	private String code;	// 编号
	private String originalCode;	// 原订单号（当遇到微信支付失败时，将生成新的订单号，此处存储原订单号）
	private String transactionCode;		// 商户订单号（如微信、支付宝等第三方生成的订单号）
	private int passportId;		// 通行证ID
	private float totalPrice;	// 总价
	private int totalQuantity;		// 总数量
	private float totalDiscount;	// 总折扣
	private float expressFee;	// 运费
	private String clientCode;	// 客户端编号，默认：ios
	private String channelCode;		// 渠道编号，默认为 artron
	private byte status;	// 状态（1-已创建；2-支付中；3-已支付）
	private String worksIds;	// 作品 IDs
	private String worksQuantities;	// 作品数量s
	private String trolleyDetailIds;	// 购物车详情 IDs
	private String addressId;	// 收货地址ID
	private Long invoiceId;		// 发票ID
	private String invoiceTitle;	// 发票抬头
	private String invoiceContent;	// 发票内容
	private String couponCode;	// 券编号
	private float couponDiscountAmount;	// 券抵扣金额（单位：元）
	
	private String consigneeName;	// 收货人姓名
	private String mobileCode;	// 收货人的手机号
	private byte payStatus;		// 支付状态：1-已支付；2-未支付；
	private String beginCreateTime;		// 下单开始时间
	private String endCreateTime;		// 下单结束时间
	private byte checkStatus;		// 审核状态：1：未审核；2：已通过；3-已拒绝；
	private String checkStatusName;		// 审核状态名：未审核；已通过；已拒绝；
	private Date checkTime;	// 审核时间
	
	public Order() {
	}
}