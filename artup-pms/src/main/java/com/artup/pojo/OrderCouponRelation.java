package com.artup.pojo;

import com.artup.common.AbstractObject;

import lombok.Getter;
import lombok.Setter;

/**
 * 订单与券的关系
 * @author hapday
 * @date 2017年8月8日 @Time 下午2:46:40
 */
@Setter
@Getter
public class OrderCouponRelation extends AbstractObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;	// ID
	private int passportId;		// 通行证ID
	private String orderId;		// 订单ID
	private String worksId;		// 作品ID
	private String couponCode;	// 券编号
	
	public OrderCouponRelation() {
		// TODO Auto-generated constructor stub
	}
}
