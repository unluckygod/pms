package com.artup.pojo;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.artup.common.AbstractObject;

import lombok.Getter;
import lombok.Setter;

/**
 * 订单的物流
 * @author hapday
 * @date 2017年3月2日 @Time 上午11:52:23
 */
@Setter
@Getter
public class Express extends AbstractObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;//id
	private String code;//物流单号
	private String orderCode;//订单编号
	private String companyCode;//物流公司
	private String companyName;		// 公司名称
	private Date createTime;	// 创建时间
	private Date lastUpdateTime;	// 最后更新日期
	private List<Map<String, Object>> trace;	
	
	public Express() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public List<Map<String, Object>> getTrace() {
		return trace;
	}

	public void setTrace(List<Map<String, Object>> trace) {
		this.trace = trace;
	}
}
