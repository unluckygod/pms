package com.artup.pojo;

import com.artup.common.AbstractObject;

/**
 * 用户-角色关系
 * @author hapday
 * @date 2017年2月27日  @time 上午11:42:56
 */
public class UserRole extends AbstractObject {
	private static final long serialVersionUID = 1L;
	private Long id;			// 主键
	private Long userId;		// 用户ID
	private Long roleId;		// 角色ID
	private byte isValid;		// 是否有效（1-是；2-否；）
	private byte isBelong;		// 是否归属（1-是；2-否；）
	private String roleName;	// 角色名
	private String roleRemark;	// 角色的备注
	
	public UserRole() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public byte getIsValid() {
		return isValid;
	}

	public void setIsValid(byte isValid) {
		this.isValid = isValid;
	}

	public byte getIsBelong() {
		return isBelong;
	}

	public void setIsBelong(byte isBelong) {
		this.isBelong = isBelong;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleRemark() {
		return roleRemark;
	}

	public void setRoleRemark(String roleRemark) {
		this.roleRemark = roleRemark;
	}
}
