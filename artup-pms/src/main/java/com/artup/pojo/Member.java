package com.artup.pojo;

import java.util.Date;

import com.artup.common.AbstractObject;

/**
 * 会员
 * @date 2017年3月20日 下午9:44:38
 * @author hapday
 */
public class Member extends AbstractObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;		// 主键
	private byte type;		// 类型
	private String username;	// 用户名，用于登陆、权限验证
	private String password;	// 密码
	private String name;		// 姓名
	private byte sex;		// 性别：-1-未明确；1-男；2-女
	private String birthday;	// 生日
	private String nickname;	// 昵称
	private String mobileCode;	// 手机号
	private String telephone;	// 固话
	private String email;		// 电子邮箱
	private String qqCode;		// QQ 号
	private String postcode;	// 邮政编码
	private String organization;	// 组织（工作单位）
	private Long provinceId;	// 省份ID
	private Long cityId;		// 地市ID
	private Long districtId;	// 区县ID
	private String detailAddress;	// 详细地址，比如街道名、小区名、单元号、门牌号
	private byte isOpen;	// 是否公开：1-是；2-否；
	private byte status;	// 状态：1-已激活；2-已停用；
	private Long portraitId;	// 头像ID
	private Date registerTime;	// 注册时间
	private String lastLoginTime;	// 最后登陆时间
	
	private String beginRegisterTime;		// 开始创建时间
	private String endRegisterTime;		// 结束创建时间
	private String beginLastLoginTime;		// 最后登陆起始时间
	private String endLastLoginTime;		// 最后登陆结束时间
	private int attentionCount;	// 关注数
	private int siteAge;	// 乐龄（天数）
	
	public Member() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public byte getType() {
		return type;
	}

	public void setType(byte type) {
		this.type = type;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte getSex() {
		return sex;
	}

	public void setSex(byte sex) {
		this.sex = sex;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getMobileCode() {
		return mobileCode;
	}

	public void setMobileCode(String mobileCode) {
		this.mobileCode = mobileCode;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getQqCode() {
		return qqCode;
	}

	public void setQqCode(String qqCode) {
		this.qqCode = qqCode;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public Long getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public Long getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Long districtId) {
		this.districtId = districtId;
	}

	public String getDetailAddress() {
		return detailAddress;
	}

	public void setDetailAddress(String detailAddress) {
		this.detailAddress = detailAddress;
	}

	public byte getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(byte isOpen) {
		this.isOpen = isOpen;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public Long getPortraitId() {
		return portraitId;
	}

	public void setPortraitId(Long portraitId) {
		this.portraitId = portraitId;
	}

	public Date getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(Date registerTime) {
		this.registerTime = registerTime;
	}

	public String getLastLoginTime() {
		return lastLoginTime;
	}
	
	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getBeginRegisterTime() {
		return beginRegisterTime;
	}

	public void setBeginRegisterTime(String beginRegisterTime) {
		this.beginRegisterTime = beginRegisterTime;
	}

	public String getEndRegisterTime() {
		return endRegisterTime;
	}

	public void setEndRegisterTime(String endRegisterTime) {
		this.endRegisterTime = endRegisterTime;
	}

	public String getBeginLastLoginTime() {
		return beginLastLoginTime;
	}
	
	public void setBeginLastLoginTime(String beginLastLoginTime) {
		this.beginLastLoginTime = beginLastLoginTime;
	}
	
	public String getEndLastLoginTime() {
		return endLastLoginTime;
	}
	
	public void setEndLastLoginTime(String endLastLoginTime) {
		this.endLastLoginTime = endLastLoginTime;
	}

	public int getAttentionCount() {
		return attentionCount;
	}

	public void setAttentionCount(int attentionCount) {
		this.attentionCount = attentionCount;
	}

	public int getSiteAge() {
		return siteAge;
	}

	public void setSiteAge(int siteAge) {
		this.siteAge = siteAge;
	}
}
