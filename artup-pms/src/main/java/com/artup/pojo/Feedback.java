package com.artup.pojo;

import java.util.Date;

import com.artup.common.AbstractObject;

/**
 * 意见反馈
 * @author hapday
 * @date 2017年8月22日 @Time 下午3:29:23
 */
public class Feedback extends AbstractObject {
	private static final long serialVersionUID = 1L;
	private String id;	// ID
	private String appVersion;	//app版本
	private String clientDevice;	// 设备名称
	private String clientOs;	// 设备
	private String contact;	// 联系方式
	private String comment;	// 意见
	private String passId;	// 通行证ID
	private Date createTime;	//创建时间
	
	public Feedback() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getClientDevice() {
		return clientDevice;
	}

	public void setClientDevice(String clientDevice) {
		this.clientDevice = clientDevice;
	}

	public String getClientOs() {
		return clientOs;
	}

	public void setClientOs(String clientOs) {
		this.clientOs = clientOs;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getPassId() {
		return passId;
	}

	public void setPassId(String passId) {
		this.passId = passId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "UserFeedback [id=" + id + ", appVersion=" + appVersion
				+ ", clientDevice=" + clientDevice + ", clientOs=" + clientOs
				+ ", contact=" + contact + ", comment=" + comment + ", passId="
				+ passId + ", createTime=" + createTime + "]";
	}
	
}
