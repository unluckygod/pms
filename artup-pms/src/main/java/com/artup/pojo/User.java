package com.artup.pojo;

import com.artup.common.AbstractObject;

/**
 * 用户
 * @author hapday
 * @date 2017年2月17日  @time 上午9:09:29
 */
public class User extends AbstractObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;		// 主键
	private byte sex;		// 性别：-1-未明确；1-男；2-女
	private String sexName;		// 性别
	private byte type;		// 类型
	private String username;	// 用户名，用于登陆、权限验证
	private String password;	// 密码
	private String name;		// 姓名
	private String nickname;	// 昵称
	private String birthday;	// 生日
	private String mobileCode;	// 手机号
	private String email;		// 电子邮箱
	private String qqCode;		// QQ 号
	private String detailAddress;	// 详细地址，比如街道名、小区名、单元号、门牌号
	private byte status;	// 状态：1-已激活；2-已停用；
	private String lastLoginTime;	// 最后登陆时间
	private String beginLastLoginTime;	// 起始最后登陆时间
	private String endLastLoginTime;	// 结束最后登陆时间
	private String startCreateTime;		// 开始创建时间
	private String endCreateTime;		// 结束创建时间
	
	public User() {
		// TODO Auto-generated constructor stub
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public byte getSex() {
		return sex;
	}

	public void setSex(byte sex) {
		this.sex = sex;
	}

	public String getSexName() {
		return sexName;
	}
	
	public void setSexName(String sexName) {
		this.sexName = sexName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public byte getType() {
		return type;
	}

	public void setType(byte type) {
		this.type = type;
	}

	public String getStartCreateTime() {
		return startCreateTime;
	}

	public void setStartCreateTime(String startCreateTime) {
		this.startCreateTime = startCreateTime;
	}

	public String getEndCreateTime() {
		return endCreateTime;
	}

	public void setEndCreateTime(String endCreateTime) {
		this.endCreateTime = endCreateTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getMobileCode() {
		return mobileCode;
	}

	public void setMobileCode(String mobileCode) {
		this.mobileCode = mobileCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getQqCode() {
		return qqCode;
	}

	public void setQqCode(String qqCode) {
		this.qqCode = qqCode;
	}

	public String getDetailAddress() {
		return detailAddress;
	}

	public void setDetailAddress(String detailAddress) {
		this.detailAddress = detailAddress;
	}

	public String getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getBeginLastLoginTime() {
		return beginLastLoginTime;
	}
	
	public void setBeginLastLoginTime(String beginLastLoginTime) {
		this.beginLastLoginTime = beginLastLoginTime;
	}

	public String getEndLastLoginTime() {
		return endLastLoginTime;
	}
	
	public void setEndLastLoginTime(String endLastLoginTime) {
		this.endLastLoginTime = endLastLoginTime;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}
}
