package com.artup.pojo;

import com.artup.common.AbstractObject;

/**
 * 订单与作品关系
 * @author hapday
 * @date 2017年7月18日 @Time 下午2:32:29
 */
public class OrderWorksRelation extends AbstractObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;	// ID
	private String orderId;	// 订单ID
	private String worksId;	// 作品ID
	private int worksQuantity;	// 作品数量
	
	public OrderWorksRelation() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getWorksId() {
		return worksId;
	}

	public void setWorksId(String worksId) {
		this.worksId = worksId;
	}
	
	public int getWorksQuantity() {
		return worksQuantity;
	}
	
	public void setWorksQuantity(int worksQuantity) {
		this.worksQuantity = worksQuantity;
	}
}
