package com.artup.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TextEdit {
	/**
     * 内容
     */
    private String content;
    
    /**
     * 文本框左下角x轴坐标，原点在pdf页面框的左下角，单位 毫米
     */
    private Float x;
    
    /**
     * 文本框左下角y轴坐标，原点在pdf页面框的左下角，单位 毫米
     */
    private Float y;
    
    /**
     * 文本框宽，单位 毫米
     */
    private Float width;
    
    /**
     * 文本框高，单位 毫米
     */
    private Float Height;
    
    /**
	 * 字号大小，单位 磅
	 */
	private Float fontSize;
	
	/**
	 * 字体名称，默认 wryh.ttf
	 */
	private String fontName;
	
	/**
	 * 对齐方式, "left", "middle", "right"
	 */
	private String align;
	
	/**
	 * 最多字数，包含标点符号，每个算一个字。
	 */
	private Integer maxWords;
	
	private Integer pageIndex;	// 页码
}
