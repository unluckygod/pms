package com.artup.pojo;

/**
 * <p>
 * 	一英寸等于 25.4 毫米
 *  一毫米等于 0.0393700787401575 英寸
 *  PPI = 150 时，一英寸 = 150 个像素点
 * </p>
 */

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ImageEntity {
	/**
	 * 150dpi时，要求的图片最低像素
	 * 1厘米 = 0.393700787 英寸
	 * 1毫米 = 0.0393700787 英寸
	 * 当 150 dpi 时 ， 1毫米 = 0.0393700787 英寸 * 150 = 5.906
	 * 当 72 dpi 时， 1毫米 = 0.0393700787 英寸 * 72 = 2.835
	 * 一英寸等于25.4mm
	 */
	public static final Float MM2PX_150 = 5.906F;
	
	public static final Float MM2PX_72 = 2.835F;
	
	public static final Float MM2PX_50 = 1.969F;
	
	
	/**
	 * 模板要求的图片宽，单位像素
	 */
	private Integer width;
	
	/**
	 * 模板要求的图片高，单位像素
	 */
	private Integer height;
	
	/**
	 * 模板要求的图片的路径
	 */
	private String path;
	
	/**
	 * 模板要求的图片宽度单位毫米
	 */
	private Integer widthMm;
	
	/**
	 * 模板要求的图片高度，单位毫米
	 */
	private Integer heightMm;
	
	/**
	 * 模板要求的图片的dpi
	 */
	private Integer dpi;
	
	/**
	 * 用户上传图片的宽，单位像素
	 */
	private Integer srcImageWidth;
	
	/**
	 * 用户上传图片的高，单位像素
	 */
	private Integer srcImageHeight;
	
	private Integer thumbnailNeedWidth;

	/**
	 * 生成缩略图的宽，单位像素
	 */
	private Integer thumbnailWidth;
	
	/**
	 * 生成缩略图的高，单位像素
	 */
	private Integer thumbnailHeight;
	
	/**
	 * 缩略图与原始图的缩放比率
	 */
	private Float thumbnailScale = 0F;
}
