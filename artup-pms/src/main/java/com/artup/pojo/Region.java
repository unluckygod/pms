package com.artup.pojo;

import com.artup.common.AbstractObject;

/**
 * 地区
 * @author hapday
 * @date 2017年7月13日 @Time 下午3:35:26
 */
public class Region extends AbstractObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id;	// ID
	private long parentId;	// 父ID
	private byte type;	// 类型：1-省份；2-地市；3-区县
	private String name;	// 名称
	
	public Region() {
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getParentId() {
		return parentId;
	}

	public void setParentId(long parentId) {
		this.parentId = parentId;
	}

	public byte getType() {
		return type;
	}

	public void setType(byte type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
