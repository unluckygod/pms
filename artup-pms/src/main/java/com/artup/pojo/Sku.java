package com.artup.pojo;

import com.artup.common.AbstractObject;

import lombok.Getter;
import lombok.Setter;

/**
 * 商品编号
 */
@Setter
@Getter
public class Sku extends AbstractObject {
	private static final long serialVersionUID = 1L;

	private long id;	// ID
	private String code;	// 编号
	private String name;	// 名称
	private String worksTypeCode;	// 作品类型编号
	private String worksSize;	// 作品尺寸
	private String materialCode;	// 材质编号
	private String materialName;	// 材质名称
	private String pictureSize;	// 画心尺寸
	private float price;	// 单价
	private int pageNumber;	// 页数
	private String colour;	// 颜色
}
