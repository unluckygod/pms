package com.artup.pojo;

import com.artup.common.AbstractObject;

/**
 * 素材
 * @author hapday
 * @date 2017年7月14日 @Time 上午11:47:57
 */
public class Material extends AbstractObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;	// ID
	private int passportId;	// 通行证ID
	private String originalFileName;	// 原文件名
	private String systemFileName;	// 原文件系统文件名
	private String expandedName;	// 扩展名
	private int originalWidth;	// 原图宽度（单位：像素）
	private int originalHeight;		// 原图高度（单位：像素）
	private String orignalStoragePath;		// 原图存放（绝对）路径
	private String thumbnailStoragePath;	// 缩略图存储（绝对）路径
	private String smallThumbnailImagePath;	// 小缩略图存储（绝对）路径
	private int thumbnailWidth;	// 缩略图宽度（单位：像素）
	private int thumbnailHeight;	// 缩略图高度（单位：像素）
	private float thumbnailScale;		// 缩放比例
	private int dpi;	// DPI
	private String uploadTime;		// 上传时间
	private String clientCode;	// 客户端编号（pc、wechat、ios、android）
	private String channelCode;		// 渠道编号
	private float  zoomScale;		// 缩放比例
	private long originalSize;	// 原图大小
	private long thumbnailSize;		// 缩略图大小
	
	private String beginUploadTime;		// 起始上传时间
	private String endUploadTime;		// 结束上传时间
	
	public Material() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getPassportId() {
		return passportId;
	}

	public void setPassportId(int passportId) {
		this.passportId = passportId;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	public String getSystemFileName() {
		return systemFileName;
	}

	public void setSystemFileName(String systemFileName) {
		this.systemFileName = systemFileName;
	}

	public String getExpandedName() {
		return expandedName;
	}

	public void setExpandedName(String expandedName) {
		this.expandedName = expandedName;
	}

	public int getOriginalWidth() {
		return originalWidth;
	}

	public void setOriginalWidth(int originalWidth) {
		this.originalWidth = originalWidth;
	}

	public int getOriginalHeight() {
		return originalHeight;
	}

	public void setOriginalHeight(int originalHeight) {
		this.originalHeight = originalHeight;
	}

	public String getOrignalStoragePath() {
		return orignalStoragePath;
	}

	public void setOrignalStoragePath(String orignalStoragePath) {
		this.orignalStoragePath = orignalStoragePath;
	}

	public String getThumbnailStoragePath() {
		return thumbnailStoragePath;
	}

	public void setThumbnailStoragePath(String thumbnailStoragePath) {
		this.thumbnailStoragePath = thumbnailStoragePath;
	}
	
	public String getSmallThumbnailImagePath() {
		return smallThumbnailImagePath;
	}

	public void setSmallThumbnailImagePath(String smallThumbnailImagePath) {
		this.smallThumbnailImagePath = smallThumbnailImagePath;
	}

	public int getThumbnailWidth() {
		return thumbnailWidth;
	}

	public void setThumbnailWidth(int thumbnailWidth) {
		this.thumbnailWidth = thumbnailWidth;
	}

	public int getThumbnailHeight() {
		return thumbnailHeight;
	}

	public void setThumbnailHeight(int thumbnailHeight) {
		this.thumbnailHeight = thumbnailHeight;
	}

	public float getThumbnailScale() {
		return thumbnailScale;
	}

	public void setThumbnailScale(float thumbnailScale) {
		this.thumbnailScale = thumbnailScale;
	}

	public int getDpi() {
		return dpi;
	}

	public void setDpi(int dpi) {
		this.dpi = dpi;
	}

	public String getUploadTime() {
		return uploadTime;
	}

	public void setUploadTime(String uploadTime) {
		this.uploadTime = uploadTime;
	}

	public String getClientCode() {
		return clientCode;
	}

	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	public String getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}

	public float getZoomScale() {
		return zoomScale;
	}

	public void setZoomScale(float zoomScale) {
		this.zoomScale = zoomScale;
	}

	public long getOriginalSize() {
		return originalSize;
	}

	public void setOriginalSize(long originalSize) {
		this.originalSize = originalSize;
	}

	public long getThumbnailSize() {
		return thumbnailSize;
	}

	public void setThumbnailSize(long thumbnailSize) {
		this.thumbnailSize = thumbnailSize;
	}

	public String getBeginUploadTime() {
		return beginUploadTime;
	}
	
	public void setBeginUploadTime(String beginUploadTime) {
		this.beginUploadTime = beginUploadTime;
	}

	public String getEndUploadTime() {
		return endUploadTime;
	}
	
	public void setEndUploadTime(String endUploadTime) {
		this.endUploadTime = endUploadTime;
	}
}
