package com.artup.pojo;

/**
 * 图片信息
 * @author hapday
 * @date 2017年8月18日 @Time 上午10:09:32
 */
public class ImageInfo {  
	private String name;  		// 名称
	private Long size;    		// 大小 
	private String size2;    		// 大小（单位：字节）
    private String width ;		// 宽度  
    private String height ;		// 高度  
    private String createTime ;	// 拍摄时间  
    private String altitude ;	// 海拔  
    private String longitude ;	// 经度  
    private String latitude;	// 纬度  
    private String lensModel;	// 透镜模型
    private String lensMake;	// 镜头厂商
    
    public String getName() {  
    	return name;  
    }  
    public void setName(String name) {  
    	this.name = name;  
    }  

    public Long getSize() {  
        return size;  
    }  
    public void setSize(Long size) {  
        this.size = size;  
    }  
    public String getSize2() {  
    	return size2;  
    }  
    public void setSize2(String size2) {  
    	this.size2 = size2;  
    }  
    public String getImgHeight() {  
        return height;  
    }  
    public void setHeight(String height) {  
        this.height = height;  
    }  
    public String getWidth() {  
        return width;  
    }  
    public void setWidth(String width) {  
        this.width = width;  
    }  
    public String getCreateTime() {  
        return createTime;  
    }  
    public void setCreateTime(String createTime) {  
        this.createTime = createTime;  
    }  
    public String getAltitude() {  
        return altitude;  
    }  
    public void setAltitude(String altitude) {  
        this.altitude = altitude;  
    }  
    public String getLatitude() {  
        return latitude;  
    }  
    public void setLatitude(String latitude) {  
        this.latitude = latitude;  
    }  
    
    public String getLlongitude() {  
        return longitude;  
    }  
    public void setLongitude(String longitude) {  
        this.longitude = longitude;  
    }

    public String getLensModel() {  
    	return lensModel;  
    }  
    public void setLensModel(String lensModel) {  
    	this.lensModel = lensModel;  
    }
    
    public String getLensMake() {  
    	return lensMake;  
    }  
    public void setLensMake(String lensMake) {  
    	this.lensMake = lensMake;  
    }  
       
    public String toString (){  
        return "【图片信息】 --- 名称：" + this.name + "，大小：" + this.size + "，宽度：" + this.width + "，高度：" + this.height + "，拍摄时间：" + this.createTime + "，海拔：" + this.altitude + "，经度：" + this.longitude + "，纬度：" + this.latitude + "，镜头厂商：" + this.lensMake + "，镜头模型：" + this.lensModel;  
    }  
}