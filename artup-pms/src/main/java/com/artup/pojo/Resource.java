package com.artup.pojo;

import java.util.List;

import com.artup.common.AbstractObject;

/**
 * 资源
 * @author hapday
 * @date 2017年2月27日  @time 上午11:39:28
 */
public class Resource extends AbstractObject {
	private static final long serialVersionUID = 1L;
	private Long id;		// ID
	private Long parentId;	// 父ID
	private short type;		// 类型：1-菜单；2-按钮；3-超链接；4-app
	private String name;		// 资源名
	private String url;			// URL
	private byte level;		// 层级
	private byte isLeaf;		// 是否是叶子节点（1-是；2-否）
	private short sortNumber;	// 顺序号
	
	private String beginCreateTime;		// 开始创建时间
	private String endCreateTime;		// 结束创建时间
	private String parentName;	// 父资源名
	private byte isBelong;		// 是否归属（1-是；2-否；）
	private List<Resource> subResourceList;	// 子资源列表、下级资源列表
	
	public Resource() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public short getType() {
		return type;
	}

	public void setType(short type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public byte getLevel() {
		return level;
	}

	public void setLevel(byte level) {
		this.level = level;
	}

	public byte getIsLeaf() {
		return isLeaf;
	}

	public void setIsLeaf(byte isLeaf) {
		this.isLeaf = isLeaf;
	}

	public short getSortNumber() {
		return sortNumber;
	}

	public void setSortNumber(short sortNumber) {
		this.sortNumber = sortNumber;
	}

	public String getBeginCreateTime() {
		return beginCreateTime;
	}

	public void setBeginCreateTime(String beginCreateTime) {
		this.beginCreateTime = beginCreateTime;
	}

	public String getEndCreateTime() {
		return endCreateTime;
	}

	public void setEndCreateTime(String endCreateTime) {
		this.endCreateTime = endCreateTime;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public byte getIsBelong() {
		return isBelong;
	}

	public void setIsBelong(byte isBelong) {
		this.isBelong = isBelong;
	}

	public List<Resource> getSubResourceList() {
		return subResourceList;
	}

	public void setSubResourceList(List<Resource> subResourceList) {
		this.subResourceList = subResourceList;
	}
}
