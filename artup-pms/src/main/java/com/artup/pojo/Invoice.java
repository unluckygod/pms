package com.artup.pojo;

import com.artup.common.AbstractObject;

/**
 * 发票
 * @author hapday
 * @date 2017年7月21日 @Time 下午4:14:39
 */
public class Invoice extends AbstractObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;	// ID
	private Integer passportId;		// 通行证ID
	private String title;	// 抬头
	private String content;		// 内容
	
	public Invoice() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPassportId() {
		return passportId;
	}

	public void setPassportId(Integer passportId) {
		this.passportId = passportId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
