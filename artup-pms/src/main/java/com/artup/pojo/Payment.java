package com.artup.pojo;

import java.util.Date;

import com.artup.common.AbstractObject;

/**
 * 支付流水
 * @author hapday
 * @date 2017年7月26日 @Time 下午2:51:40
 */
public class Payment extends AbstractObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;	// ID
	private String orderId;		// 订单ID
	private String orderCode;		// 订单编号
	private String type;	// 支付类型：wechat-微信；alipay-支付宝
	private Float totalPrice;	// 支付总金额
	private String remark;		// 备注
	private Date payTime;	// 支付时间
	private Date payFinishTime;		// 支付完成时间
	private byte status;	// 状态：1-待支付；2-已支付；
	
	public Payment() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderCode() {
		return orderCode;
	}
	
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public Float getTotalPrice() {
		return totalPrice;
	}
	
	public void setTotalPrice(Float totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getPayTime() {
		return payTime;
	}

	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}

	public Date getPayFinishTime() {
		return payFinishTime;
	}

	public void setPayFinishTime(Date payFinishTime) {
		this.payFinishTime = payFinishTime;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}
}
