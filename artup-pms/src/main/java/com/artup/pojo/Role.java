package com.artup.pojo;

import com.artup.common.AbstractObject;

/**
 * 角色
 * @author hapday
 * @date 2017年2月27日  @time 上午11:36:29
 */
public class Role extends AbstractObject {
	private static final long serialVersionUID = 1L;
	private Long id;	// ID
	private byte type;		// 类型
	private String name;	// 名称
	private String startCreateTime;		// 开始创建时间
	private String endCreateTime;		// 结束创建时间
	private byte isBelong;		// 是否归属（1-是；2-否；）
	
	public Role() {
		// TODO Auto-generated constructor stub
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public byte getType() {
		return type;
	}

	public void setType(byte type) {
		this.type = type;
	}

	public String getStartCreateTime() {
		return startCreateTime;
	}

	public void setStartCreateTime(String startCreateTime) {
		this.startCreateTime = startCreateTime;
	}

	public String getEndCreateTime() {
		return endCreateTime;
	}

	public void setEndCreateTime(String endCreateTime) {
		this.endCreateTime = endCreateTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte getIsBelong() {
		return isBelong;
	}

	public void setIsBelong(byte isBelong) {
		this.isBelong = isBelong;
	}
}
