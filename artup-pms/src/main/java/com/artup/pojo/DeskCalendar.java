package com.artup.pojo;

import com.artup.common.AbstractObject;

import lombok.Getter;
import lombok.Setter;

/**
 * 地区
 * @author hapday
 * @date 2017年7月13日 @Time 下午3:35:26
 */
@Setter
@Getter
public class DeskCalendar extends AbstractObject {
	private static final long serialVersionUID = 1L;

	private long id;	// ID
	private int name;	// 名称
	private String originalImageName;	// 原图片名称
	private String expandedName;		// 扩展名
	private String imagePath;	// 图片路径
	private long imageSize;	// 图片大小
	private String skuCode;	// SKU 编号
	private int beginYearMonth;	// 起始年月
	private int endYearMonth;	// 结束年月
	
	public DeskCalendar() {
		// TODO Auto-generated constructor stub
	}
}
