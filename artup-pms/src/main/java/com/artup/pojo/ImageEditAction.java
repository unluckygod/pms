package com.artup.pojo;

import lombok.Getter;
import lombok.Setter;

/**
 * 图片编辑动作
 * @author hapday
 * @date 2017年8月2日 @Time 下午5:33:06
 */
@Setter
@Getter
public class ImageEditAction {
	private int rotate;
	private float thumbnailScale;	// 缩放比例
	private int abscissa;	// 横坐标
	private int ordinate; 	// 纵坐标
	private int originalPointAbscissa;		// 原点横坐标
	private int originalPointOrdinate;		// 原点纵坐标
	private int x;		// 原点横坐标
	private int y;		// 原点纵坐标
	private int width;	// 据原点的宽度，单位：像素
	private int height;	// 据原点的高度，单位：像素
	
	public ImageEditAction() {
		// TODO Auto-generated constructor stub
	}
}
