package com.artup.pojo;

import com.artup.common.AbstractObject;

/**
 * 作品
 * @author hapday
 * @date 2017年7月14日 @Time 下午6:14:24
 */
public class Works extends AbstractObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;	// ID
	private int passportId;		// 通行证ID
	private String name;	// 作品名称
	private float price;	// 作品单价
	private int quantity;	// 数量
	private String size;	// 作品尺寸
	private String color;	// 颜色
	private String shape;	// 框型
	private String typeCode;	// 类型编号
	private String clientCode;	// 客户端编号
	private String templateId;	// 模板ID
	private String finishTime;	// 完成时间
	private String channelCode;		// 渠道编号
	private byte status;	// 状态（-1-全部；1-定制中；2-已完成）
	private String skuCode;		// SKU
	private String thumbnailId;		// 缩略图ID
	private String thumbnailPath;	// 缩略图
	private String pdfPath;	// PDF 路径
	private String sku;	// SKU
	
	private String orderId;		// 订单ID
	private String orderCode;	// 订单号
	private String beginFinishTime;	// 起始完成时间
	private String endFinishTime;	// 结束完成时间
	
	public Works() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getPassportId() {
		return passportId;
	}

	public void setPassportId(int passportId) {
		this.passportId = passportId;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getClientCode() {
		return clientCode;
	}

	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public String getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(String finishTime) {
		this.finishTime = finishTime;
	}

	public String getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public String getSkuCode() {
		return skuCode;
	}

	public void setSkuCode(String skuCode) {
		this.skuCode = skuCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getShape() {
		return shape;
	}

	public void setShape(String shape) {
		this.shape = shape;
	}

	public String getThumbnailId() {
		return thumbnailId;
	}

	public void setThumbnailId(String thumbnailId) {
		this.thumbnailId = thumbnailId;
	}

	public String getThumbnailPath() {
		return thumbnailPath;
	}

	public void setThumbnailPath(String thumbnailPath) {
		this.thumbnailPath = thumbnailPath;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderCode() {
		return orderCode;
	}
	
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getPdfPath() {
		return pdfPath;
	}
	
	public void setPdfPath(String pdfPath) {
		this.pdfPath = pdfPath;
	}

	public String getBeginFinishTime() {
		return beginFinishTime;
	}
	
	public void setBeginFinishTime(String beginFinishTime) {
		this.beginFinishTime = beginFinishTime;
	}

	public String getEndFinishTime() {
		return endFinishTime;
	}
	
	public void setEndFinishTime(String endFinishTime) {
		this.endFinishTime = endFinishTime;
	}
}
