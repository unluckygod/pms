package com.artup.pojo;

import com.artup.common.AbstractObject;

/**
 * 地址
 * @author hapday
 * @date 2017年7月13日 @Time 下午2:02:40
 */
public class Address extends AbstractObject {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;	// ID
	private Integer passportId;	// 通行证ID
	private String name;	// 收货人姓名
	private String mobileCode;	// 收货人手机号
	private long provinceId;	// 省份ID
	private long cityId;	// 地市ID
	private long districtId;	// 区县ID
	private String detail;	// 详细地址
	private String isDefault;	// 是否是默认地址：Y-是；N-否
	
	public Address() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getPassportId() {
		return passportId;
	}

	public void setPassportId(Integer passportId) {
		this.passportId = passportId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobileCode() {
		return mobileCode;
	}

	public void setMobileCode(String mobileCode) {
		this.mobileCode = mobileCode;
	}

	public long getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(long provinceId) {
		this.provinceId = provinceId;
	}

	public long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	public long getDistrictId() {
		return districtId;
	}

	public void setDistrictId(long districtId) {
		this.districtId = districtId;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}
}
