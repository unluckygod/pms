package com.artup.to;

import java.util.Date;

/**
 * 订单的物流
 * @author hapday
 * @date 2017年3月2日 @Time 上午11:52:23
 */
public class ExpressTo {

	private String id;//id
	
	private String order_id;//订单编号
	
	private String logistics_code;//物流单号
	
	private String logistics_company;//物流公司
	
	private Date create_date;//创建日期
	
	private Date last_update;//最后更新日期
	
	private String companyName;		// 公司名称
	
	public ExpressTo() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public String getLogistics_code() {
		return logistics_code;
	}

	public void setLogistics_code(String logistics_code) {
		this.logistics_code = logistics_code;
	}

	public String getLogistics_company() {
		return logistics_company;
	}

	public void setLogistics_company(String logistics_company) {
		this.logistics_company = logistics_company;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}

	public Date getLast_update() {
		return last_update;
	}

	public void setLast_update(Date last_update) {
		this.last_update = last_update;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
}
