package com.artup.to;

/**
 * 素材
 * @author hapday
 * @date 2017年7月14日 @Time 下午2:06:43
 */
public class MaterialTo {
	private int passportId;	// 通行证ID
	private String originalFileName;	// 原文件名
	private String expandedName;	// 扩展名
	private String systemFileName;	// 系统文件名
	private long fileSize;	// 文件大小
	private int thumbnailWidth;	// 缩略图宽度（单位：像素）
	private int thumbnailHeight;	// 缩略图高度（单位：像素）
	private String storagePath;		// 原图存放（绝对）路径
	private String clientCode;	// 客户端编号（pc、wechat、ios、android）
	
	public MaterialTo() {
	}

	public int getPassportId() {
		return passportId;
	}

	public void setPassportId(int passportId) {
		this.passportId = passportId;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	public String getExpandedName() {
		return expandedName;
	}

	public void setExpandedName(String expandedName) {
		this.expandedName = expandedName;
	}

	public String getSystemFileName() {
		return systemFileName;
	}

	public void setSystemFileName(String systemFileName) {
		this.systemFileName = systemFileName;
	}

	public long getFileSize() {
		return fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public int getThumbnailWidth() {
		return thumbnailWidth;
	}

	public void setThumbnailWidth(int thumbnailWidth) {
		this.thumbnailWidth = thumbnailWidth;
	}

	public int getThumbnailHeight() {
		return thumbnailHeight;
	}

	public void setThumbnailHeight(int thumbnailHeight) {
		this.thumbnailHeight = thumbnailHeight;
	}

	public String getStoragePath() {
		return storagePath;
	}

	public void setStoragePath(String storagePath) {
		this.storagePath = storagePath;
	}

	public String getClientCode() {
		return clientCode;
	}

	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
}
