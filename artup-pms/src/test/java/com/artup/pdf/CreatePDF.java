package com.artup.pdf;

import java.io.File;
import java.io.FileOutputStream;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * 
 * @author hapday
 * @date 2017年8月3日 @Time 上午11:57:16
 */
public class CreatePDF {

	public static void main(String[] args) {

		try {

			String pdfUrl = "C:\\Users\\Administrator\\Desktop\\测试版.pdf";
			String fileUrl = "D:/tmp/edited/cuted/2017/8/2/6666/";

			// 建立com.itextpdf.text.Document对象的实例
			Document doc = new Document(PageSize.A4, 0, 0, 0, 0);
			// 建立一个书写器(Writer)与document对象关联，通过书写器(Writer)可以将文档写入到磁盘中
			PdfWriter.getInstance(doc, new FileOutputStream(pdfUrl));
			// 打开文档
			doc.open();

			File file = new File(fileUrl);
			if (file.isDirectory()) {
				File[] list = file.listFiles();
				Image png = null;
				for (File file2 : list) {
					png = Image.getInstance(file2.getAbsolutePath());
					// 根据图片像素设置图片的大小，单位是磅
					png.scaleAbsolute(png.getWidth() / 150 * 72, png.getHeight() / 150 * 72);
					System.out.println(png.getScaledWidth() + "*" + png.getScaledHeight());
					// 根据图片大小设置页面的大小
					Rectangle pageSize = new Rectangle(png.getScaledWidth(), png.getScaledHeight());
					doc.setPageSize(pageSize);
					// 新加一页
					doc.newPage();
					// 将图片放入文档中
					doc.add(png);
				}
			}

			// 关闭文档
			doc.close();

			// 进行pdf文件生产
			File pdfFile = new File(pdfUrl);
			if (pdfFile.exists()) {
				pdfFile.createNewFile();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}