package com.artup.pdf;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.artup.util.pdf.PdfUtil;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;

public class RectangleTest {
	public static void main(String[] args) {
		System.out.println(PdfUtil.convertMm2Pt(6));
		System.out.println(PdfUtil.convertPt2Mm(561));
		System.out.println(PdfUtil.convertPt2Mm(808));
		
		System.out.println(PdfUtil.convertMm2Pt(251));
		System.out.println(PdfUtil.convertPt2Mm(530));
		System.out.println(PdfUtil.point2pixel_150ppi(530));

		// 定义一个A4大小的矩形组件
		Rectangle rectangle = new Rectangle(PageSize.A4);
		// 设置背景色为浅灰色
		rectangle.setBackgroundColor(BaseColor.GRAY);
		// 设置边框类型为box (包裹四周)
		rectangle.setBorder(Rectangle.BOX);
		// 设置边框颜色为深灰色
		rectangle.setBorderColor(BaseColor.GREEN);
		// 设置边框宽度为5
		rectangle.setBorderWidth(1);
		// 创建一个新文档，将rectangle作为预设的样式传入，后面的 10,10,10,10是文档的外边距
		Document document = new Document(rectangle, 10, 10, 10, 10);
		try {
			// 建立一个书写器与文档关联
			PdfWriter.getInstance(document, new FileOutputStream("D:\\rectangleDemo.pdf"));
			// 打开文档
			document.open();
			// 向文档中写入文字
			document.add(new Paragraph("hello world!"));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		} finally {
			// 关闭文档
			document.close();
		}
	}
}
